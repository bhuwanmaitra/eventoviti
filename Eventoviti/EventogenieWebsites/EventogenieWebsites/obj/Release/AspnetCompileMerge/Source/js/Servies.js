﻿/// <reference path="Controller.js" />
app.service('RequestData', function ($http) {
    this.loginEventoviti = function (vPassword, VEmailId, op) {

        var VendorLogin = { VPassword: vPassword, VEmailId: VEmailId, op: op }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/VendorRegistration",
            data: VendorLogin,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    this.registration = function (VCompanyName, Address, vPassword, VEmailId, FirstName, LastName, PhoneNo, VendorInfo, VImage, VMange, UserType, op, ImageName) {

        var VRegistrationData = { VCompanyName: VCompanyName, Address: Address, VPassword: vPassword, VEmailId: VEmailId, FirstName: FirstName, LastName: LastName, PhoneNo: PhoneNo, VendorInfo: VendorInfo, VImage: VImage, VMange: VMange, UserType: UserType, op: op, ImageName: ImageName }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/VendorRegistration",
            data: VRegistrationData,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    this.selectCategory = function (E_Cat_ID, E_Cat_Type, E_Cat_Image, op) {

        var SelectEventCategory = { E_Cat_ID: E_Cat_ID, E_Cat_Type: E_Cat_Type, E_Cat_Image: E_Cat_Image, op: op }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/SelectEventCategory",
            data: SelectEventCategory,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    this.SaveEventTeam = function (TemaId, MemberEmail, MemberName, MemberRole, op) {

        var SaveEventTeam = { TemaId: TemaId, MemberEmail: MemberEmail, MemberName: MemberName, MemberRole: MemberRole, op: op }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/SaveEventTeam",
            data: SaveEventTeam,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    this.CityName = function (op, CityID, CityName) {

        var SaveEventTeam = { op: op, CityID: CityID, CityName: CityName }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/GetCityName",
            data: SaveEventTeam,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    this.vendorCategory = function (vendorId, op) {

        var SaveEventTeam = { VRId: vendorId, op: op }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/GetEventType",
            data: SaveEventTeam,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    this.eventType = function (ETID, vendorId, op) {

        var SaveEventTeam = { ETID: ETID, VRId: vendorId, op: op }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/GetEventType",
            data: SaveEventTeam,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    this.featureType = function (op) {

        var selectFeature = { op: op }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/GetFeatures",
            data: selectFeature,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //by vikas
    this.AddTeamMember = function (teamData) {
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/SaveTeamMember",
            data: teamData,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //by vikas
    this.UpdateTeamMembers = function (teamData) {
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/UpdateTeamMember",
            data: teamData,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //by vikas
    this.DeleteTeamMembers = function (id) {
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/DeleteTeamMember",
            data: { TeamId: id },
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }


    //by vikas
    this.GetRoles = function () {
        return $http({
            method: 'GET',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/GetAllRoles",
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }

    //by vikas
    this.GetTeamMember = function () {
        return $http({
            method: 'GET',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/GetAllTeamMembers",
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //by vikas
    this.GetGroups = function () {
        return $http({
            method: 'GET',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/GetAllGroups",
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }

    //By Aditya

    this.AddNewEvent = function (eventData) {
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/SaveNewEvent",
            data: eventData,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    this.GetAllEvent = function (CreateBy) {
        var allEvent = { CreateBy: CreateBy }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/GetAllEvent",
            data: allEvent,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    this.EditAllEvent = function (EventID) {
        var editEvent = { EventID: EventID }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + 'Eventoviti/EditAllEvent',
            data: editEvent,
            async: false,
            headers: { 'Content-Type': 'appliction/json; charset=utf-8' }
        })
    }
    this.GetAllOngoingEvent = function (CreateBy) {
        var allEvent = { CreateBy: CreateBy }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/GetAllOngoingEvent",
            data: allEvent,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    this.GetAllUpcomingEvent = function (CreateBy) {
        var allEvent = { CreateBy: CreateBy }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/GetAllUpcomingEvent",
            data: allEvent,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    this.SendInviteToOwner = function (EName, EHostEmail, EOwnerName) {
        var inviteParm = { EName: EName, EHostEmail: EHostEmail, EOwnerName: EOwnerName }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/InviteOwnerByMail",
            data: inviteParm,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
});