﻿var app = angular.module('EvetovitiWebsiteModel', ['ngRoute', 'gm']);
////////// Directive /////////////////////
app.directive("fileread", [
  function () {
      return {
          scope: {
              fileread: "="
          },
          link: function (scope, element, attributes) {
              element.bind("change", function (changeEvent) {
                  var reader = new FileReader();
                  reader.onload = function (loadEvent) {
                      scope.$apply(function () {
                          scope.fileread = loadEvent.target.result;
                      });
                  }
                  reader.readAsDataURL(changeEvent.target.files[0]);
              });
          }
      }
  }
]);

app.service('Map', function ($q) {

    this.init = function () {
        debugger
        var options = {
            center: new google.maps.LatLng(40.7127837, -74.00594130000002),
            zoom: 13,
            disableDefaultUI: true
        }
        this.map = new google.maps.Map(
            document.getElementById("map"), options
        );
        this.places = new google.maps.places.PlacesService(this.map);
    }

    this.search = function (str) {
        debugger
        var d = $q.defer();
        this.places.textSearch({ query: str }, function (results, status) {
            if (status == 'OK') {
                d.resolve(results[0]);
            }
            else d.reject(status);
        });
        return d.promise;
    }

    this.addMarker = function (res) {
        debugger
        if (this.marker) this.marker.setMap(null);
        this.marker = new google.maps.Marker({
            map: this.map,
            position: res.geometry.location,
            animation: google.maps.Animation.DROP
        });
        this.map.setCenter(res.geometry.location);
    }

});

app.controller('HomePageCtrl', function ($scope, $location) {
    var vendorTemplate = {
        show: false
    };
    $scope.vendorTemplate = vendorTemplate;
    var userTemplate = {
        show: true
    };
    $scope.userTemplate = userTemplate;
})

app.controller('LoginCtrl', function ($scope, $location, RequestData) {
    $scope.VendorLogin = function () {
        debugger
        var vEmailId = $scope.VEmailId;
        var vPassword = $scope.vPassword;
        var ResulLoginData = RequestData.loginEventoviti(vPassword, vEmailId, 'vendorLogin');
        ResulLoginData.then(function (pl) {
            if (pl.status == EventovitiCommon.GetResponseStatus('Not Found')) {
                alert(pl.data);
            }
            else {
                debugger
                // $scope.CategoryList = pl.data;
                localStorage.VendorInfo = pl.data[0].VRId;
                $location.path('/VendorDashboard');
            }
        })
    }
})

app.controller('ChossePageCtrl', function ($scope, $location, RequestData) {
    $scope.employerSingup = function () {
        $location.path('/SignupPage/').search({ id: 'HL' });
    }
    $scope.manageSingup = function () {
        $location.path('/SignupPage/').search({ id: 'MA' });
    }
    $scope.buisnessSingup = function () {
        $location.path('/SignupPage/').search({ id: 'BE' });
    }
    $scope.employeeSingup = function () {
        $location.path('/SignupPage/').search({ id: 'EV' });
    }
})

app.controller('VendorRegistrationCtrl', function ($scope, RequestData, $location, $routeParams) {
    debugger
    var resultPost = RequestData.selectCategory(0, '', '', 'SelectCategoryType')
    resultPost.then(function (pl) {
        if (pl.status == EventovitiCommon.GetResponseStatus('Not Found')) {
            alert(pl.data);
        } else {
            $scope.CategoryList = pl.data;
        }
    });
    $scope.toggleSelection = function toggleSelection(CatList) {
        var idx = $scope.selection.indexOf(CatList);
        debugger
        // is currently selected
        if (idx > -1) {
            $scope.selection.splice(idx, 1);
        }

            // is newly selected
        else {
            $scope.selection.push(CatList.E_Cat_ID);

        }
    };
    $('#wizard-picture').on('change', function (evt) {
        debugger
        var files = $(evt.currentTarget).get(0).files;

        if (files.length > 0) {

            $scope.uploadme1 = files[0].name;
            console.log($scope.uploadme1);
        }
    });
    $scope.selection = [];
    $scope.VRegistration = function () {
        debugger
        var fd = new FormData();
        var imgBlob = $scope.uploadme;
        if (imgBlob.length > 0) {
            imgBlob = imgBlob.replace(/data:image\/jpeg;base64,/g, '');
            imgBlob = imgBlob.replace(/data:image\/png;base64,/g, '');
        }
        var compnanyName = $scope.CName;
        var vAddress = $scope.VAddress;
        var vPassword = $scope.VPassword;
        var vEmail = $scope.VEmail;
        var vFirstName = $scope.VFirstName;
        var VLastName = $scope.VLastName;
        var VPhone = $scope.VPhone;
        var VInfo = $scope.VInfo;
        var VMange = JSON.stringify($scope.selection);
        var UserType = $routeParams.id;
        var ImageName = $scope.uploadme1;
        var resultPosr = RequestData.registration(compnanyName, vAddress, vPassword, vEmail, vFirstName, VLastName, VPhone, VInfo, imgBlob, VMange, UserType, 'InsertVendor', ImageName)
        resultPosr.then(function (pl) {
            if (pl.status == EventovitiCommon.GetResponseStatus('Not Found')) {
                alert(pl.data);
            } else {
                if (pl.length != 0) {

                    $location.path('/SuccessPage');
                }
                else {

                }
            }
        })

    }

})

app.controller('vendorMasterCtrl', function ($scope, $location) {
    var vendorTemplate = {
        show: true
    };
    $scope.vendorTemplate = vendorTemplate;
    var userTemplate = {
        show: false
    };
    $scope.userTemplate = userTemplate;
    $location.path('/VendorDashboard');
})

app.controller('userDashboardCtrl', function ($scope, $location, $window) {

    debugger
    $scope.tab = 1;

    $scope.setTab = function (newTab) {
        debugger
        $scope.tab = newTab;
    };

    $scope.isSet = function (tabNum) {
        return $scope.tab === tabNum;
    };
})

app.controller('VendorDashboardCtrl', function ($scope) {

})

////Controlers For Create New Event For Vendor ////
app.controller('CreateNewEvent', function ($scope, RequestData, $location, Map) {
    $scope.Customers = [{ MemberEmail: '', MemberName: '', RoleId: '', GroupId: '' }];
    $scope.DataModel = { CitData: { CityName: "Select City", CityId: "0" } }
    $('#wizard-picture').on('change', function (evt) {
        debugger
        var files = $(evt.currentTarget).get(0).files;

        if (files.length > 0) {

            $scope.uploadme1 = files[0].name;
            console.log($scope.uploadme1);
        }
    });
    $scope.eventTypeSelection = function eventTypeSelection(eData) {
        debugger
        var idx = $scope.eventTypeSelect.indexOf(eData);
        debugger
        // is currently selected
        if (idx > -1) {
            $scope.eventTypeSelect.splice(idx, 1);
        }

            // is newly selected
        else {
            $scope.eventTypeSelect.push(eData.ETID);

        }
    };
    $scope.featureSelection = function featureSelection(eData) {
        debugger
        var idx = $scope.featureSelect.indexOf(eData);
        debugger
        // is currently selected
        if (idx > -1) {
            $scope.featureSelect.splice(idx, 1);
        }

            // is newly selected
        else {
            $scope.featureSelect.push(eData.FId);

        }
    };
    $scope.teamGroupSelection = function teamGroupSelection(eData, cat) {
        debugger
        var idx = $scope.teamGroupSelect.indexOf(eData);
        var idxx = $scope.groupSelect.indexOf(eData);
        debugger
        // is currently selected
        if (idx > -1) {
            $scope.teamGroupSelect.splice(idx, 1);
            $scope.groupSelect.splice(idxx, 1);
        }

            // is newly selected
        else {
            $scope.teamGroupSelect.push(eData.TeamId);
            $scope.groupSelect.push(cat.Groups.GroupId);
        }
    };
    $scope.getCity = function () {
        var ResultData = RequestData.CityName('selectCityName')
        ResultData.then(function (pl) {
            if (pl.status == EventovitiCommon.GetResponseStatus('Not Found')) {
                alert(pl.data);
            }
            else {
                $scope.CityData = pl.data;
            }
        })
    }
    var vendorId = localStorage.VendorInfo;
    $scope.getVendorCategory = function () {
        var ResultData = RequestData.vendorCategory(vendorId, 'selectvendorCatrgory')
        ResultData.then(function (pl) {
            debugger
            if (pl.status == EventovitiCommon.GetResponseStatus('Not Found')) {
                alert(pl.data)
            }
            else {
                $scope.eventCategoryData = pl.data;
            }
        })
    }
    $scope.getEventType = function (cat) {
        var ETID = cat.E_Cat_ID
        var ResultData = RequestData.eventType(ETID, vendorId, 'SelectEventType')
        ResultData.then(function (pl) {
            debugger
            if (pl.status == EventovitiCommon.GetResponseStatus('Not Found')) {
                alert(pl.data)
            }
            else {
                $scope.evenTypeData = pl.data;
            }
        })
    }
    $scope.getCity();
    $scope.getVendorCategory();
    $scope.getFType = function () {
        var resultData = RequestData.featureType('SelectFeatures')
        resultData.then(function (pl) {
            debugger
            if (pl.status == EventovitiCommon.GetResponseStatus('Not Found')) {
                alert(pl.data)
            }
            else {
                $scope.featureData = pl.data;
            }
        })
    }
    $scope.Roles = function () {
        var resultRoles = RequestData.GetRoles();
        resultRoles.then(function (pl) {
            debugger
            $scope.RolesList = pl.data;
        }, function () {
            debugger
        })
    }
    $scope.Groups = function () {
        var resultRoles = RequestData.GetGroups();
        resultRoles.then(function (pl) {
            $scope.Groups = pl.data;
        }, function () {
            debugger
        })
    }
    $scope.ViewTeamGroup = function () {
        var viewTeam = RequestData.GetTeamMember($scope.Customers);
        viewTeam.then(function (pl) {
            debugger
            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                $scope.Group = pl.data;
                sessionStorage.Group = JSON.stringify(pl.data);
            }
        }, function () {
        })
    }
    $scope.EditEvent = function (cal) {
        var EventID = cal.EventID
        var viewTeam = RequestData.EditAllEvent(EventID);
        viewTeam.then(function (pl) {
            debugger
            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                $scope.EditEvent = pl.data;
                sessionStorage.EditEvent = JSON.stringify(pl.data);
                $location.path('/ViewEvent');
            }
        }, function () {
        })
    }
    $scope.ViewTeamMembers = function () {
        var viewTeam = RequestData.GetTeamMember($scope.Customers);
        viewTeam.then(function (pl) {
            debugger
            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                $scope.TeamMembers = pl.data;
                sessionStorage.TeamMembers = JSON.stringify(pl.data);
            }
        }, function () {
        })
    }
    $scope.inviteHost = function () {
        $scope.isHostInvited = 'true'
    }
    $scope.noInviteHost = function () {
        $scope.isHostInvited = 'false'
    }

    $scope.SendInvite = function () {
        var resultData = RequestData.SendInviteToOwner('Invite Owner', $scope.hostEmailId, 'http://localhost:60284/index.html#!/SignupPage?id=HL')
        resultData.then(function (pl) {
            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                alert("Send invite to the owner succesfully");
            }
            else {
                alert(pl.data);
            }
        })
    }
    //$scope.getEventType();
    $scope.getFType();
    $scope.Roles();
    $scope.Groups();
    $scope.ViewTeamGroup();
    $scope.GetAllEvent = function () {
        var CreateBy = vendorId;
        var resultAllEvent = RequestData.GetAllEvent(CreateBy);
        resultAllEvent.then(function (pl) {
            debugger
            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                $scope.AllEvents = pl.data;
                sessionStorage.AllEvents = JSON.stringify(pl.data);
            }
        })
    }
    $scope.GetAllEvent();
    $scope.lat = undefined;
    $scope.lng = undefined;
    $scope.$on('gmPlacesAutocomplete::placeChanged', function () {
        debugger
        Map.search($scope.autocomplete.getPlace().formatted_address)
            .then(function (res) { // success
                debugger
                Map.addMarker(res);
                //$scope.place.name = res.name;
                $scope.lat = res.geometry.location.lat();
                $scope.lng = res.geometry.location.lng();
                $scope.$apply();
            },
            function (status) { // error
                $scope.apiError = true;
                $scope.apiStatus = status;
            });
    });
    Map.init();
    ///Save event
    $scope.saveEvent = function () {
        var imgBlob = $scope.uploadme;
        if (imgBlob.length > 0) {
            imgBlob = imgBlob.replace(/data:image\/jpeg;base64,/g, '');
            imgBlob = imgBlob.replace(/data:image\/png;base64,/g, '');
        }
        var EProfilePic = imgBlob;
        var EName = $scope.eventname;
        var EOwnerName = $scope.eventOwnerName;
        var EDateFrom = $scope.eventDateFrom;
        var EDateTo = $scope.eventDateTo;
        var EAddress = $scope.autocomplete.getPlace().formatted_address;
        var ELan = $scope.lat
        var ELang = $scope.lng
        var EGuestCount = $scope.eventGurest;
        var EType = JSON.stringify($scope.eventTypeSelect);
        var GroupId = JSON.stringify($scope.groupSelect);
        var TeamId = JSON.stringify($scope.teamGroupSelect);
        var isEHost = $scope.isHostInvited;
        var EHostEmail = $scope.hostEmailId;
        var EFeatureId = JSON.stringify($scope.featureSelect);
        var ImageName = $scope.uploadme1;
        var CreateBy = vendorId;
        var tempData = {
            EProfilePic: EProfilePic,
            EName: EName,
            EOwnerName: EOwnerName,
            EDateFrom: EDateFrom,
            EDateTo: EDateTo,
            EAddress: EAddress,
            ELan: ELan,
            ELang: ELang,
            EGuestCount: EGuestCount,
            EType: EType,
            GroupId: GroupId,
            TeamId: TeamId,
            isEHost: isEHost,
            EHostEmail: EHostEmail,
            EFeatureId: EFeatureId,
            ImageName: ImageName,
            CreateBy: CreateBy
        }
        var EventData = JSON.parse(JSON.stringify(tempData));
        var saveTeam = RequestData.AddNewEvent(EventData);
        saveTeam.then(function (pl) {
            debugger
            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                alert('Your event is added succesfully');
                $('#btnAdd').css('display', 'block')
                $('#ShowCategoryData').css('display', 'block')
                $('#addCategoryData').css('display', 'none')
                $('#btnView').css('display', 'none')
                $scope.GetAllEvent();
            }
            else {
                alert(pl.data);
            }
        }, function () {
        })
        $scope.eventTypeSelect = [];
        $scope.featureSelect = [];
        $scope.teamGroupSelect = [];
        $scope.groupSelect = [];
    }
    $scope.eventTypeSelect = [];
    $scope.featureSelect = [];
    $scope.teamGroupSelect = [];
    $scope.groupSelect = [];
})

///// Created By Vikas Vanvi Team Members ////////////////////
app.controller('CreateTeamCtrl', function ($scope, $window, RequestData) {

    $scope.IsEdit = false;
    $scope.Customers = [{ MemberEmail: '', MemberName: '', RoleId: '', GroupId: '' }];
    $scope.currentRow = $scope.Customers.length - 1;
    $scope.Add = function () {
        debugger
        $scope.row = $scope.Customers.length - 1;
        //Add the new item to the Array.
        //if ($scope.Emailid) { }     
        if ($scope.Customers[$scope.Customers.length - 1].MemberEmail && $scope.Customers[$scope.Customers.length - 1].MemberName && $scope.Customers[$scope.Customers.length - 1].RoleId && $scope.Customers[$scope.Customers.length - 1].GroupId) {
            $scope.currentRow = $scope.Customers.length;
            $scope.Customers.push({ MemberEmail: '', MemberName: '', RoleId: '', GroupId: '' });
            $scope.changeBorder($scope.row, 'none');
        }
        else {
            $scope.changeBorder($scope.row, '1px solid red');
        }
    };

    $scope.changeBorder = function (row, type) {
        $('#tm_email_' + row).parent('div').addClass('has-error');
        $('#tm_name_' + row).parent('div').addClass('has-error');
        $('#tm_role_' + row).parent('div').addClass('has-error');
        $('#tm_group_' + row).parent('div').addClass('has-error');
        $('#tm_role_' + row).parent('div').addClass('has-error');
    }
    $scope.Remove = function (index) {
        //Find the record using Index from Array.
        var name = $scope.Customers[index].Emailid;
        if ($window.confirm("Do you want to delete: " + name)) {
            //Remove the item from Array using Index.
            $scope.Customers.splice(index, 1);
            if ($scope.Customers.length == 0) {
                $scope.Add();
            }
            $scope.currentRow = $scope.Customers.length - 1;
        }
    }
    //$scope.SaveEventTeam = function () {
    //    debugger

    //    var MemberEmail = $scope.Emailid
    //    var MemberName = $scope.Name
    //    var MemberRole = $scope.Role
    //    var resultPosr = RequestData.SaveEventTeam(0, MemberEmail, MemberName, MemberRole, 'insertTeamMembers')
    //    resultPosr.then(function (pl) {

    //    })
    //}
    $scope.Roles = function () {
        var resultRoles = RequestData.GetRoles();
        resultRoles.then(function (pl) {
            debugger
            $scope.RolesList = pl.data;
        }, function () {
            debugger
        })
    }
    $scope.Groups = function () {
        var resultRoles = RequestData.GetGroups();
        resultRoles.then(function (pl) {
            $scope.Groups = pl.data;
        }, function () {
            debugger
        })
    }
    $scope.SaveTeamMembers = function () {
        debugger
        for (var i = 0; i < $scope.Customers.length; i++) {
            if ($scope.Customers[i].RoleId && $scope.Customers[i].RoleId.RoleId)
                $scope.Customers[i].RoleId = $scope.Customers[i].RoleId.RoleId;
            if ($scope.Customers[i].GroupId && $scope.Customers[i].GroupId.GroupId)
                $scope.Customers[i].GroupId = $scope.Customers[i].GroupId.GroupId;
        }
        var saveTeam = RequestData.AddTeamMember($scope.Customers);
        saveTeam.then(function (pl) {
            debugger
            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                $scope.Customers = [{ MemberEmail: '', MemberName: '', RoleId: '', GroupId: '' }];
                alert('Team Members added succesfully');
                $scope.ViewTeamMembers();
            }
            else {
                alert(pl.data);
            }
        }, function () {
        })
    }

    $scope.ViewTeamMembers = function () {
        var viewTeam = RequestData.GetTeamMember($scope.Customers);
        viewTeam.then(function (pl) {
            debugger
            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                $scope.TeamMembers = pl.data;
                sessionStorage.TeamMembers = JSON.stringify(pl.data);
            }
        }, function () {
        })
    }

    $scope.EditTeamMember = function (groupId, roleId, index) {
        debugger
        $scope.TeamMembers = jQuery.grep($scope.TeamMembers, function (element, index) {
            if (element.TeamId == roleId) {
                element.RoleIds = element.RoleId;
            }
            if (element.GroupId == groupId) {
                element.GroupIds = element.GroupId;
            }
            return element;
        });

        //$scope.RoleIds = roleData[0].RoleId;
        $scope.IsEdit = true;
        $('#tm_email_edit_' + index).css('visibility', 'visible');
        $('#tm_name_edit_' + index).css('visibility', 'visible');
        $('#tm_role_edit_' + index).css('visibility', 'visible');
        $('#tm_group_edit_' + index).css('visibility', 'visible');

        $('#tm_email_view_' + index).css('visibility', 'hidden');
        $('#tm_name_view_' + index).css('visibility', 'hidden');
        $('#tm_role_view_' + index).css('visibility', 'hidden');
        $('#tm_group_view_' + index).css('visibility', 'hidden');
        $('#tm_isAdmin_Edit_' + index).prop('disabled', false);

    }
    $scope.CancelTeamMember = function (teamId, id) {
        $scope.IsEdit = false;
        $('#tm_email_edit_' + id).css('visibility', 'hidden');
        $('#tm_name_edit_' + id).css('visibility', 'hidden');
        $('#tm_role_edit_' + id).css('visibility', 'hidden');
        $('#tm_group_edit_' + id).css('visibility', 'hidden');

        $('#tm_email_view_' + id).css('visibility', 'visible');
        $('#tm_name_view_' + id).css('visibility', 'visible');
        $('#tm_role_view_' + id).css('visibility', 'visible');
        $('#tm_group_view_' + id).css('visibility', 'visible');
        $('#tm_isAdmin_Edit_' + id).prop('disabled', 'disabled');
        debugger
        var teamData = jQuery.grep(JSON.parse(sessionStorage.TeamMembers), function (element, index) {
            return element.TeamId == teamId;
        });
        $('#tm_isAdmin_Edit_' + id).prop('checked', teamData[0].IsAdmin);
    }
    $scope.UpdateTeamMember = function (id, index) {
        var teamData = jQuery.grep($scope.TeamMembers, function (element, index) {
            return element.TeamId == id;
        });
        var tempData = { IsAdmin: teamData[0].IsAdmin, TeamId: teamData[0].TeamId, GroupId: teamData[0].GroupIds, RoleId: teamData[0].RoleIds, MemberName: teamData[0].MemberName, MemberEmail: teamData[0].MemberEmail }
        var resultPosr = RequestData.UpdateTeamMembers(tempData)
        resultPosr.then(function (pl) {
            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                $scope.Customers = [{ MemberEmail: '', MemberName: '', RoleId: '', GroupId: '' }];
                alert('Team Members updated succesfully');
                $scope.ViewTeamMembers();
                $scope.CancelTeamMember(index)
            }
            else {
                alert(pl.data);
            }
        })
    }

    $scope.DeleteTeamMember = function (id) {

        var resultPosr = RequestData.DeleteTeamMembers(id)
        resultPosr.then(function (pl) {
            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                alert('Team Members deleted succesfully');
                $scope.ViewTeamMembers();
            }
            else {
                alert(pl.data);
            }
        })
    }

    $scope.Roles();
    $scope.Groups();
    $scope.ViewTeamMembers();
})

app.controller('ViewEventCtrl', function ($scope, RequestData, Map) {
    debugger
    $scope.EventProfilePic = JSON.parse(sessionStorage.EditEvent)[0].EProfilePic;
    $scope.EName = JSON.parse(sessionStorage.EditEvent)[0].EName;
    $scope.EOwnerName = JSON.parse(sessionStorage.EditEvent)[0].EOwnerName;
    $scope.EDateFrom = JSON.parse(sessionStorage.EditEvent)[0].EDateFrom;
    $scope.EDateTo = JSON.parse(sessionStorage.EditEvent)[0].EDateTo;
    $scope.EAddress = JSON.parse(sessionStorage.EditEvent)[0].EAddress,
    $scope.ELan = JSON.parse(sessionStorage.EditEvent)[0].ELan,
    $scope.ELang = JSON.parse(sessionStorage.EditEvent)[0].ELang,
    $scope.EGuestCount = JSON.parse(sessionStorage.EditEvent)[0].EGuestCount,
    $scope.EType = JSON.parse(sessionStorage.EditEvent)[0].EType,
    $scope.GroupId = JSON.parse(sessionStorage.EditEvent)[0].GroupId,
    $scope.TeamId = JSON.parse(sessionStorage.EditEvent)[0].TeamId,
    $scope.isEHost = JSON.parse(sessionStorage.EditEvent)[0].isEHost,
    $scope.EHostEmail = JSON.parse(sessionStorage.EditEvent)[0].EHostEmail,
    $scope.EFeatureId = JSON.parse(sessionStorage.EditEvent)[0].EFeatureId,
    $scope.ImageName = JSON.parse(sessionStorage.EditEvent)[0].ImageName,
    $scope.CreateBy = JSON.parse(sessionStorage.EditEvent)[0].CreateBy
    Map.init();
    Map.search(JSON.parse(sessionStorage.EditEvent)[0].EAddress)
           .then(function (res) { // success
               debugger
               Map.addMarker(res);
               //$scope.place.name = res.name;
               $scope.lat = res.geometry.location.lat();
               $scope.lng = res.geometry.location.lng();
               $scope.$apply();
           },
           function (status) { // error
               $scope.apiError = true;
               $scope.apiStatus = status;
           });

})
/////Ongoing Event Ctrl made by aditya////////////
app.controller('OngoingEventCtrl', function ($scope, RequestData) {
    var CreateBy = localStorage.VendorInfo;
    $scope.GetAllOngoingEvent = function () {
        var ResultData = RequestData.GetAllOngoingEvent(CreateBy)
        ResultData.then(function (pl) {
            if (pl.status == EventovitiCommon.GetResponseStatus('Not Found')) {
                alert(pl.data);
            }
            else {
                $scope.OngoingEvents = pl.data;
            }
        })
    }
    $scope.GetAllOngoingEvent();
})

app.controller('UpcomingEventCtrl', function ($scope, RequestData) {
    var CreateBy = localStorage.VendorInfo;
    $scope.GetAllUpcomingEvent = function () {
        var ResultData = RequestData.GetAllUpcomingEvent(CreateBy)
        ResultData.then(function (pl) {
            if (pl.status == EventovitiCommon.GetResponseStatus('Not Found')) {
                alert(pl.data);
            }
            else {
                $scope.UpcomingEvents = pl.data;
            }
        })
    }
    $scope.GetAllUpcomingEvent();
})

app.controller('TaskAlloctionCtrl', function ($scope, RequestData, $window) {
    $scope.dateFrom = new Date();
    $scope.tomorrow = new Date();
    $scope.tomorrow.setDate($scope.tomorrow.getDate() + 1);
    $scope.SubTask = [{ SubTaskTitle: '', SubTaskNameTo: '', DateTimeFrom: new Date(), DateTimeTo: new Date() }];
    $scope.currentRow = $scope.SubTask.length - 1;
    $scope.Remove = function (index) {
        //Find the record using Index from Array.
        var name = $scope.SubTask[index].Emailid;
        if ($window.confirm("Do you want to delete: " + name)) {
            //Remove the item from Array using Index.
            $scope.SubTask.splice(index, 1);
            if ($scope.SubTask.length == 0) {
                $scope.Add();
            }
            $scope.currentRow = $scope.SubTask.length - 1;
        }
    }
    $scope.Add = function () {
        debugger
        $scope.row = $scope.SubTask.length - 1;
        //Add the new item to the Array.
        //if ($scope.Emailid) { }     
        if ($scope.SubTask[$scope.SubTask.length - 1].SubTaskTitle) {
            $scope.currentRow = $scope.SubTask.length;
            $scope.SubTask.push({ SubTaskTitle: '', SubTaskNameTo: '', DateTimeFrom: '', DateTimeTo: '' });
            $scope.changeBorder($scope.row, 'none');
        }
        else {
            $scope.changeBorder($scope.row, '1px solid red');
        }
    };
    $scope.changeBorder = function (row, type) {
        $('#tm_SubTaskTitle_' + row).parent('div').addClass('has-error');
        $('#tm_SubTaskNameTo_' + row).parent('div').addClass('has-error');
        $('#tm_DateTimeFrom_' + row).parent('div').addClass('has-error');
        $('#tm_DateTimeTo_' + row).parent('div').addClass('has-error');
    }
})

app.controller('VenuCtrl', function ($scope, RequestData, Map) {
    var vendorId = localStorage.VendorInfo;
    $scope.Customers = [{ MemberEmail: '', MemberName: '', RoleId: '', GroupId: '' }];
    $scope.lat = undefined;
    $scope.lng = undefined;
    $scope.$on('gmPlacesAutocomplete::placeChanged', function () {
        debugger
        Map.search($scope.autocomplete.getPlace().formatted_address)
            .then(function (res) { // success
                debugger
                Map.addMarker(res);
                //$scope.place.name = res.name;
                $scope.lat = res.geometry.location.lat();
                $scope.lng = res.geometry.location.lng();
                $scope.$apply();
            },
            function (status) { // error
                $scope.apiError = true;
                $scope.apiStatus = status;
            });
    });
    Map.init();
    $scope.getEventType = function () {
        var ETID = 1;
        var ResultData = RequestData.eventType(ETID, vendorId, 'SelectEventTypeId')
        ResultData.then(function (pl) {
            debugger
            if (pl.status == EventovitiCommon.GetResponseStatus('Not Found')) {
                alert(pl.data)
            }
            else {
                $scope.evenTypeData = pl.data;
            }
        })
    }
    $scope.SaveVenueManage = function () {
        debugger
        var tempData = {
            VenueName: $scope.VName,
            VenueAddress: $scope.autocomplete.getPlace().formatted_address,
            VLng: $scope.lat,
            VLong: $scope.lng,
            ETID: $scope.Customers.GroupId,
            VenueDetails: $scope.VDetails,
            CreatedBy: vendorId
        }
        var VenuManage = tempData;
        var saveTeam = RequestData.SaveVenue(VenuManage);
        saveTeam.then(function (pl) {
            debugger
            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                alert('Your Venue is added succesfully');
                $('#btnAdd').css('display', 'block')
                $('#ShowCategoryData').css('display', 'block')
                $('#addCategoryData').css('display', 'none')
                $('#btnView').css('display', 'none')
                $scope.GetAllVenueManage();
            }
            else {
                alert(pl.data);
            }
        }, function () {
        })
    }
    $scope.GetAllVenueManage = function () {
        debugger
        var CreatedBy = vendorId;
        var viewTeam = RequestData.GetVenueManage(CreatedBy);
        viewTeam.then(function (pl) {
            debugger
            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                debugger
                $scope.VenueManage = pl.data;
                sessionStorage.VenueManage = JSON.stringify(pl.data);
            }
        }, function () {
        })
    }
    $scope.editVenue = function (venueList) {
        debugger
        $('#btnAdd').css('display', 'none');
        $('#ShowCategoryData').css('display', 'none');
        $('#addCategoryData').css('display', 'block');
        $('#btnView').css('display', 'block');
        $('#update').removeClass('hidden');
        $('#save').addClass('hidden');
        $scope.VenueId = venueList.VenueId;
        $scope.VName = venueList.VenueName;
        $scope.autocomplete = venueList.VenueAddress;
        $scope.lat = venueList.VLng;
        $scope.lng = venueList.VLong;
        $scope.Customers.GroupId = venueList.EventType;
       // $scope.$apply();
        $scope.VDetails = venueList.VenueDetails;
        //  sessionStorage.VenueManage = JSON.stringify(pl.data);
        Map.init();
        Map.search($scope.VenueManage[0].VenueAddress)
               .then(function (res) { // success
                   debugger
                   Map.addMarker(res);
                   $scope.$apply();
               },
               function (status) { // error
                   $scope.apiError = true;
                   $scope.apiStatus = status;
               });
        $scope.$apply();

    }
    $scope.UpdateVenueManage = function () {
        var tempData = {
            venueId: $scope.VenueId,
            VenueName: $scope.VName,
            VenueAddress: $scope.autocomplete.getPlace().formatted_address,
            VLng: $scope.lat,
            VLong: $scope.lng,
            ETID: $scope.Customers.GroupId,
            VenueDetails: $scope.VDetails,
            CreatedBy: vendorId
        }
        var VenuManage = tempData;
        var updateVenue = RequestData.UpdateVenueManage(VenuManage);
        updateVenue.then(function (pl) {
            debugger
            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                alert('Your Venue is updated succesfully');
                $('#btnAdd').css('display', 'block');
                $('#ShowCategoryData').css('display', 'block');
                $('#addCategoryData').css('display', 'none');
                $('#btnView').css('display', 'none');
                $scope.GetAllVenueManage();
            }
            else {
                alert(pl.data);
            }
        }, function () {
        })

    }
    $scope.DeleteVenue = function (venueList) {
        var venueData = {
            venueId: venueList.VenueId
        }
        var deleteData = RequestData.DeleteVenueManage(venueData)
        deleteData.then(function (pl) {
            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                alert('Your Venue is Removed succesfully');
                $('#btnAdd').css('display', 'block');
                $('#ShowCategoryData').css('display', 'block');
                $('#addCategoryData').css('display', 'none');
                $('#btnView').css('display', 'none');
                $scope.GetAllVenueManage();
            }
            else {
                alert(pl.data);
            }
        })
    }
})


////Controlers For Comman Header and Footers ////
app.controller('FooterCtrl', function ($scope) {
    $scope.footer = { name: "Footer.html", url: "MainPage/CommanLinks/Footer.html" };
})
app.controller('HeaderCtrl', function ($scope) {
    $scope.header = { name: "HeaderBar.html", url: "MainPage/CommanLinks/HeaderBar.html" };
})
app.controller('LoginFooterCtrl', function ($scope) {
    $scope.footer = { name: "loginFooter.html", url: "MainPage/CommanLinks/loginFooter.html" };
})
app.controller('LoginHeaderCtrl', function ($scope) {
    $scope.header = { name: "HeaderBar.html", url: "MainPage/CommanLinks/LoginHeader.html" };
})
app.controller('AdminFooterCtrl', function ($scope) {
    $scope.footer = { name: "AdminFooter.html", url: "MainPage/CommanLinks/AdminFooter.html" };
})
app.controller('AdminHeaderCtrl', function ($scope) {
    $scope.header = { name: "AdminHeader.html", url: "MainPage/CommanLinks/AdminHeader.html" };
})
app.controller('SliderMenuCtrl', function ($scope) {
    $scope.sliderHeader = { name: "AdminSliderMenu.html", url: "MainPage/CommanLinks/AdminSliderMenu.html" };
})


