﻿/// <reference path="Controller.js" />

app.config(['$routeProvider', function ($routeProvider) {
    debugger
    $routeProvider
         .when('/Dashboard', {
             templateUrl: "MainPage/Home.html",
             controller: "",
         })
        .when('/Login', {
            templateUrl: "MainPage/login-page.html",
            controller: "LoginCtrl",
        })
        .when('/ChossePage', {
            templateUrl: "MainPage/ChosseEventRegistration.html",
            controller: "ChossePageCtrl",
        })
       .when('/SignupPage/:id', {
           templateUrl: "MainPage/signup-page.html",
           controller: "VendorRegistrationCtrl",
       })
         .when('/SignupPage', {
             templateUrl: "MainPage/signup-page.html",
             controller: "VendorRegistrationCtrl",
         })
        .when('/VendorDashboard', {
            templateUrl: "Vendors/Dashboard.html",
            controller: "VendorDashboardCtrl",

        })
        .when('/vendorMaster', {
            templateUrl: "Index.html",
            controller: "vendorMasterCtrl",
        })
        .when('/userDashboard', {
            templateUrl: "Index.html",
            controller: "userDashboardCtrl",
        })
        .when('/NewEvent', {
            templateUrl: "Vendors/CreateNewEvent.html",
            controller: "CreateNewEvent",
        })
        .when('/TaskList', {
            templateUrl: "Vendors/TaskList.html",
            controller: "",
        })
        .when('/TeamMembers', {
            templateUrl: "Vendors/TeamMembers.html",
            controller: "CreateTeamCtrl",
        })
        .when('/TaskAllocation', {
            templateUrl: "Vendors/TaskAllocation.html",
            controller: "TaskAlloctionCtrl",
        })
        .when('/OngoingEvent', {
            templateUrl: "Vendors/OngoingEvent.html",
            controller: "OngoingEventCtrl",
        })
        .when('/UpcomingEvent', {
            templateUrl: "Vendors/UpcomingEvent.html",
            controller: "UpcomingEventCtrl",
        })
        .when('/ViewEvent', {
            templateUrl: "Vendors/ViewEvent.html",
            controller: "ViewEventCtrl",
        })
        .when('/SuccessPage', {
            templateUrl: "MainPage/SuccessPage.html",
            controller: "",
        })
         .when('/VenuManagement', {
             templateUrl: "Vendors/VenuManagement.html",
             controller: "VenuCtrl",
         })
        .when('/AboutUs', {
            templateUrl: "MainPage/AboutUs.html",
            controller: "",
        })
   .otherwise({
       redirectTo: '/Dashboard'
   });

}]);