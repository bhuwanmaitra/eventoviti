﻿using EventogenieWebsites.Entity;
using EventogenieWebsites.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Repositories
{
    public class VRegistrationRepository : Repository<Vendor_Registration>
    {
        private DataBaseContext _context = null;
        public VRegistrationRepository(DataBaseContext context, UnitOfWork _uom)
                : base(context, _uom)
            {
            _context = context;
        }
    }

}