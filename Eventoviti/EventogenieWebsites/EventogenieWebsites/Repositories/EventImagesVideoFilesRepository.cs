﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EventogenieWebsites.Entity;
using EventogenieWebsites.Models;

namespace EventogenieWebsites.Repositories
{
    public class EventImagesVideoFilesRepository: Repository<EventImagesVideoFiles>
    {public DataBaseContext _context = null;
        public EventImagesVideoFilesRepository(DataBaseContext context, UnitOfWork _uom)
            : base(context, _uom)
        {
            _context = context;
        }
    }
    
}