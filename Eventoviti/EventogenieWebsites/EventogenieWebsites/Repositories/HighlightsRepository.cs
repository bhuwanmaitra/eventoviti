﻿using EventogenieWebsites.Entity;
using EventogenieWebsites.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Repositories
{
    public class HighlightsRepository : Repository<ac_Highlights>
    {
        public DataBaseContext _context = null;
        public HighlightsRepository(DataBaseContext context, UnitOfWork _uom)
            :base(context,_uom)
        {
            _context = context;
        }
    }
}