﻿using EventogenieWebsites.Entity;
using EventogenieWebsites.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Repositories
{
    public class VenueScheduleRepository:Repository<u_VenueSchedule>
    {
        private DataBaseContext _context = null;
        public VenueScheduleRepository(DataBaseContext context, UnitOfWork uow):base(context,uow)
        {
            _context = context;
        }
    }
}