﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EventogenieWebsites.Entity;
using EventogenieWebsites.Models;
namespace EventogenieWebsites.Repositories
{
    public class VenueManageRepository: Repository<u_VenueManagement>
    {
        public DataBaseContext _context = null;
        public VenueManageRepository(DataBaseContext context, UnitOfWork _uom)
            : base(context, _uom)
        {
            _context = context;
        }
    }
}