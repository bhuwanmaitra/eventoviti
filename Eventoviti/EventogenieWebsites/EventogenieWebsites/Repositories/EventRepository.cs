﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EventogenieWebsites.Entity;
using EventogenieWebsites.Models;

namespace EventogenieWebsites.Repositories
{
    public class EventRepository : Repository<u_Event>
    {
        public DataBaseContext _context = null;
        public EventRepository(DataBaseContext context, UnitOfWork _uom)
            : base(context, _uom)
        {
            _context = context;
        }
    }
}