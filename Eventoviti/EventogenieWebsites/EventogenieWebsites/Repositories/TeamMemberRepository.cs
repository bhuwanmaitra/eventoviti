﻿using EventogenieWebsites.Entity;
using EventogenieWebsites.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Repositories
{
    public class TeamMemberRepository:Repository<u_Team>
    {
        private DataBaseContext _context = null;
        public TeamMemberRepository(DataBaseContext context,UnitOfWork uow):base(context,uow)
        {
            _context = context;
        }
    }
}