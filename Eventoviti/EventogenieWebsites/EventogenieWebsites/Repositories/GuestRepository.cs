﻿
using EventogenieWebsites.Entity;
using EventogenieWebsites.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace EventogenieWebsites.Repositories
{
    public class GuestRepository : Repository<Guest>
        {
            public DataBaseContext _context = null;
            public GuestRepository(DataBaseContext context, UnitOfWork _uom)
                : base(context, _uom)
            {
                _context = context;
            }
        }
  
}