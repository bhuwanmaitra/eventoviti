﻿using EventogenieWebsites.Entity;
using EventogenieWebsites.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Repositories
{
    public class VRRepository : Repository<VRegistration>
    {
        private DataBaseContext _context = null;
        public VRRepository(DataBaseContext context, UnitOfWork _uom)
                : base(context, _uom)
            {
            _context = context;
        }
    }
}