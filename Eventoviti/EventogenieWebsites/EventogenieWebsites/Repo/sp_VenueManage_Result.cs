//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EventogenieWebsites.Repo
{
    using System;
    
    public partial class sp_VenueManage_Result
    {
        public int VenueId { get; set; }
        public string VenueName { get; set; }
        public string VenueAddress { get; set; }
        public Nullable<decimal> VLng { get; set; }
        public Nullable<decimal> VLong { get; set; }
        public string ETID { get; set; }
        public string SpecialityTags { get; set; }
        public string ContactNo { get; set; }
        public Nullable<System.TimeSpan> ContactTimeFrom { get; set; }
        public Nullable<System.TimeSpan> ContactTimeTo { get; set; }
        public string VenueDetails { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public string EmailId { get; set; }
        public Nullable<bool> OutLocation { get; set; }
        public string EventType { get; set; }
        public int VTypeId { get; set; }
        public Nullable<int> VSTId { get; set; }
        public string VTNames { get; set; }
        public Nullable<int> SCapacity { get; set; }
        public Nullable<int> FCapacity { get; set; }
        public Nullable<int> CreatedBy1 { get; set; }
        public Nullable<int> VenueId1 { get; set; }
        public int VTId { get; set; }
        public string VTYName { get; set; }
    }
}
