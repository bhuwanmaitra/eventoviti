//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EventogenieWebsites.Repo
{
    using System;
    using System.Collections.Generic;
    
    public partial class u_VenueSchedule
    {
        public int VSId { get; set; }
        public Nullable<int> VenueId { get; set; }
        public string VSDay { get; set; }
        public Nullable<System.DateTime> VSDateFrom { get; set; }
        public Nullable<System.DateTime> VSDateTo { get; set; }
        public string VSTimeFrom { get; set; }
        public string VSTimeto { get; set; }
        public Nullable<int> CreatedBy { get; set; }
    }
}
