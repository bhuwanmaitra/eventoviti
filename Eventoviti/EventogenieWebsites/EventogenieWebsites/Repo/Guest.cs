//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EventogenieWebsites.Repo
{
    using System;
    using System.Collections.Generic;
    
    public partial class Guest
    {
        public long id { get; set; }
        public long EventID { get; set; }
        public string GuestName { get; set; }
        public string IdProof { get; set; }
        public Nullable<int> GuestNo { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public Nullable<int> Pincode { get; set; }
        public string GuestGroup { get; set; }
        public Nullable<decimal> ContactNo { get; set; }
        public Nullable<decimal> WhatsAppNo { get; set; }
        public string EmailId { get; set; }
        public string AnySpecialRequiremnt { get; set; }
        public int CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string GuestStatus { get; set; }
        public string RoomDetail { get; set; }
    }
}
