//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EventogenieWebsites.Repo
{
    using System;
    using System.Collections.Generic;
    
    public partial class u_Highlights
    {
        public int VHId { get; set; }
        public string HId { get; set; }
        public Nullable<int> WYear { get; set; }
        public Nullable<int> EHandled { get; set; }
        public string USP { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> VenueId { get; set; }
    }
}
