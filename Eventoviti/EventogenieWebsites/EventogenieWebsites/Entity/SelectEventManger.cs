﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class SelectEventManger
    {
        public int E_Cat_ID { get; set; }
        public string E_Cat_Type { get; set; }
        public string E_Cat_Image { get; set; }
        public bool Selected { get; set; }
        public string op { get; set; }
    }
}