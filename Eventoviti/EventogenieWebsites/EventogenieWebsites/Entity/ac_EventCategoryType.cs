﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class ac_EventCategoryType
    {
        [Key]
        public int E_Cat_ID { get; set; }
        public string E_Cat_Type { get; set; }
        public string E_Cat_Image { get; set; }
    }
}