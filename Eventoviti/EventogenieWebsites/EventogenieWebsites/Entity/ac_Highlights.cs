﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class ac_Highlights
    {
        [Key]
        public int HId { get; set; }
        public int VSId { get; set; }
        public string HName { get; set; }
        public ac_VebdorSubType VSubType { get; set; }
    }
}