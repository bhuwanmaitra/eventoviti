﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class u_Highlights
    {
        [Key]
        public int VHId { get; set; }
        public string HId { get; set; }
        public int WYear { get; set; }
        public int EHandled { get; set; }
        public string USP { get; set; }
        public int CreatedBy { get; set; }
        public int VenueId { get; set; }
    }
}