﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class u_Event
    {
        [Key]
        public int EventID { get; set; }
        [Required(ErrorMessage = "Profile pic is required")]
        public string EProfilePic { get; set; }
        [Required(ErrorMessage = "Event Name is required")]
        public string EName { get; set; }
        [Required(ErrorMessage = "Event Name is required")]
        public string EOwnerName { get; set; }
        [Required(ErrorMessage = "Start Date is required")]
        public DateTime EDateFrom { get; set; }
        [Required(ErrorMessage = "End Date is required")]
        public DateTime EDateTo { get; set; }
        [Required(ErrorMessage = "Location Latitude is required")]
        public string EAddress { get; set; }
        [Required(ErrorMessage = "Location Latitude is required")]
        public decimal ELan { get; set; }
        [Required(ErrorMessage = "Location Lognitude is required")]
        public decimal ELang { get; set; }
        [Required(ErrorMessage = "Guest Count is required")]
        public string EGuestCount { get; set; }
        [Required(ErrorMessage = "Event Type is required")]
        public string EType { get; set; }
        [Required(ErrorMessage = "Event Group need to selected")]
        public string GroupId { get; set; }
        [Required(ErrorMessage = "Team Members need to selected")]
        public string TeamId { get; set; }
        public bool isEHost { get; set; }
        public string EHostEmail { get; set; }
        [Required(ErrorMessage = "Feature is required")]
        public string EFeatureId { get; set; }
        public int CreateBy { get; set; }
        public string ImageName { get; set; }
        //[ForeignKey ("TeamId")]
        //public u_Team Teams { get; set; }
        //[ForeignKey("GroupId")]
        //public ac_group Groups { get; set; }
        //[ForeignKey("CityID")]
        //public ac_City Citys { get; set; }
        //[ForeignKey("ETID")]
        //public ac_EventType ETypes { get; set; }
    }
}