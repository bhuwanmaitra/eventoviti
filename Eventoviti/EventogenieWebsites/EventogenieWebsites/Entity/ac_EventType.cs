﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class ac_EventType
    {
        [Key]
        public int ETID { get; set; }
        public int VCID { get; set; }
        public string EventType { get; set; }
        public string EventTypeImage { get; set; }
    }
}