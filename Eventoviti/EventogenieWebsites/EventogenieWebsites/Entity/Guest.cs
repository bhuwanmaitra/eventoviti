﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class Guest
    {
        [Key]
        public long id { get; set; }
        public long EventID { get; set; }
        public string GuestName { get; set; }
        public string IdProof { get; set; }
        public Nullable<int> GuestNo { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public Nullable<int> Pincode { get; set; }
        public string GuestGroup { get; set; }
        public Nullable<decimal> ContactNo { get; set; }
        public Nullable<decimal> WhatsAppNo { get; set; }
        public string EmailId { get; set; }
        public string AnySpecialRequiremnt { get; set; }
        public int CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string GuestStatus { get; set; }
        public string RoomDetail { get; set; }
    }
}