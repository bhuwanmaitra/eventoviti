﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class u_VenueManagement
    {
        [Key]
        public int VenueId { get; set; }
        public string VenueName { get; set; }
        public string VenueAddress { get; set; }
        [Required(ErrorMessage = "Location Latitude is required")]
        public decimal VLng { get; set; }
        [Required(ErrorMessage = "Location Latitude is required")]
        public decimal VLong { get; set; }
        public string ETID { get; set; }
        public string SpecialityTags { get; set; }
        public string ContactNo { get; set; }
        public TimeSpan? ContactTimeFrom { get; set; }
        public TimeSpan? ContactTimeTo { get; set; }
        public string VenueDetails { get; set; }
        public int CreatedBy { get; set; }
        public string EmailId { get; set; }
        public bool? OutLocation { get; set; }
        
    }
}