﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class u_VenuePrivacyPolicy
    {
        [Key]
        public int VPCId { get; set; }
        public int VenueId { get; set; }
        public bool TermSevice { get; set; }
        public string TSDetails { get; set; }
        public bool BookingTerms { get; set; }
        public string BTDetails { get; set; }
        public bool CancellationServices { get; set; }
        public string CSDetails { get; set; }
        public bool TravellingTerms { get; set; }
        public string TTDetails { get; set; }
        public int CreatedBy { get; set; }
    }
}