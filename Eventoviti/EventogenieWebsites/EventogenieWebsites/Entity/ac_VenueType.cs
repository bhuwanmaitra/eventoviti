﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class ac_VenueType
    {
        [Key]
        public int VTId { get; set; }
        public string VTName { get; set; }

    }
}