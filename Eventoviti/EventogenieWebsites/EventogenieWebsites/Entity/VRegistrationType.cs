﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class VRegistrationType
    {
        [Key]
        public int? VTRId { get; set; }
        public int? VRId { get; set; }
        public int? E_Cat_ID { get; set; }
        public string VSId { get; set; }
        public int? CreatedBy { get; set; }

    }
}