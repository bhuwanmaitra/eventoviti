﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class Vendor_Registration
    {
        [Key]
        public int? VRId { get; set; }
        public string FullName { get; set; }
        public string VCompanyName { get; set; }
        public string Address { get; set; }
        public string PhoneNo { get; set; }
        public string VEmailId { get; set; }
        public string VPassword { get; set; }
        public string VImage { get; set; }
        public string UserType { get; set; }
    }
}