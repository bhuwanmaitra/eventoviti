﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class VenueComman
    {
        public int? VenueId { get; set; }
        public string VenueName { get; set; }
        public string VenueAddress { get; set; }
        public decimal? VLng { get; set; }
        public decimal? VLong { get; set; }
        public string ETID { get; set; }
        public string SpecialityTags { get; set; }
        public string ContactNo { get; set; }
        public TimeSpan? ContactTimeFrom { get; set; }
        public TimeSpan? ContactTimeTo { get; set; }
        public string VenueDetails { get; set; }
        public int? CreatedBy { get; set; }
        public string op { get; set; }
        public string EventType { get; set; }
        public int? VSTId { get; set; }
        public int? VTId { get; set; }
        public string VTYName { get; set; }
        public string VTNames { get; set; }
        public int? SCapacity { get; set; }
        public int? FCapacity { get; set; }
        public string VCompanyName { get; set; }
        public int EventId { get; set; }
        public int VPCId { get; set; }
        public bool? TermSevice { get; set; }
        public string TSDetails { get; set; }
        public bool? BookingTerms { get; set; }
        public string BTDetails { get; set; }
        public bool? CancellationServices { get; set; }
        public string CSDetails { get; set; }
        public bool? TravellingTerms { get; set; }
        public string TTDetails { get; set; }
        public int? PackageId { get; set; }
        public string PackageName { get; set; }
        public decimal? PackagePrice { get; set; }
        public decimal BookingAmount { get; set; }
        public int Capacity { get; set; }
        public string PackageInfo { get; set; }
        public int HId { get; set; }
        public int VSId { get; set; }
        public string HName { get; set; }
    }
}