﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class EventImagesVideoFiles
    {
        public long id { get; set; }
        public Nullable<int> EventId { get; set; }
        public string FilePath { get; set; }
        public string Type { get; set; }
        public int CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
    }
}