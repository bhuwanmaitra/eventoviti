﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class ac_City
    {
        [Key]
        public int CityID { get; set; }
        public string CityName { get; set; }
    }
}