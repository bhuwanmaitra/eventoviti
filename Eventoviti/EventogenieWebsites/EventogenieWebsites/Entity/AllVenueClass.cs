﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class AllVenueClass
    {
        public int? stayVTId { get; set; }
        public int? VSTId { get; set; }
        public string SVTNames { get; set; }
        public int? SQuantity { get; set; }
        public int? StayCapacity { get; set; }
        public int? CreatedBy { get; set; }
        public int? VenueId { get; set; }
        public int? VSId { get; set; }
        public string VSName { get; set; }
        public int? E_Cat_ID { get; set; }
        public string VSImages { get; set; }
        public char Ctype { get; set; }
        public string op { get; set; }
    }
}