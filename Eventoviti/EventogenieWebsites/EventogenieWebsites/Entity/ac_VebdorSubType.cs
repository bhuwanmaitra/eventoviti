﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class ac_VebdorSubType
    {
        [Key]
        public int VSId { get; set; }
        public string VSName { get; set; }
        public int E_Cat_ID { get; set; }
        public string VSImages { get; set; }
        public string Ctype { get; set; }
        [ForeignKey("E_Cat_ID")]
        public ac_EventCategoryType VendorType { get; set; }
        
    }
}