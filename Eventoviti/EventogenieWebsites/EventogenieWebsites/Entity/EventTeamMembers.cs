﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class EventTeamMembers
    {
        public int TemaId { get; set; }
        public string MemberEmail { get; set; }
        public string MemberName { get; set; }
        public string MemberRole { get; set; }
        public string Op { get; set; }
    }
}