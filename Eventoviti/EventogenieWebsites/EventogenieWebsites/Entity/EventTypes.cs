﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class EventTypes
    {
        public int? ETID { get; set; }
        public int? VCID { get; set; }
        public int? VRId { get; set; }
        public string EventType { get; set; }
        public string EventTypeImage { get; set; }
        public string E_Cat_Type { get; set; }
        public int? E_Cat_ID { get; set; }
        public string op { get; set; }
    }
}