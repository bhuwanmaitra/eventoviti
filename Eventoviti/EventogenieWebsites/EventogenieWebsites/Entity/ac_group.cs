﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class ac_group
    {
        [Key]
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public int GroupBy { get; set; }
    }
}