﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class u_Packages
    {
        [Key]
        public int PackageId { get; set; }
        public int VenueId { get; set; }
        public string PackageName { get; set; }
        public decimal PackagePrice { get; set; }
        public decimal BookingAmount { get; set; }
        public int Capacity { get; set; }
        public string PackageInfo { get; set; }
        public int CreatedBy { get; set; }
    }
}