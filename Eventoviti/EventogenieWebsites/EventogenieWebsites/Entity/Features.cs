﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class Features
    {
        public int? FId { get; set; }
        public int? EventId { get; set; }
        public string FeatureName { get; set; }
        public string op { get; set; }
    }
}