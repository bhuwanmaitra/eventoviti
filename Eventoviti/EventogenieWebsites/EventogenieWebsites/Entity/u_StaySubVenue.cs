﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class u_StaySubVenue
    {
        [Key]
        public int StayVTId { get; set; }
        public int VSTId { get; set; }
        public string SVTNames { get; set; }
        public int SQuantity { get; set; }
        public int StayCapacity { get; set; }
        public int CreatedBy { get; set; }
        public int VenueId { get; set; }
        [ForeignKey("VSTId")]
        public ac_VebdorSubType vebdorSubType { get; set; }
    }
}