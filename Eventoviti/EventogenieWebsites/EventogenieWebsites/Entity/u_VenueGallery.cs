﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class u_VenueGallery
    {
        [Key]
        public int VImageId { get; set; }
        public int VenueId { get; set; }
        public string VIName { get; set; }
        public int CreatedBy { get; set; }
    }
}