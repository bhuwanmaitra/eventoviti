﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class VenueManagePages
    {
        public int VenueId { get; set; }
        public string VenueName { get; set; }
        public string VenueAddress { get; set; }
        public decimal VLng { get; set; }
        public decimal VLong { get; set; }
        public string ETID { get; set; }
        public string SpecialityTags { get; set; }
        public string ContactNo { get; set; }
        public TimeSpan ContactTimeFrom { get; set; }
        public TimeSpan ContactTimeTo { get; set; }
        public string VenueDetails { get; set; }
        public int CreatedBy { get; set; }
        public string EmailId { get; set; }
        public bool? OutLocation { get; set; }
        public List<Image> listImage { get; set; }         
        public List<u_VenueAccientType> listVenueType { get; set; }
        public u_VenueGallery listVenueGallery { get; set; }
        public u_Packages listPackages { get; set; }
        public u_VenuePrivacyPolicy listPrivacyPolicy { get; set; }
        public List<u_StaySubVenue> listStaySubVenue { get; set; }
        public u_Highlights listHighlights { get; set; }
    }
}