﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class u_VenueAccientType
    {
        [Key]
        public int VTypeId { get; set; }
        public int VSTId { get; set; }
        public string VTNames { get; set; }
        public int SCapacity { get; set; }
        public int FCapacity { get; set; }
        public int CreatedBy { get; set; }
        public int VenueId { get; set; }
        [ForeignKey("VSTId")]
        public ac_VebdorSubType vebdorSubType { get; set; }
    }
}