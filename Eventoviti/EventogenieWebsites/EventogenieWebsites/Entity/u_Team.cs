﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class u_Team
    {
        [Key]
        public int TeamId { get; set; }
        [Required(ErrorMessage = "Email is required")]
        public string MemberEmail { get; set; }
        [Required(ErrorMessage = "Name is required")]
        public string MemberName { get; set; }

        [ForeignKey("RoleId")]
        public u_Roles Roles { get; set; }
        [ForeignKey("GroupId")]
        public ac_group Groups { get; set; }


        [Required(ErrorMessage = "Member Role is required")]
        public int? RoleId { get; set; }

        [Required(ErrorMessage = "Member Group is required")]
        public int? GroupId { get; set; }
        public bool? IsAdmin { get; set; } = false;
    }
}