﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class VRegistration
    {
        public int? VRId { get; set; }
        public string VCompanyName { get; set; }
        public string Address { get; set; }
        public string VPassword { get; set; }
        public string VEmailId { get; set; }
        public string PhoneNo { get; set; }
        public string VImage { get; set; }
        public string UserType { get; set; }
        public string ImageName { get; set; }
        public string FullName { get; set; }
        public int? VTRId { get; set; }
        public string E_Cat_ID { get; set; }
        public string VSId { get; set; }
        public int? CreatedBy { get; set; }
        public string op { get; set; }
    }
}