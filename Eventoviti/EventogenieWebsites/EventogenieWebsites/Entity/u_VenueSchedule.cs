﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class u_VenueSchedule
    {
        [Key]
        public int VSId { get; set; }
        public int VenueId { get; set; }
        public string VSDay { get; set; }
        public DateTime VSDateFrom { get; set; }
        public DateTime VSDateTo { get; set; }
        public string VSTimeFrom { get; set; }
        public string VSTimeto { get; set; }
        public int CreatedBy { get; set; }
    }
}