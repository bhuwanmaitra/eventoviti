﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Entity
{
    public class ac_SubTask
    {
        [Key]
       public int SubTaskId { get; set; }
       public string SubTaskName { get; set; }
       public DateTime? DateAllocate { get; set; }
       public TimeSpan? TimeAllocate { get; set; }
       public int? TaskId { get; set; }

    }
}