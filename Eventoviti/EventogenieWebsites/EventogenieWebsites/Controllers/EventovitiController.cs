﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EventogenieWebsites.Models;
using Newtonsoft.Json;
using System.IO;
using System.Web;
using System.Text;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Drawing;
using EventogenieWebsites.Entity;
using EventogenieWebsites.Repositories;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;

namespace EventogenieWebsites.Controllers
{
    public class EventovitiController : ApiController
    {
        UnitOfWork uow = new UnitOfWork();
        public string HttpMessage(string requestBody)
        {
            using (var stream = new MemoryStream())
            {
                var context = (HttpContextBase)Request.Properties["MS_HttpContext"];
                context.Request.InputStream.Seek(0, SeekOrigin.Begin);
                context.Request.InputStream.CopyTo(stream);
                requestBody = Encoding.UTF8.GetString(stream.ToArray());
            }
            return requestBody;
        }
        #region Registration
        [HttpPost]
        public List<ac_VebdorSubType> GetSubVendorList()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<ac_VebdorSubType>(requestBody);
            var eventRepository = uow.Repository<VendorSubRepository>();
            var eventData = eventRepository.GetAllData().Distinct()
                .Where(x => x.E_Cat_ID == (int)requestModel.E_Cat_ID).ToList();
            return eventData;
        }
        [HttpPost]
        public HttpResponseMessage VendorRegistration()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<VRegistration>(requestBody);
            var doctorRepository = uow.Repository<VRegistrationRepository>();
            if (requestModel.VImage != null)
            {
                var bytes = Convert.FromBase64String(requestModel.VImage);
                string url = HttpContext.Current.Server.MapPath("~/SaveImages");
                if (!System.IO.Directory.Exists(url))
                    System.IO.Directory.CreateDirectory(url);
                string path = System.Web.HttpContext.Current.Server.MapPath("~/SaveImages");
                using (var ms = new MemoryStream(bytes, 0, bytes.Length))
                {
                    System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
                    image.Save(path + "\\" + requestModel.VRId + requestModel.ImageName, System.Drawing.Imaging.ImageFormat.Png);
                }

            }

            requestModel.VImage = requestModel.VRId + requestModel.ImageName;
            var result = doctorRepository.ExecWithStoreProcedure("sp_VendorRegistration @VRId, @VCompanyName, @Address, @VPassword, @VEmailId, @PhoneNo, @VImage,  @UserType, @op",
                 new SqlParameter("VRId", System.Data.SqlDbType.Int) { Value = requestModel.VRId.HasValue ? requestModel.VRId : (object)DBNull.Value },
                 new SqlParameter("VCompanyName", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.VCompanyName) ? (object)DBNull.Value : requestModel.VCompanyName },
                 new SqlParameter("Address", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.Address) ? (object)DBNull.Value : requestModel.Address },
                 new SqlParameter("VPassword", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.VPassword) ? (object)DBNull.Value : requestModel.VPassword },
                 new SqlParameter("VEmailId", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.VEmailId) ? (object)DBNull.Value : requestModel.VEmailId },
                 new SqlParameter("PhoneNo", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.PhoneNo) ? (object)DBNull.Value : requestModel.PhoneNo },
                 new SqlParameter("VImage", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.VImage) ? (object)DBNull.Value : requestModel.VImage },
                 new SqlParameter("UserType", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.UserType) ? (object)DBNull.Value : requestModel.UserType },
                 new SqlParameter("op", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.op) ? (object)DBNull.Value : requestModel.op }
             ).ToList();

            return this.Request.CreateResponse(HttpStatusCode.OK, result);
        }
        [HttpPost]
        public HttpResponseMessage SaveVendorType()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<VRegistration>(requestBody);
            var VendorRepository = uow.Repository<VRegistrationTypeRepository>();
            VRegistrationType VManager = new VRegistrationType();
            var values = requestModel.E_Cat_ID.ToString();
            if (values.StartsWith("[")) values = values.Substring(1);
            if (values.EndsWith("]")) values = values.Substring(0, values.Length - 1);
            int id = Convert.ToInt32(values);
            var VSId = requestModel.VSId.ToString();
            if (values.StartsWith("[")) VSId = VSId.Substring(1);
            if (values.EndsWith("]")) VSId = VSId.Substring(0, VSId.Length - 1);

            VManager.VRId = requestModel.VRId;
            VManager.E_Cat_ID = id;
            VManager.VSId = VSId;
            VManager.CreatedBy = requestModel.CreatedBy;

            VendorRepository.Add(VManager);
            return this.Request.CreateResponse(HttpStatusCode.OK, VManager.VRId);
        }
        #endregion 
        [HttpPost]
        public HttpResponseMessage VendorRegistrations()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<VRegistration>(requestBody);
            var VendorRepository = uow.Repository<VRegistrationRepository>();
            Vendor_Registration VManager = new Vendor_Registration();
            if (requestModel.VImage != null)
            {
                var bytes = Convert.FromBase64String(requestModel.VImage);
                string url = HttpContext.Current.Server.MapPath("~/SaveImages");
                if (!System.IO.Directory.Exists(url))
                    System.IO.Directory.CreateDirectory(url);
                string path = System.Web.HttpContext.Current.Server.MapPath("~/SaveImages");
                using (var ms = new MemoryStream(bytes, 0, bytes.Length))
                {
                    System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
                    image.Save(path + "\\" + requestModel.VRId + requestModel.ImageName, System.Drawing.Imaging.ImageFormat.Png);
                }
            }

            requestModel.VImage = requestModel.VRId + requestModel.ImageName;
            VManager.VImage = requestModel.VImage;
            VManager.FullName = requestModel.FullName;
            VManager.VCompanyName = requestModel.VCompanyName;
            VManager.Address = requestModel.Address;
            VManager.PhoneNo = requestModel.PhoneNo;
            VManager.VEmailId = requestModel.VEmailId;
            VManager.VPassword = requestModel.VPassword;
            VManager.UserType = requestModel.UserType;
            VendorRepository.Add(VManager);
            return this.Request.CreateResponse(HttpStatusCode.OK, VManager.VRId);
        }
        [HttpPost]
        public List<SelectEventManger> SelectEventCategory()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<SelectEventManger>(requestBody);
            var eventRepository = uow.Repository<SelectEventCategoryRepositry>();

            var result = eventRepository.ExecWithStoreProcedure("Sp_CategoryType @E_Cat_ID, @E_Cat_Type, @E_Cat_Image, @op",
                 new SqlParameter("E_Cat_ID", System.Data.SqlDbType.Int) { Value = requestModel.E_Cat_ID },
                 new SqlParameter("E_Cat_Type", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.E_Cat_Type) ? (object)DBNull.Value : requestModel.E_Cat_Type },
                 new SqlParameter("E_Cat_Image", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.E_Cat_Image) ? (object)DBNull.Value : requestModel.E_Cat_Image },
                 new SqlParameter("op", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.op) ? (object)DBNull.Value : requestModel.op }
             ).ToList();
            return result;
        }
        [HttpPost]
        public List<EventTeamMembers> SaveEventTeam()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<EventTeamMembers>(requestBody);
            var eventRepository = uow.Repository<EventTeamRepository>();
            //fo
            var result = eventRepository.ExecWithStoreProcedure("sp_CreateTeamMembers @TemaId, @MemberEmail, @MemberName,@MemberRole, @op",
                 new SqlParameter("TemaId", System.Data.SqlDbType.Int) { Value = requestModel.TemaId },
                 new SqlParameter("MemberEmail", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.MemberEmail) ? (object)DBNull.Value : requestModel.MemberEmail },
                 new SqlParameter("MemberName", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.MemberName) ? (object)DBNull.Value : requestModel.MemberName },
                 new SqlParameter("MemberRole", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.MemberRole) ? (object)DBNull.Value : requestModel.MemberRole },
                 new SqlParameter("op", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.Op) ? (object)DBNull.Value : requestModel.Op }
             ).ToList();

            return result;
        }
        [HttpPost]
        public List<CommanFile> GetCityName()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<CommanFile>(requestBody);
            var eventRepository = uow.Repository<CommanRepository>();
            var result = eventRepository.ExecWithStoreProcedure("sp_city @CityID , @CityName,@op",
                 new SqlParameter("CityID", System.Data.SqlDbType.Int) { Value = requestModel.CityID.HasValue ? requestModel.CityID : (object)DBNull.Value },
                 new SqlParameter("CityName", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.CityName) ? (object)DBNull.Value : requestModel.CityName },
                 new SqlParameter("op", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.op) ? (object)DBNull.Value : requestModel.op }
             ).ToList();
            return result;
        }
        [HttpPost]
        public List<EventTypes> GetEventType()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<EventTypes>(requestBody);
            var eventRepository = uow.Repository<eventTypeRepository>();
            var result = eventRepository.ExecWithStoreProcedure("sp_EventType @ETID , @VCID,@VRId,@EventType ,@EventTypeImage,@op",
                 new SqlParameter("ETID", System.Data.SqlDbType.Int) { Value = requestModel.ETID.HasValue ? requestModel.ETID : (object)DBNull.Value },
                 new SqlParameter("VCID", System.Data.SqlDbType.Int) { Value = requestModel.VCID.HasValue ? requestModel.VCID : (object)DBNull.Value },
                 new SqlParameter("VRId", System.Data.SqlDbType.Int) { Value = requestModel.VRId.HasValue ? requestModel.VRId : (object)DBNull.Value },
                 new SqlParameter("EventType", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.EventType) ? (object)DBNull.Value : requestModel.EventType },
                 new SqlParameter("EventTypeImage", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.EventTypeImage) ? (object)DBNull.Value : requestModel.EventTypeImage },
                 new SqlParameter("op", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.op) ? (object)DBNull.Value : requestModel.op }
             ).ToList();
            return result;
        }
        [HttpPost]
        public List<Features> GetFeatures()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<Features>(requestBody);
            var eventRepository = uow.Repository<FeatureRepository>();
            var result = eventRepository.ExecWithStoreProcedure("sp_Feature @FId, @EventId,@FeatureName,@op",
                 new SqlParameter("FId", System.Data.SqlDbType.Int) { Value = requestModel.FId.HasValue ? requestModel.FId : (object)DBNull.Value },
                 new SqlParameter("EventId", System.Data.SqlDbType.Int) { Value = requestModel.EventId.HasValue ? requestModel.EventId : (object)DBNull.Value },
                new SqlParameter("FeatureName", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.FeatureName) ? (object)DBNull.Value : requestModel.FeatureName },
                 new SqlParameter("op", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.op) ? (object)DBNull.Value : requestModel.op }
             ).ToList();
            return result;
        }

        #region Team Member

        [HttpPost]
        public HttpResponseMessage SaveTeamMember()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<List<u_Team>>(requestBody);
            Validate<List<u_Team>>(requestModel);
            if (!ModelState.IsValid)
            {
                var message = string.Join("\n", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList());
                return Request.CreateResponse(HttpStatusCode.NonAuthoritativeInformation, message);
            }
            var teamMemberRepository = uow.Repository<TeamMemberRepository>();
            teamMemberRepository.AddRange(requestModel);
            for (int i = 0; i < requestModel.Count; i++)
            {
                Task.Factory.StartNew(() => SendEmailData("Send Eamil to Employees", requestModel[i].MemberEmail, "http://localhost:60284/index.html#!/SignupPage?id=EV"), TaskCreationOptions.LongRunning);
            }
            return Request.CreateResponse(HttpStatusCode.OK, "");
        }
        [HttpPost]
        public HttpResponseMessage UpdateTeamMember()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<u_Team>(requestBody);
            Validate<u_Team>(requestModel);
            if (!ModelState.IsValid)
            {
                var message = string.Join("\n", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList());
                return Request.CreateResponse(HttpStatusCode.NonAuthoritativeInformation, message);
            }
            var teamMemberRepository = uow.Repository<TeamMemberRepository>();
            var teamData = teamMemberRepository.Get(x => x.TeamId == requestModel.TeamId);
            teamData.MemberEmail = requestModel.MemberEmail;
            teamData.MemberName = requestModel.MemberName;
            teamData.GroupId = requestModel.GroupId;
            teamData.RoleId = requestModel.RoleId;
            teamData.IsAdmin = requestModel.IsAdmin;
            teamMemberRepository.Attach(teamData);
            return Request.CreateResponse(HttpStatusCode.OK, "");
        }
        [HttpPost]
        public HttpResponseMessage DeleteTeamMember()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<u_Team>(requestBody);
            var teamMemberRepository = uow.Repository<TeamMemberRepository>();
            var teamData = teamMemberRepository.Get(x => x.TeamId == requestModel.TeamId);
            teamMemberRepository.Delete(teamData);
            return Request.CreateResponse(HttpStatusCode.OK, "record deleted successfully");
        }

        #endregion

        #region NewEvent
        [HttpPost]
        public HttpResponseMessage SaveNewEvent()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<u_Event>(requestBody);
            Validate<u_Event>(requestModel);
            if (!ModelState.IsValid)
            {
                var message = string.Join("\n", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList());
                return Request.CreateResponse(HttpStatusCode.NonAuthoritativeInformation, message);
            }
            if (requestModel.EProfilePic != null)
            {
                var bytes = Convert.FromBase64String(requestModel.EProfilePic);
                string url = HttpContext.Current.Server.MapPath("~/SaveImages");
                if (!System.IO.Directory.Exists(url))
                    System.IO.Directory.CreateDirectory(url);
                string path = System.Web.HttpContext.Current.Server.MapPath("~/SaveImages");
                using (var ms = new MemoryStream(bytes, 0, bytes.Length))
                {
                    System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
                    image.Save(path + "\\" + requestModel.EventID + requestModel.ImageName, System.Drawing.Imaging.ImageFormat.Png);
                    requestModel.EProfilePic = requestModel.EventID + requestModel.ImageName;
                }
                var values = requestModel.EType.ToString();
                if (values.StartsWith("[")) values = values.Substring(1);
                if (values.EndsWith("]")) values = values.Substring(0, values.Length - 1);
                requestModel.EType = values;
                var featureValues = requestModel.EFeatureId.ToString();
                if (featureValues.StartsWith("[")) featureValues = featureValues.Substring(1);
                if (featureValues.EndsWith("]")) featureValues = featureValues.Substring(0, featureValues.Length - 1);
                requestModel.EFeatureId = featureValues;
            }
            requestModel.ImageName = requestModel.EventID + requestModel.ImageName;

            var teamMemberRepository = uow.Repository<EventRepository>();
            teamMemberRepository.Add(requestModel);
            return Request.CreateResponse(HttpStatusCode.OK, "");
        }
        [HttpPost]
        public List<u_Event> GetAllEvent()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<u_Event>(requestBody);
            var eventRepository = uow.Repository<EventRepository>();
            var eventData = eventRepository.GetAllData().Distinct()
                .Where(x => x.CreateBy == (int)requestModel.CreateBy).ToList();
            return eventData;
        }

        [HttpPost]
        public List<u_Event> GetAllOngoingEvent()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<u_Event>(requestBody);
            var eventRepository = uow.Repository<EventRepository>();
            DateTime startDateTime = DateTime.Today.Date;
            var eventData = eventRepository.GetAllData().Distinct()
                .Where(x => x.CreateBy == (int)requestModel.CreateBy
                        //&& DbFunctions.TruncateTime(x.EDateFrom) >= DbFunctions.TruncateTime(DateTime.Today)
                        && x.EDateTo >= DateTime.Today).ToList();
            return eventData;
        }
        [HttpPost]
        public List<u_Event> GetAllUpcomingEvent()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<u_Event>(requestBody);
            var eventRepository = uow.Repository<EventRepository>();
            DateTime startDateTime = DateTime.Today.Date;
            var eventData = eventRepository.GetAllData().Distinct()
                .Where(x => x.CreateBy == (int)requestModel.CreateBy
                        && DbFunctions.TruncateTime(x.EDateFrom) > DbFunctions.TruncateTime(DateTime.Today)).ToList();
            return eventData;
        }

        [HttpPost]
        public List<u_Event> EditAllEvent()
        {
            DataBaseContext db = new DataBaseContext();
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<u_Event>(requestBody);
            var teamMemberRepository = uow.Repository<EventRepository>();
            //char[] delimiters = new char[] { ',' };
            //string[] stringArray = teamMemberRepository.GetAllData().Where(ch => ch.EventID == (int)requestModel.EventID).Select(ch => ch.ETID).ToString().Split(delimiters);
            //// int[] intArray = Array.ConvertAll(stringArray, s => int.Parse(s));
            //int[] ETID = stringArray.Select(x => int.Parse(x)).ToArray();
            var teamData = teamMemberRepository.GetAllData()
                //.Join(db., p => p.Id, pc => pc.ProdId, (p, pc) => new { p, pc })
                //.Include(x => ETID.Contains(x.ETypes.ETID))
                .Distinct()
                .Where(x => x.EventID == (int)requestModel.EventID).ToList();
            return teamData;
        }
        #endregion

        #region Common
        [HttpGet]
        public List<u_Roles> GetAllRoles()
        {
            var roleRepository = uow.Repository<RoleRepository>();
            var roleList = roleRepository.GetAll().ToList();
            return roleList;
        }
        [HttpGet]
        public List<u_Team> GetAllTeamMembers()
        {
            var teamMemberRepository = uow.Repository<TeamMemberRepository>();
            var teamData = teamMemberRepository.GetAllData().Include(x => x.Roles).Include(x => x.Groups).Distinct().ToList();
            return teamData;
        }
        [HttpGet]
        public List<ac_group> GetAllGroups()
        {
            var groupRepository = uow.Repository<GroupRepository>();
            var groupData = groupRepository.GetAll().ToList();
            return groupData;
        }
        #endregion

        #region Send Email Formate
        [HttpPost]
        public HttpResponseMessage InviteOwnerByMail()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<u_Event>(requestBody);
            string subject = requestModel.EName;
            string emailId = requestModel.EHostEmail;
            string link = requestModel.EOwnerName;
            Task.Factory.StartNew(() => SendEmailData(subject, emailId, link), TaskCreationOptions.LongRunning);
            return Request.CreateResponse(HttpStatusCode.OK, "");
        }
        #endregion

        #region Comman Services Management
        [HttpPost]
        public HttpResponseMessage InsertVenue()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<VenueManagePages>(requestBody);
            Validate<VenueManagePages>(requestModel);
            if (!ModelState.IsValid)
            {
                var message = string.Join("\n", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList());
                return Request.CreateResponse(HttpStatusCode.NonAuthoritativeInformation, message);
            }
            var taskRepository = uow.Repository<VenueManageRepository>();

            u_VenueManagement VManager = new u_VenueManagement();
            var values = requestModel.ETID.ToString();
            if (values.StartsWith("[")) values = values.Substring(1);
            if (values.EndsWith("]")) values = values.Substring(0, values.Length - 1);
            requestModel.ETID = values;
            VManager.VenueName = requestModel.VenueName;
            VManager.VenueAddress = requestModel.VenueAddress;
            VManager.VLng = requestModel.VLng;
            VManager.VLong = requestModel.VLong;
            VManager.ETID = requestModel.ETID;
            VManager.SpecialityTags = requestModel.SpecialityTags;
            VManager.ContactNo = requestModel.ContactNo;
            VManager.ContactTimeFrom = requestModel.ContactTimeFrom;
            VManager.ContactTimeTo = requestModel.ContactTimeTo;
            VManager.VenueDetails = requestModel.VenueDetails;
            VManager.CreatedBy = requestModel.CreatedBy;
            VManager.EmailId = requestModel.EmailId;
            VManager.OutLocation = requestModel.OutLocation;
            //task.Priority = requestModel.Priority;
            taskRepository.Add(VManager);

            var VenueTypeRepository = uow.Repository<u_VenueTypeRepository>();
            foreach (var venueType in requestModel.listVenueType)
            {
                if (requestModel.listVenueType[0].FCapacity != 0)
                {
                    venueType.VenueId = VManager.VenueId;
                    venueType.CreatedBy = VManager.CreatedBy;
                    VenueTypeRepository.Add(venueType);
                }
            }
            var stayRepository = uow.Repository<StaySubVenueRepository>();
            foreach (var stayVenueType in requestModel.listStaySubVenue)
            {
                if (requestModel.listStaySubVenue[0].SQuantity != 0)
                {
                    stayVenueType.VenueId = VManager.VenueId;
                    stayVenueType.CreatedBy = VManager.CreatedBy;
                    stayRepository.Add(stayVenueType);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, VManager.VenueId);
        }
        [HttpPost]
        public List<u_VenueManagement> GetAllVenueManage()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<u_VenueManagement>(requestBody);
            var venueRepository = uow.Repository<VenueManageRepository>();
            var venueData = venueRepository.GetAllData().Distinct()
                .Where(x => x.CreatedBy == (int)requestModel.CreatedBy).ToList();
            return venueData;
        }
        [HttpPost]
        public List<u_VenueManagement> GetVenueByID()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<u_VenueManagement>(requestBody);
            var venueRepository = uow.Repository<VenueManageRepository>();
            var venueData = venueRepository.GetAllData().Distinct()
                 .Where(x => x.CreatedBy == (int)requestModel.CreatedBy && x.VenueId == (int)requestModel.VenueId)
                .ToList();
            return venueData;
        }
        [HttpPost]
        public HttpResponseMessage UpdateVenueManage()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<u_VenueManagement>(requestBody);
            Validate<u_VenueManagement>(requestModel);
            if (!ModelState.IsValid)
            {
                var message = string.Join("\n", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList());
                return Request.CreateResponse(HttpStatusCode.NonAuthoritativeInformation, message);
            }
            var venueManageRepository = uow.Repository<VenueManageRepository>();
            var venueManageData = venueManageRepository.Get(x => x.VenueId == requestModel.VenueId);
            venueManageData.VenueName = requestModel.VenueName;
            venueManageData.VenueAddress = requestModel.VenueAddress;
            venueManageData.VLng = requestModel.VLng;
            venueManageData.VLong = requestModel.VLong;
            venueManageData.ETID = requestModel.ETID;
            venueManageData.SpecialityTags = requestModel.SpecialityTags;
            venueManageData.ContactNo = requestModel.ContactNo;
            venueManageData.ContactTimeFrom = requestModel.ContactTimeFrom;
            venueManageData.ContactTimeTo = requestModel.ContactTimeTo;
            venueManageData.VenueDetails = requestModel.VenueDetails;
            venueManageData.CreatedBy = requestModel.CreatedBy;
            venueManageRepository.Attach(venueManageData);
            return Request.CreateResponse(HttpStatusCode.OK, "");
        }
        [HttpPost]
        public HttpResponseMessage DeleteVeueManage()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<u_VenueManagement>(requestBody);
            var venueRepository = uow.Repository<VenueManageRepository>();
            var venueData = venueRepository.Get(x => x.VenueId == requestModel.VenueId);
            venueRepository.Delete(venueData);
            return Request.CreateResponse(HttpStatusCode.OK, "record deleted successfully");
        }
        [HttpPost]
        public HttpResponseMessage SaveVenueSchedule()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<u_VenueSchedule>(requestBody);
            Validate<u_VenueSchedule>(requestModel);
            if (!ModelState.IsValid)
            {
                var message = string.Join("\n", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList());
                return Request.CreateResponse(HttpStatusCode.NonAuthoritativeInformation, message);
            }
            var teamMemberRepository = uow.Repository<VenueScheduleRepository>();
            teamMemberRepository.Add(requestModel);
            return Request.CreateResponse(HttpStatusCode.OK, "");
        }
        [HttpPost]
        public List<u_VenueSchedule> GetAllVenueSchedule()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<u_VenueSchedule>(requestBody);
            var venueRepository = uow.Repository<VenueScheduleRepository>();
            var venueData = venueRepository.GetAllData().Distinct()
                .Where(x => x.CreatedBy == (int)requestModel.CreatedBy && x.VenueId == (int)requestModel.VenueId).ToList();
            return venueData;
        }
        [HttpPost]
        public HttpResponseMessage UpdateVenueSchedule()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<u_VenueSchedule>(requestBody);
            Validate<u_VenueSchedule>(requestModel);
            if (!ModelState.IsValid)
            {
                var message = string.Join("\n", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList());
                return Request.CreateResponse(HttpStatusCode.NonAuthoritativeInformation, message);
            }
            var venueManageRepository = uow.Repository<VenueScheduleRepository>();
            var venueManageData = venueManageRepository.Get(x => x.VSId == requestModel.VSId);
            venueManageData.VSDay = requestModel.VSDay;
            venueManageData.VSDateFrom = requestModel.VSDateFrom;
            venueManageData.VSDateTo = requestModel.VSDateTo;
            venueManageData.VSTimeFrom = requestModel.VSTimeFrom;
            venueManageData.VSTimeto = requestModel.VSTimeto;
            venueManageData.CreatedBy = requestModel.CreatedBy;
            venueManageRepository.Attach(venueManageData);
            return Request.CreateResponse(HttpStatusCode.OK, "");
        }
        [HttpPost]
        public HttpResponseMessage DeleteVeueSechedule()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<u_VenueSchedule>(requestBody);
            var venueRepository = uow.Repository<VenueScheduleRepository>();
            var venueData = venueRepository.Get(x => x.VSId == requestModel.VSId);
            venueRepository.Delete(venueData);
            return Request.CreateResponse(HttpStatusCode.OK, "record deleted successfully");
        }
        [HttpPost]
        public List<ac_VebdorSubType> SelectVenuetype()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<ac_VebdorSubType>(requestBody);
            var venueRepository = uow.Repository<VendorSubRepository>();
            var venueData = venueRepository.GetAllData().Include(x => x.VendorType).Distinct().
                Where(x => x.Ctype == requestModel.Ctype).ToList();
            return venueData;
        }
        [HttpPost]
        public List<AllVenueClass> SelectVenueOnlyType()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<AllVenueClass>(requestBody);
            var stayRepository = uow.Repository<AllVenueClassRepository>();
            var result = stayRepository.ExecWithStoreProcedure("sp_VendorSubType @VSId ,@VSName ,@E_Cat_ID, @VSImages, @VenueId,@op",
                 new SqlParameter("VSId", System.Data.SqlDbType.Int) { Value = requestModel.VSId.HasValue ? requestModel.VSId : (object)DBNull.Value },
                new SqlParameter("VSName", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.VSName) ? (object)DBNull.Value : requestModel.VSName },
                 new SqlParameter("E_Cat_ID", System.Data.SqlDbType.Int) { Value = requestModel.E_Cat_ID.HasValue ? requestModel.E_Cat_ID : (object)DBNull.Value },
                 new SqlParameter("VSImages", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.VSImages) ? (object)DBNull.Value : requestModel.VSImages },
                 new SqlParameter("VenueId", System.Data.SqlDbType.Int) { Value = requestModel.VenueId.HasValue ? requestModel.VenueId : (object)DBNull.Value },
                 new SqlParameter("op", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.op) ? (object)DBNull.Value : requestModel.op }
             ).ToList();
            return result;

        }
        [HttpPost]
        public HttpResponseMessage InsertVenueGallery()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<VenueManagePages>(requestBody);
            Validate<VenueManagePages>(requestModel);
            if (!ModelState.IsValid)
            {
                var message = string.Join("\n", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList());
                return Request.CreateResponse(HttpStatusCode.NonAuthoritativeInformation, message);
            }
            var VenueTypeRepository = uow.Repository<VenueGalleryRepository>();
            for (var i = 0; i < requestModel.listImage.Count; i++)
            {
                if (!string.IsNullOrEmpty(requestModel.listImage[0].name))
                {
                    if (requestModel.listImage[i].name != null)
                    {
                        var bytes = Convert.FromBase64String(requestModel.listImage[i].url);
                        string url = HttpContext.Current.Server.MapPath("~/SaveImages");
                        if (!System.IO.Directory.Exists(url))
                            System.IO.Directory.CreateDirectory(url);
                        string path = System.Web.HttpContext.Current.Server.MapPath("~/SaveImages");
                        using (var ms = new MemoryStream(bytes, 0, bytes.Length))
                        {
                            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
                            image.Save(path + "\\" + requestModel.VenueId + requestModel.listImage[i].name, System.Drawing.Imaging.ImageFormat.Png);
                        }
                    }
                    u_VenueGallery lis = new u_VenueGallery();
                    lis.VIName = requestModel.listImage[i].name;
                    lis.VenueId = requestModel.VenueId;
                    lis.CreatedBy = requestModel.CreatedBy;
                    VenueTypeRepository.Add(lis);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, "");
        }
        [HttpPost]
        public HttpResponseMessage InsertVenuePackages()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<VenueManagePages>(requestBody);
            Validate<VenueManagePages>(requestModel);
            if (!ModelState.IsValid)
            {
                var message = string.Join("\n", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList());
                return Request.CreateResponse(HttpStatusCode.NonAuthoritativeInformation, message);
            }
            var VenuePackagesRepository = uow.Repository<VenuePackagesRepository>();
            if (!string.IsNullOrEmpty(requestModel.listPackages.PackageName))
            {
                // u_Packages packages = new u_Packages();
                requestModel.listPackages.CreatedBy = requestModel.CreatedBy;
                requestModel.listPackages.VenueId = requestModel.VenueId;
                VenuePackagesRepository.Add(requestModel.listPackages);
            }
            return Request.CreateResponse(HttpStatusCode.OK, "");
        }
        [HttpPost]
        public HttpResponseMessage InsertVenuePolicy()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<VenueManagePages>(requestBody);
            Validate<VenueManagePages>(requestModel);
            if (!ModelState.IsValid)
            {
                var message = string.Join("\n", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList());
                return Request.CreateResponse(HttpStatusCode.NonAuthoritativeInformation, message);
            }
            var VenuePackagesRepository = uow.Repository<VenuePrivacyRepository>();
            if (!string.IsNullOrEmpty(requestModel.listPrivacyPolicy.TSDetails))
            {
                requestModel.listPrivacyPolicy.CreatedBy = requestModel.CreatedBy;
                requestModel.listPrivacyPolicy.VenueId = requestModel.VenueId;
                VenuePackagesRepository.Add(requestModel.listPrivacyPolicy);
            }
            return Request.CreateResponse(HttpStatusCode.OK, "");
        }
        [HttpPost]
        public List<ac_Highlights> GetHighlights()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<ac_Highlights>(requestBody);
            var HighlightsRepository = uow.Repository<HighlightsRepository>();
            var venueData = HighlightsRepository.GetAllData().Include(x => x.VSubType).Distinct()
                            .ToList();
            return venueData;
        }
        [HttpPost]
        public List<VenueComman> GetAllVenuesDatas()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<VenueComman>(requestBody);
            var eventRepository = uow.Repository<VenueManagePagesRepository>();
            var result = eventRepository.ExecWithStoreProcedure("sp_VenueManage @VenueId , @VenueName,@VenueAddress,@VLng ,@VLong,@ETID,@SpecialityTags,@ContactNo,@ContactTimeFrom,@ContactTimeTo,@VenueDetails,@CreatedBy,@op",
                 new SqlParameter("VenueId", System.Data.SqlDbType.Int) { Value = requestModel.VenueId.HasValue ? requestModel.VenueId : (object)DBNull.Value },
                 new SqlParameter("VenueName", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.VenueName) ? (object)DBNull.Value : requestModel.VenueName },
                 new SqlParameter("VenueAddress", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.VenueAddress) ? (object)DBNull.Value : requestModel.VenueAddress },
                 new SqlParameter("VLng", System.Data.SqlDbType.Decimal) { Value = requestModel.VLng.HasValue ? requestModel.VLng : (object)DBNull.Value },
                 new SqlParameter("VLong", System.Data.SqlDbType.Decimal) { Value = requestModel.VLong.HasValue ? requestModel.VLong : (object)DBNull.Value },
                 new SqlParameter("ETID", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.ETID) ? (object)DBNull.Value : requestModel.ETID },
                 new SqlParameter("SpecialityTags", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.SpecialityTags) ? (object)DBNull.Value : requestModel.SpecialityTags },
                 new SqlParameter("ContactNo", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.ContactNo) ? (object)DBNull.Value : requestModel.ContactNo },
                 new SqlParameter("ContactTimeFrom", System.Data.SqlDbType.Time) { Value = requestModel.ContactTimeFrom.HasValue ? requestModel.ContactTimeFrom : (object)DBNull.Value },
                 new SqlParameter("ContactTimeTo", System.Data.SqlDbType.Time) { Value = requestModel.ContactTimeTo.HasValue ? requestModel.ContactTimeTo : (object)DBNull.Value },
                 new SqlParameter("VenueDetails", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.VenueDetails) ? (object)DBNull.Value : requestModel.VenueDetails },
                 new SqlParameter("CreatedBy", System.Data.SqlDbType.Int) { Value = requestModel.CreatedBy.HasValue ? requestModel.CreatedBy : (object)DBNull.Value },
                 new SqlParameter("op", System.Data.SqlDbType.VarChar) { Value = string.IsNullOrEmpty(requestModel.op) ? (object)DBNull.Value : requestModel.op }
             ).ToList();
            return result;
        }
        [HttpPost]
        public HttpResponseMessage InsertHighlights()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<VenueManagePages>(requestBody);
            Validate<VenueManagePages>(requestModel);
            if (!ModelState.IsValid)
            {
                var message = string.Join("\n", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList());
                return Request.CreateResponse(HttpStatusCode.NonAuthoritativeInformation, message);
            }
            var hRepository = uow.Repository<CHighlightsRepository>();
            if (requestModel.listHighlights.EHandled != 0)
            {

                // u_Packages packages = new u_Packages();
                requestModel.listHighlights.CreatedBy = requestModel.CreatedBy;
                requestModel.listHighlights.VenueId = requestModel.VenueId;
                hRepository.Add(requestModel.listHighlights);
            }
            return Request.CreateResponse(HttpStatusCode.OK, "");
        }
        [HttpPost]
        public List<u_Packages> GetPackages()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<u_Packages>(requestBody);
            var HighlightsRepository = uow.Repository<VenuePackagesRepository>();
            var venueData = HighlightsRepository.GetAllData().Distinct()
                            .Where(x => x.VenueId == (int)requestModel.VenueId && x.CreatedBy == (int) requestModel.CreatedBy).ToList();
            return venueData;
        }
        [HttpPost]
        public List<u_VenueManagement> GetServicesDetails() {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<u_VenueManagement>(requestBody);
            var HighlightsRepository = uow.Repository<VenueManageRepository>();
            var venueData = HighlightsRepository.GetAllData().Distinct()
                            .Where(x => x.VenueId == (int)requestModel.VenueId && x.CreatedBy == (int)requestModel.CreatedBy).ToList();
            return venueData;
        }
        [HttpPost]
        public List<u_VenueGallery> GetServicesImages()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<u_VenueGallery>(requestBody);
            var ImagesRepository = uow.Repository<VenueGalleryRepository>();
            var venueData = ImagesRepository.GetAllData().Distinct()
                            .Where(x => x.VenueId == (int)requestModel.VenueId && x.CreatedBy == (int)requestModel.CreatedBy).ToList();
            return venueData;
        }
        #endregion

        #region Task
        public HttpResponseMessage AddTask()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<TaskModel>(requestBody);
            Validate<TaskModel>(requestModel);
            if (!ModelState.IsValid)
            {
                var message = string.Join("\n", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList());
                return Request.CreateResponse(HttpStatusCode.NonAuthoritativeInformation, message);
            }
            var taskRepository = uow.Repository<TaskRepository>();

            ac_Task task = new ac_Task();
            task.IsReminderMail = requestModel.IsReminderMail;
            task.IsReminderNotification = requestModel.IsReminderNotification;
            if (Convert.ToInt16(requestModel.Priority) == 0)
            {
                task.Priority = null;
            }
            else
            {
                task.Priority = Convert.ToInt16(requestModel.Priority);
            }
            task.ReminderDate = requestModel.ReminderDate;
            task.ReminderTime = requestModel.ReminderTime;
            task.SubTaskId = requestModel.SubTaskId;
            task.TaskAssignedBy = requestModel.TaskAssignedBy;
            task.EventId = (int)requestModel.EventId;
            task.TaskDate = requestModel.TaskDate;
            task.TaskDateFrom = requestModel.TaskDateFrom;
            task.TaskDateTo = requestModel.TaskDateTo;
            task.TaskName = requestModel.TaskName;
            task.TaskTimeFrom = requestModel.TaskTimeFrom;
            task.TaskTimeTo = requestModel.TaskTimeTo;
            task.TeamId = (int)requestModel.TeamId;
            //task.Priority = requestModel.Priority;
            taskRepository.Add(task);

            var subTaskRepository = uow.Repository<SubTaskRepository>();
            foreach (var team in requestModel.lstTaskTeam)
            {
                if (!string.IsNullOrEmpty(requestModel.lstTaskTeam[0].SubTaskName))
                {
                    team.TaskId = task.TaskId;
                    subTaskRepository.Add(team);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, "");
        }
        public HttpResponseMessage DeleteTask()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<ac_Task>(requestBody);
            var taskRepository = uow.Repository<TaskRepository>();
            var taskData = taskRepository.Get(x => x.TaskId == requestModel.TaskId);
            taskRepository.Delete(taskData);
            return Request.CreateResponse(HttpStatusCode.OK, "");
        }
        [HttpPost]
        public HttpResponseMessage UpdateTask()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<ac_Task>(requestBody);
            Validate<ac_Task>(requestModel);
            if (!ModelState.IsValid)
            {
                var message = string.Join("\n", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList());
                return Request.CreateResponse(HttpStatusCode.NonAuthoritativeInformation, message);
            }
            var taskRepository = uow.Repository<TaskRepository>();
            var taskData = taskRepository.Get(x => x.TaskId == requestModel.TaskId);
            taskData.IsReminderMail = requestModel.IsReminderMail;
            taskData.IsReminderNotification = requestModel.IsReminderMail;
            taskData.Priority = requestModel.Priority;
            taskData.ReminderDate = requestModel.ReminderDate;
            taskData.ReminderTime = requestModel.ReminderTime;
            taskData.SubTaskId = requestModel.SubTaskId;
            taskData.TaskAssignedBy = requestModel.TaskAssignedBy;
            taskData.EventId = requestModel.EventId;
            taskData.TaskDate = requestModel.TaskDate;
            taskData.TaskDateFrom = requestModel.TaskDateFrom;
            taskData.TaskDateTo = requestModel.TaskDateTo;
            taskData.TaskName = requestModel.TaskName;
            taskData.TaskTimeFrom = requestModel.TaskTimeFrom;
            taskData.TaskTimeTo = requestModel.TaskTimeTo;
            taskData.TeamId = requestModel.TeamId;
            taskRepository.Attach(taskData);



            return Request.CreateResponse(HttpStatusCode.OK, "");
        }

        #endregion

        #region Groups
        [HttpPost]
        public HttpResponseMessage SaveGroup()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<ac_group>(requestBody);
            Validate<ac_group>(requestModel);
            if (!ModelState.IsValid)
            {
                var message = string.Join("\n", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList());
                return Request.CreateResponse(HttpStatusCode.NonAuthoritativeInformation, message);
            }
            var groupRepository = uow.Repository<GroupRepository>();

            if (requestModel.GroupId > 0)
            {
                var groupData = groupRepository.Get(x => x.GroupId == requestModel.GroupId);
                groupData.GroupName = requestModel.GroupName;
                groupRepository.Attach(groupData);
            }
            else
            {
                groupRepository.Add(requestModel);
            }
            return Request.CreateResponse(HttpStatusCode.OK, "");
        }
        [HttpPost]
        public HttpResponseMessage DeleteGroup()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);
            var requestModel = JsonConvert.DeserializeObject<ac_group>(requestBody);
            var groupRepository = uow.Repository<GroupRepository>();
            var groupData = groupRepository.Get(x => x.GroupId == requestModel.GroupId);
            groupRepository.Delete(groupData);
            return Request.CreateResponse(HttpStatusCode.OK, "");
        }
        #endregion
        public static bool SendEmailData(string subject, string email, string link)
        {
            try
            {
                var EmailSubj = subject;
                var EmailID = email;
                Email.SendMail(EmailSubj, EmailID, link);
                return true;
            }
            catch (Exception )
            {
                return false;
            }
        }
    }
}
