﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EventogenieWebsites.Models;
using Newtonsoft.Json;
using System.IO;
using System.Web;
using System.Text;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Drawing;
using EventogenieWebsites.Entity;
using EventogenieWebsites.Repositories;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Configuration;
using System.Data;

namespace EventogenieWebsites.Controllers
{
    public class TaskManagementController : ApiController
    {
        UnitOfWork uow = new UnitOfWork();



        public string HttpMessage(string requestBody)
        {
            using (var stream = new MemoryStream())
            {
                var context = (HttpContextBase)Request.Properties["MS_HttpContext"];
                context.Request.InputStream.Seek(0, SeekOrigin.Begin);
                context.Request.InputStream.CopyTo(stream);
                requestBody = Encoding.UTF8.GetString(stream.ToArray());
            }
            return requestBody;
        }
        [HttpPost]
        public HttpResponseMessage SaveNewGuest()
        {
            var requestBody = string.Empty;
            requestBody = HttpMessage(requestBody);

            var requestModel = JsonConvert.DeserializeObject<GuestModel>(requestBody);
            Validate<GuestModel>(requestModel);
            var aa = requestModel.IdProof;
            if (!ModelState.IsValid)
            {
                var message = string.Join("\n", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList());
                return Request.CreateResponse(HttpStatusCode.NonAuthoritativeInformation, message);
            }

            var guestRepository = uow.Repository<GuestRepository>();

            Guest task = new Guest();
            //for image  save on server
            //if (requestModel.IdProof != null)
            //{
            //    var bytes = Convert.FromBase64String(requestModel.IdProof);
            //    string url = HttpContext.Current.Server.MapPath("~/SaveImages");
            //    if (!System.IO.Directory.Exists(url))
            //        System.IO.Directory.CreateDirectory(url);
            //    string path = HttpContext.Current.Server.MapPath("~/SaveImages");
            //    using (var ms = new MemoryStream(bytes, 0, bytes.Length))
            //    {
            //        System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
            //        image.Save(path + "\\" + Guid.NewGuid() + requestModel.ImageName, System.Drawing.Imaging.ImageFormat.Png);
            //        task.IdProof = Guid.NewGuid() + requestModel.ImageName;
            //    }

            //}
            
            task.GuestNo = requestModel.GuestNo;
            task.Address = requestModel.Address;
            task.Pincode = requestModel.Pincode;
            task.EmailId = requestModel.EmailId;
            task.City = requestModel.City;
            task.GuestGroup = requestModel.GuestGroup;
            task.GuestName = requestModel.GuestName;
            task.WhatsAppNo = requestModel.WhatsAppNo;
            task.ContactNo = requestModel.ContactNo;
            task.GuestStatus = "Guest Added";
            task.AnySpecialRequiremnt = requestModel.AnySpecialRequiremnt;
            task.CreatedOn = DateTime.Now;
            guestRepository.Add(task);
            return Request.CreateResponse(HttpStatusCode.OK, "");
        }
        [HttpGet]
        public List<Guest> GetAllGuestListStatus()
        {
            var teamMemberRepository = uow.Repository<GuestRepository>();
            var teamData = teamMemberRepository.GetAll().ToList();
            return teamData;
        }

        [HttpGet]
        public List<Guest> GetAllGuestListDetail()
        {
            var teamMemberRepository = uow.Repository<GuestRepository>();
            var teamData = teamMemberRepository.GetAll().ToList();
            return teamData;
        }
        

        [HttpPost]
        public HttpResponseMessage InsertVenue()
        {
            //    var requestBody = string.Empty;
            //    requestBody = HttpMessage(requestBody);
            //    var requestModel = JsonConvert.DeserializeObject<VenueManagePages>(requestBody);
            //    Validate<VenueManagePages>(requestModel);
            //    if (!ModelState.IsValid)
            //    {
            //        var message = string.Join("\n", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList());
            //        return Request.CreateResponse(HttpStatusCode.NonAuthoritativeInformation, message);
            //    }
            //    var taskRepository = uow.Repository<VenueManageRepository>();

            //    u_VenueManagement VManager = new u_VenueManagement();
            //    var values = requestModel.ETID.ToString();
            //    if (values.StartsWith("[")) values = values.Substring(1);
            //    if (values.EndsWith("]")) values = values.Substring(0, values.Length - 1);
            //    requestModel.ETID = values;
            //    VManager.VenueName = requestModel.VenueName;
            //    VManager.VenueAddress = requestModel.VenueAddress;
            //    VManager.VLng = requestModel.VLng;
            //    VManager.VLong = requestModel.VLong;
            //    VManager.ETID = requestModel.ETID;
            //    VManager.SpecialityTags = requestModel.SpecialityTags;
            //    VManager.ContactNo = requestModel.ContactNo;
            //    VManager.ContactTimeFrom = requestModel.ContactTimeFrom;
            //    VManager.ContactTimeTo = requestModel.ContactTimeTo;
            //    VManager.VenueDetails = requestModel.VenueDetails;
            //    VManager.CreatedBy = requestModel.CreatedBy;
            //    VManager.EmailId = requestModel.EmailId;
            //    VManager.OutLocation = requestModel.OutLocation;
            //    //task.Priority = requestModel.Priority;
            //    taskRepository.Add(VManager);

            //    var VenueTypeRepository = uow.Repository<u_VenueTypeRepository>();
            //    foreach (var venueType in requestModel.listVenueType)
            //    {
            //        if (requestModel.listVenueType[0].FCapacity != 0)
            //        {
            //            venueType.VenueId = VManager.VenueId;
            //            venueType.CreatedBy = VManager.CreatedBy;
            //            VenueTypeRepository.Add(venueType);
            //        }
            //    }
            //    var stayRepository = uow.Repository<StaySubVenueRepository>();
            //    foreach (var stayVenueType in requestModel.listStaySubVenue)
            //    {
            //        if (requestModel.listStaySubVenue[0].SQuantity != 0)
            //        {
            //            stayVenueType.VenueId = VManager.VenueId;
            //            stayVenueType.CreatedBy = VManager.CreatedBy;
            //            stayRepository.Add(stayVenueType);
            //        }
            //    }
            return Request.CreateResponse(HttpStatusCode.OK, "");
        }

        [HttpPost]
        public HttpResponseMessage SaveBulkData(List<Guest> model)
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("EventID", typeof(string));
                dt.Columns.Add("GuestName", typeof(string));
                dt.Columns.Add("City", typeof(string));
                dt.Columns.Add("GuestGroup", typeof(string));
                dt.Columns.Add("ContactNo", typeof(decimal));
                dt.Columns.Add("WhatsAppNo", typeof(decimal));
                dt.Columns.Add("CreatedOn", typeof(DateTime));
                dt.Columns.Add("CreatedBy", typeof(int));
                dt.Columns.Add("GuestStatus", typeof(string));
                dt.Columns.Add("GuestNo", typeof(string));

                foreach (var item in model)
                {
                    DataRow dr = dt.NewRow();
                    dr["EventID"] = 0;
                    dr["GuestName"] = item.GuestName;
                    dr["City"] = item.City;
                    dr["GuestGroup"] = item.GuestGroup;
                    dr["ContactNo"] = item.ContactNo;
                    dr["WhatsAppNo"] = item.WhatsAppNo;
                    dr["CreatedOn"] = DateTime.Now;
                    dr["CreatedBy"] = "123";
                    dr["GuestStatus"] = "Guest Added";
                    dr["GuestNo"] = item.GuestNo;

                    dt.Rows.Add(dr);

                }

                string conString = ConfigurationManager.ConnectionStrings["EvetovitiDatabase"].ConnectionString;
                using (SqlConnection con = new SqlConnection(conString))
                {
                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                    {
                        sqlBulkCopy.DestinationTableName = "dbo.Guest";

                        sqlBulkCopy.ColumnMappings.Add("EventID", "EventID");
                        sqlBulkCopy.ColumnMappings.Add("GuestName", "GuestName");
                        sqlBulkCopy.ColumnMappings.Add("City", "City");
                        sqlBulkCopy.ColumnMappings.Add("GuestGroup", "GuestGroup");
                        sqlBulkCopy.ColumnMappings.Add("ContactNo", "ContactNo");
                        sqlBulkCopy.ColumnMappings.Add("WhatsAppNo", "WhatsAppNo");
                        sqlBulkCopy.ColumnMappings.Add("CreatedOn", "CreatedOn");
                        sqlBulkCopy.ColumnMappings.Add("CreatedBy", "CreatedBy");
                        sqlBulkCopy.ColumnMappings.Add("GuestStatus", "GuestStatus");
                        sqlBulkCopy.ColumnMappings.Add("GuestNo", "GuestNo");
                        con.Open();
                        sqlBulkCopy.WriteToServer(dt);
                        con.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                
            }
            return Request.CreateResponse(HttpStatusCode.OK, "");
        }
        [HttpPost]
        public object GetGuestGroup(string Prefix)
        {

            string connectionString = ConfigurationManager.ConnectionStrings["String"].ConnectionString;
            GuestModel Countries = new GuestModel();
            Countries.GuestModel_list = new List<GuestModel>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    

                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.CommandText = "select ";
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@search", SqlDbType.NVarChar).Value = Prefix;

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        GuestModel tempList = new GuestModel();
                        tempList.id = Convert.ToInt32(reader["id"]);
                        tempList.GuestName = reader["name"].ToString();
                        Countries.GuestModel_list.Add(tempList);
                    }

                    connection.Close();
                }

                catch (Exception ex)
                {
                    connection.Close();
                    throw ex;
                }
            }
            var Countrie = (from c in Countries.GuestModel_list.ToList()
                            select new
                            {
                                label = c.GuestName,
                                val = c.id
                            }).ToList();

            return Json(Countrie);
        }
    }
}