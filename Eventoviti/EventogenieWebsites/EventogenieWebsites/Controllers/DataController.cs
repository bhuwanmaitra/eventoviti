﻿using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.Mvc;
using EventogenieWebsites.Entity;
using EventogenieWebsites.Models;
using EventogenieWebsites.Repositories;

namespace EventogenieWebsites.Controllers
{
    public class DataController : Controller
    {
        UnitOfWork uow = new UnitOfWork();
        // GET: Data
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult SaveAndUpload()
        {
            var path = SaveFiles(Request.Files[0], "~/ExcelFiles/");
            SaveToExecl(path);
            return new JsonResult { Data = new { Message = "Ok", Status = true } };
        }
        [HttpPost]
        public JsonResult SaveMultipleImages()
        {
            string fileName = string.Empty;
            for (int i = 0; i < Request.Files.Count; i++)
            {
                var path = SaveFiles(Request.Files[i], "~/SaveImages/MultipleImages/");
                //save file path into database 
                var guestRepository = uow.Repository<EventImagesVideoFilesRepository>();
                EventImagesVideoFiles task = new EventImagesVideoFiles();
                task.EventId = 0;
                task.FilePath = path;
                task.Type = "Images";
                task.CreatedBy= 123;
                task.CreatedOn = DateTime.Now;
                guestRepository.Add(task);
            }

            return new JsonResult { Data = new { Message = "Ok", Status = true } };
        }
        [HttpPost]
        public JsonResult SaveLogo()
        {
            var path = SaveFiles(Request.Files[0], "~/SaveImages/LogoImages/");
            //save file path into database 
            var guestRepository = uow.Repository<EventImagesVideoFilesRepository>();
            EventImagesVideoFiles task = new EventImagesVideoFiles();
            task.EventId = 0;
            task.FilePath = path;
            task.Type = "Logo";
            task.CreatedBy = 123;
            task.CreatedOn = DateTime.Now;
            guestRepository.Add(task);
            return new JsonResult { Data = new { Message = "Ok", Status = true } };
        }
        [HttpPost]
        public string SaveFiles(HttpPostedFileBase file, string direcoryPath)
        {
            string Message, fileName, actualFileName;
            Message = fileName = actualFileName = string.Empty;
            bool flag = false;
            string path = string.Empty;
            if (Request.Files != null)
            {
                //var file = Request.Files[0];
                actualFileName = file.FileName;
                fileName = Guid.NewGuid() + Path.GetExtension(file.FileName);
                int size = file.ContentLength;

                try
                {
                    string Dirurl = Server.MapPath(direcoryPath);
                    if (!Directory.Exists(Dirurl))
                        Directory.CreateDirectory(Dirurl);
                    path = Path.Combine(Dirurl, fileName);

                    file.SaveAs(path);
                    path = direcoryPath + fileName;
                }
                catch (Exception ex)
                {
                    path = string.Empty;
                    Message = "File upload failed! Please try again";
                }
            }

            return path;
        }
        private void SaveToExecl(string filePath)
        {
            string extension = Path.GetExtension(filePath);
            string conString = string.Empty;
            switch (extension)
            {
                case ".xls": //Excel 97-03.
                    conString = ConfigurationManager.ConnectionStrings["Con2003"].ConnectionString;
                    break;
                case ".xlsx": //Excel 07 and above.
                    conString = ConfigurationManager.ConnectionStrings["Con2007"].ConnectionString;
                    break;
            }

            DataTable dt = new DataTable();
            conString = string.Format(conString,Server.MapPath(filePath));

            using (OleDbConnection connExcel = new OleDbConnection(conString))
            {
                using (OleDbCommand cmdExcel = new OleDbCommand())
                {
                    using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                    {
                        cmdExcel.Connection = connExcel;
                        connExcel.Open();
                        DataTable dtExcelSchema;
                        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                        connExcel.Close();
                        connExcel.Open();
                        cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
                        odaExcel.SelectCommand = cmdExcel;
                        odaExcel.Fill(dt);
                        connExcel.Close();
                    }
                }
            }

            conString = ConfigurationManager.ConnectionStrings["EvetovitiDatabase"].ConnectionString;
            using (SqlConnection con = new SqlConnection(conString))
            {
                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                {
                    sqlBulkCopy.DestinationTableName = "dbo.Guest";
                    dt.Columns.Add("CreatedOn", typeof(DateTime));
                    dt.Columns.Add("CreatedBy", typeof(int));
                    dt.Columns.Add("EventID", typeof(decimal));
                    dt.Columns.Add("GuestStatus", typeof(string));
                    foreach (DataRow dr in dt.Rows)
                    {
                        dr["EventID"] = "0";
                        dr["CreatedOn"] = DateTime.Now;
                        dr["GuestStatus"] = "Guest Added";
                        dr["CreatedBy"] = 123;

                    }

                    sqlBulkCopy.ColumnMappings.Add("GuestName", "GuestName");
                    sqlBulkCopy.ColumnMappings.Add("City", "City");
                    sqlBulkCopy.ColumnMappings.Add("GuestNo", "GuestNo");
                    sqlBulkCopy.ColumnMappings.Add("GuestGroup", "GuestGroup");
                    sqlBulkCopy.ColumnMappings.Add("ContactNo", "ContactNo");
                    sqlBulkCopy.ColumnMappings.Add("WhatsAppNo", "WhatsAppNo");
                    sqlBulkCopy.ColumnMappings.Add("CreatedBy", "CreatedBy");
                    sqlBulkCopy.ColumnMappings.Add("CreatedOn", "CreatedOn");
                    sqlBulkCopy.ColumnMappings.Add("EventID", "EventID");
                    sqlBulkCopy.ColumnMappings.Add("GuestStatus", "GuestStatus");
                    con.Open();
                    sqlBulkCopy.WriteToServer(dt);
                    con.Close();
                }
            }
        }
    }
}