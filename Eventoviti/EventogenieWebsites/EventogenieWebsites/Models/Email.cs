﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace EventogenieWebsites.Models
{
    public class Email
    {
        static UnitOfWork uow = new UnitOfWork();
        public static string SendMail(string emailSubject, string emailIDs, string link)
        {
            var emailFromAddressData = "Shrivastava.aditya002@gmail.com";
            var emailFromCompanyData = "Eventoviti";
            var SMTPUser = "Shrivastava.aditya002@gmail.com";
            var SMTPPassword = "Aditya@002";
            var SMTPPort = "587";
            var SMTPServer = "smtp.gmail.com";

            string emailId = emailIDs;
            MailMessage mail = new MailMessage();
            SmtpClient smtpServer = new SmtpClient(SMTPServer);
            smtpServer.UseDefaultCredentials = true;
            smtpServer.Credentials = new System.Net.NetworkCredential(SMTPUser, SMTPPassword);

            StringBuilder sbContect = new StringBuilder();
            sbContect.Append("Dear Sir/Madam");
            sbContect.AppendLine("<br/>");
            sbContect.AppendLine("<br/>");
            sbContect.AppendLine("<br/>");
            sbContect.Append("You are Register in Eventoviti for more, Please click on below link to join.");
            sbContect.AppendLine("<br/>");
            sbContect.AppendLine("<br/>");
            if (emailFromCompanyData == "Eventoviti")
                sbContect.Append(link);
            else
                sbContect.Append(link);
            sbContect.AppendLine("<br/>");
            sbContect.AppendLine("<br/>");
            sbContect.AppendLine("If your email client does not support internet links, please copy and paste above link to the Address Bar.");
            sbContect.AppendLine("<br/>");
            sbContect.AppendLine("<br/>");
            sbContect.Append("Thank you");
            sbContect.AppendLine("<br/>");
            sbContect.Append(emailFromCompanyData + " Team");
            sbContect.AppendLine("<br/>");
            sbContect.AppendLine("<br/>");
            sbContect.AppendLine("<br/>");
            sbContect.AppendLine("<br/>");
            sbContect.AppendLine("----");
            sbContect.AppendLine("<br/>");
            sbContect.Append("CONFIDENTIALITY NOTICE:  This email message and any accompanying data or files is confidential and may contain privileged information intended only for the named recipient(s). If you are not the intended recipient(s), you are hereby notified that the dissemination, distribution, and or copying of this message is strictly prohibited. If you receive this message in error, or are not the named recipient(s), please notify the sender at the email address above, delete this email from your computer, and destroy any copies in any form immediately. Receipt by anyone other than the named recipient(s) is not a waiver of any attorney-client, work product, or other applicable privilege.");
            

            smtpServer.Port = Convert.ToInt32(SMTPPort);
            smtpServer.EnableSsl = true;

            mail.From = new MailAddress(emailFromAddressData, emailFromCompanyData);
            mail.IsBodyHtml = true;
            mail.To.Add(emailId);
            mail.Subject = emailSubject;
            mail.Body = sbContect.ToString();
            mail.Priority = MailPriority.High;
            Task.Factory.StartNew(() => smtpServer.Send(mail), TaskCreationOptions.LongRunning);
            return "Success";
        }
    }
}