﻿using EventogenieWebsites.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EventogenieWebsites.Models
{
    public class TaskModel
    {
        public int TaskId { get; set; }
        [Required(ErrorMessage = "Task Name Is Required")]
        public string TaskName { get; set; }
        public DateTime? TaskDate { get; set; }
        public DateTime? ReminderDate { get; set; }
        public TimeSpan? ReminderTime { get; set; }
        public bool? IsReminderNotification { get; set; }
        public bool? IsReminderMail { get; set; }
        public int? SubTaskId { get; set; }
        [Required(ErrorMessage = "Please Select Team Name")]
        public int? TeamId { get; set; }
        //public int? Priority { get; set; }
        [Required(ErrorMessage = "Please Select Event")]
        public int? EventId { get; set; }
        public int? TaskAssignedBy { get; set; }
        [Required(ErrorMessage = "Task Date From Is Required")]
        public DateTime? TaskDateFrom { get; set; }
        [Required(ErrorMessage = "Task Date To Is Required")]
        public DateTime? TaskDateTo { get; set; }
        public TimeSpan? TaskTimeFrom { get; set; }
        public TimeSpan? TaskTimeTo { get; set; }
        public List<ac_SubTask> lstTaskTeam { get; set; }
        public Constant.GlobalEnum.enmPriority Priority { get; set; }
    }
}