﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using EventogenieWebsites.Models;
using EventogenieWebsites.Entity;

namespace EventogenieWebsites.Models
{
    public class DataBaseContext : DbContext
    {
        //private DbSet<rp_users> rp_users { get; set; }
        private DbSet<u_Team> u_Team { get; set; }
        private DbSet<u_Roles> u_Roles { get; set; }
        private DbSet<ac_group> ac_group { get; set; }
        private DbSet<u_Event> u_Event { get; set; }
        private DbSet<ac_City> ac_City { get; set; }
        private DbSet<u_VenueManagement> u_VenueManagement { get; set; }
        private DbSet<u_VenueSchedule> u_VenueSchedule { get; set; }
        private DbSet<ac_VenueType> ac_VenueType { get; set; }
        private DbSet<u_VenueAccientType> u_VenueAccientType { get; set; }
        private DbSet<u_VenueGallery> u_VenueGallery { get; set; }
        private DbSet<u_Packages> u_Packages { get; set; }
        private DbSet<u_VenuePrivacyPolicy> u_VenuePrivacyPolicy { get; set; }
        private DbSet<ac_VebdorSubType> ac_VebdorSubType { get; set; }
        private DbSet<ac_EventCategoryType> ac_EventCategoryType { get; set; }
        private DbSet<Vendor_Registration> Vendor_Registration { get; set; }
        private DbSet<VRegistrationType> VRegistrationType { get; set; }
        public DbSet<u_StaySubVenue> u_StaySubVenue { get; set; }
        private DbSet<ac_Highlights> ac_Highlights { get; set; }
        private DbSet<u_Highlights> u_Highlights { get; set; }

        private DbSet<Guest> Guest { get; set; }
        private DbSet<EventImagesVideoFiles> EventImagesVideoFiles { get; set; }

        public DataBaseContext()
            : base("Name=EvetovitiDatabase")
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
}