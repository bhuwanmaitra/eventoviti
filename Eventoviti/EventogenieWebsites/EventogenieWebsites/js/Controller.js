﻿var app = angular.module('EvetovitiWebsiteModel', ['ngRoute', 'gm', 'textAngular', 'naif.base64']);
////////// Directive /////////////////////
app.directive("fileread", [
    function () {
        return {
            scope: {
                fileread: "="
            },
            link: function (scope, element, attributes) {
                element.bind("change", function (changeEvent) {
                    var reader = new FileReader();
                    reader.onload = function (loadEvent) {
                        scope.$apply(function () {
                            scope.fileread = loadEvent.target.result;
                        });
                    }
                    reader.readAsDataURL(changeEvent.target.files[0]);
                });
            }
        }
    }
]);
app.directive('datetimepicker', [
    function () {
        var link;
        link = function (scope, element, attr, ngModel) {

            //  element.datetimepicker();
            element.on('dp.change', function (event) {
                scope.$apply(function () {

                    var d = event.date._d;
                    ngModel.$setViewValue(d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate() + ' ' + d.toString().split(' ')[4]);
                });
            });
        };

        return {
            restrict: 'A',
            link: link,
            require: 'ngModel'
        };
    }
]);
app.directive('timepicker', [
    function () {
        var link;
        link = function (scope, element, attr, ngModel) {

            //element.datetimepicker();
            element.on('dp.change', function (event) {
                console.log("scope obj below");
                console.log(scope);
                console.log("ngModel obj below");
                console.log(ngModel);
                //need to run digest cycle for applying bindings
                scope.$apply(function () {

                    var d = event.date._d;
                    ngModel.$setViewValue(d.toString().split(' ')[4]);
                });
            });
        };

        return {
            restrict: 'A',
            link: link,
            require: 'ngModel'
        };
    }
])
app.directive('date', function (dateFilter) {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {

            var dateFormat = attrs['date'] || 'yyyy-MM-dd';

            ctrl.$formatters.unshift(function (modelValue) {
                return dateFilter(modelValue, dateFormat);
            });
        }
    };
})
app.directive('notification', ['$timeout', function ($timeout) {
    return {
        restrict: 'E',
        template: "<div class='alert alert-{{alertData.type}}' ng-show='alertData.message' role='alert' data-notification='{{alertData.status}}'>{{alertData.message}}</div>",
        scope: {
            alertData: "="
        }
    };
}]);
app.directive('ngFileModel', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.ngFileModel);
            var isMultiple = attrs.multiple;
            var modelSetter = model.assign;
            element.bind('change', function () {
                var values = [];
                var reader = new FileReader();

                angular.forEach(element[0].files, function (item) {
                    var value = {
                        // File Name 
                        name: item.name,
                        //File Size 
                        size: item.size,
                        //File URL to view 
                        url: URL.createObjectURL(item),
                        // File Input Value 
                        _file: item
                    };
                    values.push(value);
                });
                scope.$apply(function () {
                    if (isMultiple) {
                        modelSetter(scope, values);
                    } else {
                        modelSetter(scope, values[0]);
                    }
                });
            });
        }
    };
});
app.filter('unique', function () {
    // we will return a function which will take in a collection
    // and a keyname
    return function (collection, keyname) {
        // we define our output and keys array;
        var output = [],
            keys = [];

        // we utilize angular's foreach function
        // this takes in our original collection and an iterator function
        angular.forEach(collection, function (item) {
            // we check to see whether our object exists
            var key = item[keyname];
            // if it's not already part of our keys array
            if (keys.indexOf(key) === -1) {
                // add it to our keys array
                keys.push(key);
                // push this item to our final output array
                output.push(item);
            }
        });
        // return our array which should be devoid of
        // any duplicates
        return output;
    };
});
app.directive('readMore', function () {
    return {
        restrict: 'A',
        transclude: true,
        replace: true,
        template: '<div></div>',
        scope: {
            moreText: '@',
            lessText: '@',
            words: '@',
            ellipsis: '@',
            char: '@',
            limit: '@',
            content: '@'
        },
        link: function (scope, elem, attr, ctrl, transclude) {
            var moreText = angular.isUndefined(scope.moreText) ? ' <a class="read-more">Read More...</a>' : ' <a class="read-more">' + scope.moreText + '</a>',
                lessText = angular.isUndefined(scope.lessText) ? ' <a class="read-less">Less ^</a>' : ' <a class="read-less">' + scope.lessText + '</a>',
                ellipsis = angular.isUndefined(scope.ellipsis) ? '' : scope.ellipsis,
                limit = angular.isUndefined(scope.limit) ? 10 : scope.limit;

            attr.$observe('content', function (str) {
                readmore(str);
            });

            transclude(scope.$parent, function (clone, scope) {
                readmore(clone.text().trim());
            });

            function readmore(text) {

                var text = text,
                    orig = text,
                    regex = /\s+/gi,
                    charCount = text.length,
                    wordCount = text.trim().replace(regex, ' ').split(' ').length,
                    countBy = 'char',
                    count = charCount,
                    foundWords = [],
                    markup = text,
                    more = '';

                if (!angular.isUndefined(attr.words)) {
                    countBy = 'words';
                    count = wordCount;
                }

                if (countBy === 'words') { // Count words

                    foundWords = text.split(/\s+/);

                    if (foundWords.length > limit) {
                        text = foundWords.slice(0, limit).join(' ') + ellipsis;
                        more = foundWords.slice(limit, count).join(' ');
                        markup = text + moreText + '<span class="more-text">' + more + lessText + '</span>';
                    }

                } else { // Count characters

                    if (count > limit) {
                        text = orig.slice(0, limit) + ellipsis;
                        more = orig.slice(limit, count);
                        markup = text + moreText + '<span class="more-text">' + more + lessText + '</span>';
                    }

                }

                elem.append(markup);
                elem.find('.read-more').on('click', function () {
                    $(this).hide();
                    elem.find('.more-text').addClass('show').slideDown();
                });
                elem.find('.read-less').on('click', function () {
                    elem.find('.read-more').show();
                    elem.find('.more-text').hide().removeClass('show');
                });

            }
        }
    };
});

app.controller('HomePageCtrl', function ($scope, $location) {
    var vendorTemplate = {
        show: false
    };
    $scope.vendorTemplate = vendorTemplate;
    var userTemplate = {
        show: true
    };
    $scope.userTemplate = userTemplate;
})

app.controller('LoginCtrl', function ($scope, $location, RequestData) {
    $scope.VendorLogin = function () {

        var vEmailId = $scope.VEmailId;
        var vPassword = $scope.vPassword;
        var ResulLoginData = RequestData.loginEventoviti(vPassword, vEmailId, 'vendorLogin');
        ResulLoginData.then(function (pl) {
            if (pl.status == EventovitiCommon.GetResponseStatus('Not Found')) {
                alert(pl.data);
            }
            else {

                // $scope.CategoryList = pl.data;
                localStorage.VendorInfo = pl.data[0].VRId;
                localStorage.VendorImage = pl.data[0].VImage;
                $location.path('/VendorDashboard');
            }
        })
    }
})

app.controller('ChossePageCtrl', function ($scope, $location, RequestData) {
    $scope.employerSingup = function () {
        $location.path('/SignupPage/').search({ id: 'HL' });
    }
    $scope.manageSingup = function () {
        $location.path('/SignupPage/').search({ id: 'MA' });
    }
    $scope.buisnessSingup = function () {
        $location.path('/SignupPage/').search({ id: 'BE' });
    }
    $scope.employeeSingup = function () {
        $location.path('/SignupPage/').search({ id: 'EV' });
    }
})

app.controller('VendorRegistrationCtrl', function ($scope, RequestData, $location, $routeParams) {

    var resultPost = RequestData.selectCategory(0, '', '', 'SelectCategoryType')
    $scope.Registration = {};
    $scope.Reg = {};
    $scope.Reg.E_Cat_ID = '';
    $scope.Reg.VSId = [];
    resultPost.then(function (pl) {
        if (pl.status == EventovitiCommon.GetResponseStatus('Not Found')) {
            alert(pl.data);
        } else {
            $scope.CategoryList = pl.data;
        }
    });

    if ($routeParams.id == 'MA') {
        $scope.regiterName = 'Own Event';
    }
    else if ($routeParams.id == 'BE') {
        $scope.regiterName = 'Business';
    }
    $scope.CategoryClick = function (CatList) {

        var idx = $scope.Reg.E_Cat_ID.indexOf(CatList);

        // is currently selected
        if (idx > -1) {
            $scope.Reg.E_Cat_ID.splice(idx, 1);
        }

        // is newly selected
        else {
            $scope.Reg.E_Cat_ID = CatList.E_Cat_ID;

        }
        var VendorSubData = {
            E_Cat_ID: CatList.E_Cat_ID,
        }
        var resultPosr = RequestData.GetSubVendorList(VendorSubData)
        resultPosr.then(function (pl) {
            if (pl.status == EventovitiCommon.GetResponseStatus('Not Found')) {
                alert(pl.data);
            } else {
                if (pl.data.length != 0) {
                    $scope.VenodrSub = pl.data;
                    //$location.path('/SuccessPage');
                }
                else {
                    $('#selectEventSubType').modal('hide');
                    $scope.Reg.CreatedBy = $scope.Reg.VRId;
                    $scope.Reg.VSId = JSON.stringify($scope.Reg.VSId)
                    var resultPosr = RequestData.SaveVendorType($scope.Reg)
                    resultPosr.then(function (pl) {
                        if (pl.status == EventovitiCommon.GetResponseStatus('Not Found')) {
                            alert(pl.data);
                        } else {
                            if (pl.length != 0) {
                                $scope.Reg.E_Cat_ID = '';
                                $scope.Reg.VSId = [];

                            }
                            else {
                                $scope.Reg.E_Cat_ID = '';
                                $scope.Reg.VSId = [];
                            }
                        }
                    })

                }
            }
        })
    }

    $scope.toggleSelection = function toggleSelection(CatList) {
        var idx = $scope.E_Cat_ID.indexOf(CatList);

        // is currently selected
        if (idx > -1) {
            $scope.Reg.E_Cat_ID.splice(idx, 1);
        }

        // is newly selected
        else {
            $scope.Reg.E_Cat_ID = CatList.E_Cat_ID;

        }
    };
    $scope.toggleSubSelection = function toggleSubSelection(CatList) {
        var idx = $scope.Reg.VSId.indexOf(CatList);

        // is currently selected
        if (idx > -1) {
            $scope.Reg.VSId.splice(idx, 1);
        }

        // is newly selected
        else {
            $scope.Reg.VSId.push(CatList.VSId);

        }
    };
    $('#wizard-picture').on('change', function (evt) {

        var files = $(evt.currentTarget).get(0).files;

        if (files.length > 0) {

            $scope.Registration.uploadme1 = files[0].name;
            console.log($scope.Registration.uploadme1);
        }
    });

    $scope.VRegistration = function () {

        var fd = new FormData();
        $scope.Registration.VImage = $scope.uploadme;
        if ($scope.Registration.VImage.length > 0 || $scope.Registration.VImage != undefined) {
            $scope.Registration.VImage = $scope.Registration.VImage.replace(/data:image\/jpeg;base64,/g, '');
            $scope.Registration.VImage = $scope.Registration.VImage.replace(/data:image\/png;base64,/g, '');
        }
        else {
            $('#images').removeClass('picture').addClass('picture-red')
            return;
        }
        $scope.Registration.ImageName = $scope.Registration.uploadme1;
        $scope.Registration.UserType = $routeParams.id;
        var resultPosr = RequestData.registration($scope.Registration)
        resultPosr.then(function (pl) {
            if (pl.status == EventovitiCommon.GetResponseStatus('Not Found')) {
                alert(pl.data);
            } else {
                if (pl.length != 0) {

                    localStorage.VRId = pl.data;
                    $scope.Reg.VRId = pl.data;
                    // $location.path('/SuccessPage');
                }
                else {

                }
            }
        })

    }
    $scope.SaveVendorCategory = function () {

        $scope.Reg.CreatedBy = $scope.Reg.VRId;
        $scope.Reg.VSId = JSON.stringify($scope.Reg.VSId)
        var resultPosr = RequestData.SaveVendorType($scope.Reg)
        resultPosr.then(function (pl) {
            if (pl.status == EventovitiCommon.GetResponseStatus('Not Found')) {
                alert(pl.data);
            } else {
                if (pl.length != 0) {
                    $scope.Reg.E_Cat_ID = '';
                    $scope.Reg.VSId = [];
                    //localStorage.VRId = pl.data.VRId;
                    //$location.path('/SuccessPage');
                }
                else {
                    $scope.Reg.E_Cat_ID = '';
                    $scope.Reg.VSId = [];
                }
            }
        })
    }
    $scope.FinishedReg = function () {
        $location.path('/SuccessPage');
    }

})

app.controller('vendorMasterCtrl', function ($scope, $location) {
    var vendorTemplate = {
        show: true
    };
    $scope.vendorTemplate = vendorTemplate;
    var userTemplate = {
        show: false
    };
    $scope.userTemplate = userTemplate;
    $location.path('/VendorDashboard');
})

app.controller('userDashboardCtrl', function ($scope, $location, $window) {


    $scope.tab = 1;

    $scope.setTab = function (newTab) {

        $scope.tab = newTab;
    };

    $scope.isSet = function (tabNum) {
        return $scope.tab === tabNum;
    };
})

app.controller('VendorDashboardCtrl', function ($scope) {

})

////Controlers For Create New Event For Vendor ////
app.controller('CreateNewEvent', function ($scope, RequestData, $location, Map) {
    $scope.Customers = [{ MemberEmail: '', MemberName: '', RoleId: '', GroupId: '' }];
    $scope.DataModel = { CitData: { CityName: "Select City", CityId: "0" } }
    $('#wizard-picture').on('change', function (evt) {

        var files = $(evt.currentTarget).get(0).files;

        if (files.length > 0) {

            $scope.uploadme1 = files[0].name;
            console.log($scope.uploadme1);
        }
    });
    $scope.eventTypeSelection = function eventTypeSelection(eData) {

        var idx = $scope.eventTypeSelect.indexOf(eData);

        // is currently selected
        if (idx > -1) {
            $scope.eventTypeSelect.splice(idx, 1);
        }

        // is newly selected
        else {
            $scope.eventTypeSelect.push(eData.ETID);

        }
    };
    $scope.featureSelection = function featureSelection(eData) {

        var idx = $scope.featureSelect.indexOf(eData);

        // is currently selected
        if (idx > -1) {
            $scope.featureSelect.splice(idx, 1);
        }

        // is newly selected
        else {
            $scope.featureSelect.push(eData.FId);

        }
    };
    $scope.teamGroupSelection = function teamGroupSelection(eData, cat) {

        var idx = $scope.teamGroupSelect.indexOf(eData);
        var idxx = $scope.groupSelect.indexOf(eData);

        // is currently selected
        if (idx > -1) {
            $scope.teamGroupSelect.splice(idx, 1);
            $scope.groupSelect.splice(idxx, 1);
        }

        // is newly selected
        else {
            $scope.teamGroupSelect.push(eData.TeamId);
            $scope.groupSelect.push(cat.Groups.GroupId);
        }
    };
    $scope.getCity = function () {
        var ResultData = RequestData.CityName('selectCityName')
        ResultData.then(function (pl) {
            if (pl.status == EventovitiCommon.GetResponseStatus('Not Found')) {
                alert(pl.data);
            }
            else {
                $scope.CityData = pl.data;
            }
        })
    }
    var vendorId = localStorage.VendorInfo;
    $scope.getVendorCategory = function () {
        var ResultData = RequestData.vendorCategory(vendorId, 'selectvendorCatrgory')
        ResultData.then(function (pl) {

            if (pl.status == EventovitiCommon.GetResponseStatus('Not Found')) {
                alert(pl.data)
            }
            else {
                $scope.eventCategoryData = pl.data;
            }
        })
    }
    $scope.getEventType = function (cat) {
        var ETID = cat.E_Cat_ID
        var ResultData = RequestData.eventType(ETID, vendorId, 'SelectEventType')
        ResultData.then(function (pl) {

            if (pl.status == EventovitiCommon.GetResponseStatus('Not Found')) {
                alert(pl.data)
            }
            else {
                $scope.evenTypeData = pl.data;
            }
        })
    }
    $scope.getCity();
    $scope.getVendorCategory();
    $scope.getFType = function () {
        var resultData = RequestData.featureType('SelectFeatures')
        resultData.then(function (pl) {

            if (pl.status == EventovitiCommon.GetResponseStatus('Not Found')) {
                alert(pl.data)
            }
            else {
                $scope.featureData = pl.data;
            }
        })
    }
    $scope.Roles = function () {
        var resultRoles = RequestData.GetRoles();
        resultRoles.then(function (pl) {

            $scope.RolesList = pl.data;
        }, function () {

        })
    }
    $scope.Groups = function () {
        var resultRoles = RequestData.GetGroups();
        resultRoles.then(function (pl) {
            $scope.Groups = pl.data;
        }, function () {

        })
    }
    $scope.ViewTeamGroup = function () {
        var viewTeam = RequestData.GetTeamMember($scope.Customers);
        viewTeam.then(function (pl) {

            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                $scope.Group = pl.data;
                sessionStorage.Group = JSON.stringify(pl.data);
            }
        }, function () {
        })
    }
    $scope.EditEvent = function (cal) {
        var EventID = cal.EventID
        var viewTeam = RequestData.EditAllEvent(EventID);
        viewTeam.then(function (pl) {

            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                $scope.EditEvent = pl.data;
                sessionStorage.EditEvent = JSON.stringify(pl.data);
                $location.path('/ViewEvent');
            }
        }, function () {
        })
    }
    $scope.ViewTeamMembers = function () {
        var viewTeam = RequestData.GetTeamMember($scope.Customers);
        viewTeam.then(function (pl) {

            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                $scope.TeamMembers = pl.data;
                sessionStorage.TeamMembers = JSON.stringify(pl.data);
            }
        }, function () {
        })
    }
    $scope.inviteHost = function () {
        $scope.isHostInvited = 'true'
    }
    $scope.noInviteHost = function () {
        $scope.isHostInvited = 'false'
    }

    $scope.SendInvite = function () {
        var resultData = RequestData.SendInviteToOwner('Invite Owner', $scope.hostEmailId, 'http://localhost:60284/index.html#!/SignupPage?id=HL')
        resultData.then(function (pl) {
            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                alert("Send invite to the owner succesfully");
            }
            else {
                alert(pl.data);
            }
        })
    }
    //$scope.getEventType();
    $scope.getFType();
    $scope.Roles();
    $scope.Groups();
    $scope.ViewTeamGroup();
    $scope.GetAllEvent = function () {
        var CreateBy = vendorId;
        var resultAllEvent = RequestData.GetAllEvent(CreateBy);
        resultAllEvent.then(function (pl) {

            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                $scope.AllEvents = pl.data;
                sessionStorage.AllEvents = JSON.stringify(pl.data);
            }
        })
    }
    $scope.GetAllEvent();
    $scope.lat = undefined;
    $scope.lng = undefined;
    $scope.$on('gmPlacesAutocomplete::placeChanged', function () {

        Map.search($scope.autocomplete.getPlace().formatted_address)
            .then(function (res) { // success

                Map.addMarker(res);
                //$scope.place.name = res.name;
                $scope.lat = res.geometry.location.lat();
                $scope.lng = res.geometry.location.lng();
                $scope.$apply();
            },
            function (status) { // error
                $scope.apiError = true;
                $scope.apiStatus = status;
            });
    });
    Map.init();
    ///Save event
    $scope.saveEvent = function () {
        var imgBlob = $scope.uploadme;
        if (imgBlob.length > 0) {
            imgBlob = imgBlob.replace(/data:image\/jpeg;base64,/g, '');
            imgBlob = imgBlob.replace(/data:image\/png;base64,/g, '');
        }
        var EProfilePic = imgBlob;
        var EName = $scope.eventname;
        var EOwnerName = $scope.eventOwnerName;
        var EDateFrom = $scope.eventDateFrom;
        var EDateTo = $scope.eventDateTo;
        var EAddress = $scope.autocomplete.getPlace().formatted_address;
        var ELan = $scope.lat
        var ELang = $scope.lng
        var EGuestCount = $scope.eventGurest;
        var EType = JSON.stringify($scope.eventTypeSelect);
        var GroupId = JSON.stringify($scope.groupSelect);
        var TeamId = JSON.stringify($scope.teamGroupSelect);
        var isEHost = $scope.isHostInvited;
        var EHostEmail = $scope.hostEmailId;
        var EFeatureId = JSON.stringify($scope.featureSelect);
        var ImageName = $scope.uploadme1;
        var CreateBy = vendorId;
        var tempData = {
            EProfilePic: EProfilePic,
            EName: EName,
            EOwnerName: EOwnerName,
            EDateFrom: EDateFrom,
            EDateTo: EDateTo,
            EAddress: EAddress,
            ELan: ELan,
            ELang: ELang,
            EGuestCount: EGuestCount,
            EType: EType,
            GroupId: GroupId,
            TeamId: TeamId,
            isEHost: isEHost,
            EHostEmail: EHostEmail,
            EFeatureId: EFeatureId,
            ImageName: ImageName,
            CreateBy: CreateBy
        }
        var EventData = JSON.parse(JSON.stringify(tempData));
        var saveTeam = RequestData.AddNewEvent(EventData);
        saveTeam.then(function (pl) {

            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                alert('Your event is added succesfully');
                $('#btnAdd').css('display', 'block')
                $('#ShowData').css('display', 'block')
                $('#addData').css('display', 'none')
                $('#btnView').css('display', 'none')
                $scope.GetAllEvent();
            }
            else {
                alert(pl.data);
            }
        }, function () {
        })
        $scope.eventTypeSelect = [];
        $scope.featureSelect = [];
        $scope.teamGroupSelect = [];
        $scope.groupSelect = [];
    }
    $scope.eventTypeSelect = [];
    $scope.featureSelect = [];
    $scope.teamGroupSelect = [];
    $scope.groupSelect = [];
})

///// Created By Vikas Vanvi Team Members ////////////////////
app.controller('CreateTeamCtrl', function ($scope, $window, RequestData) {

    $scope.IsEdit = false;
    $scope.Customers = [{ MemberEmail: '', MemberName: '', RoleId: '', GroupId: '' }];
    $scope.currentRow = $scope.Customers.length - 1;
    $scope.Add = function () {
        debugger;
        $scope.row = $scope.Customers.length - 1;
        //Add the new item to the Array.
        //if ($scope.Emailid) { }     
        if ($scope.Customers[$scope.Customers.length - 1].MemberEmail && $scope.Customers[$scope.Customers.length - 1].MemberName && $scope.Customers[$scope.Customers.length - 1].RoleId && $scope.Customers[$scope.Customers.length - 1].GroupId) {
            $scope.currentRow = $scope.Customers.length;
            $scope.Customers.push({ MemberEmail: '', MemberName: '', RoleId: '', GroupId: '' });
            $scope.changeBorder($scope.row, 'none');
        }
        else {
            $scope.changeBorder($scope.row, '1px solid red');
        }
    };

    $scope.changeBorder = function (row, type) {
        //$('#tm_email_' + row).parent('div').addClass('has-error');
        //$('#tm_name_' + row).parent('div').addClass('has-error');
        //$('#tm_role_' + row).parent('div').addClass('has-error');
        //$('#tm_group_' + row).parent('div').addClass('has-error');
        //$('#tm_role_' + row).parent('div').addClass('has-error');
        $('#tm_email_' + row).css('border-bottom', type);
        $('#tm_name_' + row).css('border-bottom', type);
        $('#tm_role_' + row).css('border-bottom', type);
        $('#tm_group_' + row).css('border-bottom', type);
        $('#tm_role_' + row).css('border-bottom', type);
    }
    $scope.Remove = function (index) {
        //Find the record using Index from Array.
        var name = $scope.Customers[index].Emailid;
        if ($window.confirm("Do you want to delete: " + name)) {
            //Remove the item from Array using Index.
            $scope.Customers.splice(index, 1);
            if ($scope.Customers.length == 0) {
                $scope.Add();
            }
            $scope.currentRow = $scope.Customers.length - 1;
        }
    }
    //$scope.SaveEventTeam = function () {
    //    

    //    var MemberEmail = $scope.Emailid
    //    var MemberName = $scope.Name
    //    var MemberRole = $scope.Role
    //    var resultPosr = RequestData.SaveEventTeam(0, MemberEmail, MemberName, MemberRole, 'insertTeamMembers')
    //    resultPosr.then(function (pl) {

    //    })
    //}
    $scope.Roles = function () {
        var resultRoles = RequestData.GetRoles();
        resultRoles.then(function (pl) {

            $scope.RolesList = pl.data;
        }, function () {

        })
    }
    $scope.Group = function () {
        var resultRoles = RequestData.GetGroups();
        resultRoles.then(function (pl) {
            $scope.Groups = pl.data;
        }, function () {

        })
    }
    $scope.SaveTeamMembers = function () {

        for (var i = 0; i < $scope.Customers.length; i++) {
            if ($scope.Customers[i].RoleId && $scope.Customers[i].RoleId.RoleId)
                $scope.Customers[i].RoleId = $scope.Customers[i].RoleId.RoleId;
            if ($scope.Customers[i].GroupId && $scope.Customers[i].GroupId.GroupId)
                $scope.Customers[i].GroupId = $scope.Customers[i].GroupId.GroupId;
        }
        var saveTeam = RequestData.AddTeamMember($scope.Customers);
        saveTeam.then(function (pl) {

            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                $scope.Customers = [{ MemberEmail: '', MemberName: '', RoleId: '', GroupId: '' }];
                alert('Team Members added succesfully');
                $scope.ViewTeamMembers();
            }
            else {
                alert(pl.data);
            }
        }, function () {
        })
    }

    $scope.ViewTeamMembers = function () {

        var viewTeam = RequestData.GetTeamMember($scope.Customers);
        viewTeam.then(function (pl) {

            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                $scope.CLSGuestStausList = pl.data;
                sessionStorage.CLSGuestStausList = JSON.stringify(pl.data);
            }
        }, function () {
        })
    }

    $scope.EditTeamMember = function (groupId, roleId, index) {

        $scope.TeamMembers = jQuery.grep($scope.TeamMembers, function (element, index) {
            if (element.TeamId == roleId) {
                element.RoleIds = element.RoleId;
            }
            if (element.GroupId == groupId) {
                element.GroupIds = element.GroupId;
            }
            return element;
        });

        //$scope.RoleIds = roleData[0].RoleId;
        $scope.IsEdit = true;
        $('#tm_email_edit_' + index).css('visibility', 'visible');
        $('#tm_name_edit_' + index).css('visibility', 'visible');
        $('#tm_role_edit_' + index).css('visibility', 'visible');
        $('#tm_group_edit_' + index).css('visibility', 'visible');

        $('#tm_email_view_' + index).css('visibility', 'hidden');
        $('#tm_name_view_' + index).css('visibility', 'hidden');
        $('#tm_role_view_' + index).css('visibility', 'hidden');
        $('#tm_group_view_' + index).css('visibility', 'hidden');
        $('#tm_isAdmin_Edit_' + index).prop('disabled', false);

    }
    $scope.CancelTeamMember = function (teamId, id) {
        $scope.IsEdit = false;
        $('#tm_email_edit_' + id).css('visibility', 'hidden');
        $('#tm_name_edit_' + id).css('visibility', 'hidden');
        $('#tm_role_edit_' + id).css('visibility', 'hidden');
        $('#tm_group_edit_' + id).css('visibility', 'hidden');

        $('#tm_email_view_' + id).css('visibility', 'visible');
        $('#tm_name_view_' + id).css('visibility', 'visible');
        $('#tm_role_view_' + id).css('visibility', 'visible');
        $('#tm_group_view_' + id).css('visibility', 'visible');
        $('#tm_isAdmin_Edit_' + id).prop('disabled', 'disabled');

        var teamData = jQuery.grep(JSON.parse(sessionStorage.TeamMembers), function (element, index) {
            return element.TeamId == teamId;
        });
        $('#tm_isAdmin_Edit_' + id).prop('checked', teamData[0].IsAdmin);
    }
    $scope.UpdateTeamMember = function (id, index) {
        var teamData = jQuery.grep($scope.TeamMembers, function (element, index) {
            return element.TeamId == id;
        });
        var tempData = { IsAdmin: teamData[0].IsAdmin, TeamId: teamData[0].TeamId, GroupId: teamData[0].GroupIds, RoleId: teamData[0].RoleIds, MemberName: teamData[0].MemberName, MemberEmail: teamData[0].MemberEmail }
        var resultPosr = RequestData.UpdateTeamMembers(tempData)
        resultPosr.then(function (pl) {
            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                $scope.Customers = [{ MemberEmail: '', MemberName: '', RoleId: '', GroupId: '' }];
                alert('Team Members updated succesfully');
                $scope.ViewTeamMembers();
                $scope.CancelTeamMember(index)
            }
            else {
                alert(pl.data);
            }
        })
    }

    $scope.EditGroup = function (name, id) {

        $('#btnAdd').trigger('click');
        $('#btnSaveGroup').val('Update Group');

        $scope.groupName = name;
        $scope.groupId = id;
    }

    $scope.CancelGroup = function () {
        $('#btnSaveGroup').val('Save Group');
        $('#btnAdd').trigger('click');
        $scope.groupName = '';
        $scope.groupId = 0;
    }

    $scope.DeleteGroup = function (id) {
        var deletegroup = RequestData.DeleteGroup(id);
        deletegroup.then(function (pl) {

            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                $scope.Group();
                alert('Group deleted succesfully');
            }
            else {
                alert(pl.data);
            }
        }, function () {
        })
    }

    $scope.SaveGroups = function () {

        var savegroup = RequestData.SaveGroup($scope.groupName, $scope.groupId);
        savegroup.then(function (pl) {

            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {

                $scope.groupName = '';
                $scope.groupId = 0;
                $scope.Group();
                alert('Group added succesfully');
                $scope.CancelGroup();
                //$scope.ListGroup();
            }
            else {
                alert(pl.data);
            }
        }, function () {
        })
    }


    $scope.DeleteTeamMember = function (id) {
        var resultPosr = RequestData.DeleteTeamMembers(id)
        resultPosr.then(function (pl) {
            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                alert('Team Members deleted succesfully');
                $scope.ViewTeamMembers();
            }
            else {
                alert(pl.data);
            }
        })
    }


    $scope.Roles();
    $scope.Group();
    $scope.ViewTeamMembers();
})

app.controller('ViewEventCtrl', function ($scope, RequestData, Map) {

    $scope.EventProfilePic = JSON.parse(sessionStorage.EditEvent)[0].EProfilePic;
    $scope.EName = JSON.parse(sessionStorage.EditEvent)[0].EName;
    $scope.EOwnerName = JSON.parse(sessionStorage.EditEvent)[0].EOwnerName;
    $scope.EDateFrom = JSON.parse(sessionStorage.EditEvent)[0].EDateFrom;
    $scope.EDateTo = JSON.parse(sessionStorage.EditEvent)[0].EDateTo;
    $scope.EAddress = JSON.parse(sessionStorage.EditEvent)[0].EAddress,
        $scope.ELan = JSON.parse(sessionStorage.EditEvent)[0].ELan,
        $scope.ELang = JSON.parse(sessionStorage.EditEvent)[0].ELang,
        $scope.EGuestCount = JSON.parse(sessionStorage.EditEvent)[0].EGuestCount,
        $scope.EType = JSON.parse(sessionStorage.EditEvent)[0].EType,
        $scope.GroupId = JSON.parse(sessionStorage.EditEvent)[0].GroupId,
        $scope.TeamId = JSON.parse(sessionStorage.EditEvent)[0].TeamId,
        $scope.isEHost = JSON.parse(sessionStorage.EditEvent)[0].isEHost,
        $scope.EHostEmail = JSON.parse(sessionStorage.EditEvent)[0].EHostEmail,
        $scope.EFeatureId = JSON.parse(sessionStorage.EditEvent)[0].EFeatureId,
        $scope.ImageName = JSON.parse(sessionStorage.EditEvent)[0].ImageName,
        $scope.CreateBy = JSON.parse(sessionStorage.EditEvent)[0].CreateBy
    Map.init();
    Map.search(JSON.parse(sessionStorage.EditEvent)[0].EAddress)
        .then(function (res) { // success

            Map.addMarker(res);
            //$scope.place.name = res.name;
            $scope.lat = res.geometry.location.lat();
            $scope.lng = res.geometry.location.lng();
            $scope.$apply();
        },
        function (status) { // error
            $scope.apiError = true;
            $scope.apiStatus = status;
        });

})
/////Ongoing Event Ctrl made by aditya/////
app.controller('OngoingEventCtrl', function ($scope, RequestData) {
    var CreateBy = localStorage.VendorInfo;
    $scope.GetAllOngoingEvent = function () {
        var ResultData = RequestData.GetAllOngoingEvent(CreateBy)
        ResultData.then(function (pl) {
            if (pl.status == EventovitiCommon.GetResponseStatus('Not Found')) {
                alert(pl.data);
            }
            else {
                $scope.OngoingEvents = pl.data;
            }
        })
    }
    $scope.GetAllOngoingEvent();
})

app.controller('UpcomingEventCtrl', function ($scope, RequestData) {
    var CreateBy = localStorage.VendorInfo;
    $scope.GetAllUpcomingEvent = function () {
        var ResultData = RequestData.GetAllUpcomingEvent(CreateBy)
        ResultData.then(function (pl) {
            if (pl.status == EventovitiCommon.GetResponseStatus('Not Found')) {
                alert(pl.data);
            }
            else {
                $scope.UpcomingEvents = pl.data;
            }
        })
    }
    $scope.GetAllUpcomingEvent();
})

app.controller('TaskAlloctionCtrl', function ($scope, RequestData, $window) {

    $scope.Initialize = function () {
        $scope.dateFrom = new Date();
        $scope.tomorrow = new Date();
        $scope.tomorrow.setDate($scope.tomorrow.getDate() + 1);
        $scope.Task = { Priority: 0 };
        $scope.SubTask = [{ SubTaskName: '', DateAllocate: '', TimeAllocate: '' }];
        $scope.currentRow = $scope.SubTask.length - 1;

    }

    $scope.Remove = function (index) {
        //Find the record using Index from Array.
        var name = $scope.SubTask[index].Emailid;
        if ($window.confirm("Do you want to delete: " + name)) {
            //Remove the item from Array using Index.
            $scope.SubTask.splice(index, 1);
            if ($scope.SubTask.length == 0) {
                $scope.Add();
            }
            $scope.currentRow = $scope.SubTask.length - 1;
        }
    }
    $scope.Add = function () {

        $scope.row = $scope.SubTask.length - 1;
        if ($scope.SubTask[$scope.SubTask.length - 1].SubTaskName) {
            $scope.currentRow = $scope.SubTask.length;
            $scope.SubTask.push({ SubTaskName: '', DateAllocate: '', TimeAllocate: '' });
            $scope.removeBorder($scope.row, 'none');
        }
        else {
            $scope.addBorder($scope.row, '1px solid red');
        }
    };

    //Add remove border for validation
    $scope.removeBorder = function (row, type) {
        $('#tm_SubTaskTitle_' + row).parent('div').removeClass('has-error');
        $('#tm_DateTimeFrom_' + row).parent('div').removeClass('has-error');
        $('#tm_DateTimeTo_' + row).parent('div').removeClass('has-error');
    }
    $scope.addBorder = function (row, type) {
        $('#tm_SubTaskTitle_' + row).parent('div').addClass('has-error');
        $('#tm_DateTimeFrom_' + row).parent('div').addClass('has-error');
        $('#tm_DateTimeTo_' + row).parent('div').addClass('has-error');
    }

    //Enable Datetimepicker on focus
    $(function () {
        $('body').on('focus', ".tm_DateTimeFrom", function () {
            $(this).datetimepicker({
                format: 'MM/DD/YYYY'
            });
        });
        $('body').on('focus', ".tm_DateTimeTo", function () {
            $(this).datetimepicker({
                format: 'LT'
            });
        });

    });

    $scope.TeamMembersList = function () {
        var viewTeam = RequestData.GetTeamMember();
        viewTeam.then(function (pl) {

            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {

                $scope.TeamMembers = pl.data;
            }
        }, function () {
        })
    }
    $scope.GetAllEvent = function () {
        var resultAllEvent = RequestData.GetEventList();
        resultAllEvent.then(function (pl) {

            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                $scope.EventList = pl.data;
            }
        })
    }

    $scope.SaveTask = function () {

        $scope.Task.lstTaskTeam = $scope.SubTask;
        var resultSaveTask = RequestData.SaveTask($scope.Task);
        resultSaveTask.then(function (pl) {
            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                $scope.SubTask = [{ SubTaskName: '', DateAllocate: '', TimeAllocate: '' }];
                alert('Task added succesfully');
                $scope.Initialize();
                $scope.Task.EventId = 0;
                $scope.Task.TeamId = 0;
            }
            else {
                alert(pl.data);
            }
        })

    }


    $scope.IsReminder = function () {

        $scope.IsDisable = $scope.IsDisable == true ? false : true;
    }

    $scope.IsChecked = true;
    $scope.IsDisable == true;
    $scope.IsReminder();
    $scope.Initialize();
    $scope.TeamMembersList();
    // $scope.GetAllEvent();
})
/////Venue Ctrl made by aditya/////
app.controller('VenuCtrl', function ($scope, RequestData, Map, $window, $location) {
    var vendorId = localStorage.VendorInfo;
    $scope.fill = [];
    $scope.DataImages = [{ VIPath: '', VIName: '' }];
    $scope.VM = {};
    $scope.VM.SpecialityTags = "Demo Tags, Tags of days"
    $scope.Customers = [{ MemberEmail: '' }];
    $scope.Customers.GroupId = '';
    $scope.Days = [
        { Name: 'Monday', Values: '1' },
        { Name: 'Tuesday', Values: '2' },
        { Name: 'Wednesday', Values: '3' },
        { Name: 'Thuesday', Values: '4' },
        { Name: 'Friday', Values: '5' },
        { Name: 'Saturday', Values: '6' },
        { Name: 'Sunday', Values: '7' },
        { Name: 'Weak Days', Values: '8' },
    ];
    $scope.highlightsSelect = [];
    $scope.toggleSelection = function toggleSelection(CatList) {
        var idx = $scope.highlightsSelect.indexOf(CatList);

        // is currently selected
        if (idx > -1) {
            $scope.highlightsSelect.splice(idx, 1);
        }

        // is newly selected
        else {
            $scope.highlightsSelect.push(CatList.HId);

        }
    };

    ////// Add rows Code By Aditya //////
    $scope.SubTask = [{ VSTId: '0', VTNames: '', SCapacity: '', FCapacity: '' }];
    $scope.currentRow = $scope.SubTask.length - 1;
    $scope.Remove = function (index) {
        debugger;
        //Find the record using Index from Array.
        var name = $scope.SubTask[index].VSTId;
        if ($window.confirm("Do you want to delete: " + name)) {
            //Remove the item from Array using Index.
            $scope.SubTask.splice(index, 1);
            if ($scope.SubTask.length == 0) {
                $scope.Add();
            }
            $scope.currentRow = $scope.SubTask.length - 1;
        }
    }
    $scope.Add = function () {
        debugger;
        $scope.row = $scope.SubTask.length - 1;
        if ($scope.SubTask[$scope.SubTask.length - 1].VSTId && $scope.SubTask[$scope.SubTask.length - 1].VTNames && $scope.SubTask[$scope.SubTask.length - 1].SCapacity && $scope.SubTask[$scope.SubTask.length - 1].FCapacity) {
            $scope.currentRow = $scope.SubTask.length;
            $scope.SubTask.push({ VSTId: '0', VTNames: '', SCapacity: '', FCapacity: '' });
            $scope.removeBorder($scope.row, 'none');
        }
        else {
            $scope.addBorder($scope.row, '1px solid red');
        }
    };
    $scope.removeBorder = function (row, type) {
        $('#tm_VenuType_' + row).parent('div').removeClass('has-error');
        $('#tm_VTName_' + row).parent('div').removeClass('has-error');
        $('#tm_SCapacity_' + row).parent('div').removeClass('has-error');
        $('#tm_FCapacity_' + row).parent('div').removeClass('has-error');
        $('#tm_Prices_' + row).parent('div').removeClass('has-error');
    }
    $scope.addBorder = function (row, type) {
        $('#tm_VenuType_' + row).parent('div').addClass('has-error');
        $('#tm_VTName_' + row).parent('div').addClass('has-error');
        $('#tm_SCapacity_' + row).parent('div').addClass('has-error');
        $('#tm_FCapacity_' + row).parent('div').addClass('has-error');
        $('#tm_Prices_' + row).parent('div').addClass('has-error');
    }

    ///Stay venues add rows
    $scope.StayVenue = [{ VSTId: '0', SVTNames: '', SQuantity: '', StayCapacity: '' }];
    $scope.stayCurrentRow = $scope.StayVenue.length - 1;
    $scope.StayRemove = function (index) {
        //Find the record using Index from Array.
        var name = $scope.StayVenue[index].VSTId;
        if ($window.confirm("Do you want to delete: " + name)) {
            //Remove the item from Array using Index.
            $scope.StayVenue.splice(index, 1);
            if ($scope.StayVenue.length == 0) {
                $scope.StayAdd();
            }
            $scope.stayCurrentRow = $scope.StayVenue.length - 1;
        }
    }
    $scope.StayAdd = function () {

        $scope.row = $scope.StayVenue.length - 1;
        if ($scope.StayVenue[$scope.StayVenue.length - 1].VSTId && $scope.StayVenue[$scope.StayVenue.length - 1].SVTNames && $scope.StayVenue[$scope.StayVenue.length - 1].StayCapacity) {
            $scope.stayCurrentRow = $scope.StayVenue.length;
            $scope.StayVenue.push({ VSTId: '0', SVTNames: '', SQuantity: '', StayCapacity: '' });
            $scope.stayRemoveBorder($scope.row, 'none');
        }
        else {
            $scope.stayAddBorder($scope.row, '1px solid red');
        }
    };
    $scope.stayRemoveBorder = function (row, type) {
        $('#tm_StayType_' + row).parent('div').removeClass('has-error');
        $('#tm_SVTName_' + row).parent('div').removeClass('has-error');
        $('#tm_SQuantity_' + row).parent('div').removeClass('has-error');
        $('#tm_StayCapacity_' + row).parent('div').removeClass('has-error');

    }
    $scope.stayAddBorder = function (row, type) {
        $('#tm_StayType_' + row).parent('div').addClass('has-error');
        $('#tm_SVTName_' + row).parent('div').addClass('has-error');
        $('#tm_SQuantity_' + row).parent('div').addClass('has-error');
        $('#tm_StayCapacity_' + row).parent('div').addClass('has-error');

    }
    $scope.VM.VLng = undefined;
    $scope.VM.VLong = undefined;
    $scope.$on('gmPlacesAutocomplete::placeChanged', function () {

        Map.search($scope.autocomplete.getPlace().formatted_address)
            .then(function (res) { // success

                Map.addMarker(res);
                //$scope.place.name = res.name;
                $scope.VM.VLng = res.geometry.location.lat();
                $scope.VM.VLong = res.geometry.location.lng();
                $scope.$apply();
            },
            function (status) { // error
                $scope.apiError = true;
                $scope.apiStatus = status;
            });
    });
    Map.init();
    $('#wizard-picture').on('change', function (evt) {

        var files = $(evt.currentTarget).get(0).files;

        if (files.length > 0) {
            $scope.uploadme1 = files[0].name;
            console.log($scope.uploadme1);
        }
    });
    $scope.onChange = function (e, fileList) {
        debugger;
        console.log(fileList.base64);
    };
    $scope.onLoad = function (e, reader, file, fileList, fileOjects, fileObj) {
        alert('this is handler for file reader onload event!');
    };
    $scope.getEventType = function () {
        var ETID = 1;
        var ResultData = RequestData.eventType(ETID, vendorId, 'SelectEventTypeId')
        ResultData.then(function (pl) {

            if (pl.status == EventovitiCommon.GetResponseStatus('Not Found')) {
                alert(pl.data)
            }
            else {
                $scope.evenTypeData = pl.data;
            }
        })
    }
    $scope.SaveVenueManage = function () {
        debugger;
        if ($scope.VM.VenueId == undefined) {
            $scope.VM.CreatedBy = vendorId;
            $scope.VM.VenueAddress = $scope.autocomplete.getPlace().formatted_address;
            $scope.VM.ETID = JSON.stringify($scope.Customers.GroupId);
            $scope.VM.listVenueType = $scope.SubTask;
            $scope.VM.listStaySubVenue = $scope.StayVenue;
            var saveTeam = RequestData.SaveVenue($scope.VM);
            saveTeam.then(function (pl) {

                if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                    alert('Your Venue is added succesfully');
                    $scope.SubTask = [{ VSTId: '0', VTNames: '', VQuntity: '', VArea: '', VCapacity: '' }];
                    $scope.VM.VenueId = pl.data;
                }
                else {
                    alert(pl.data);
                }
            }, function () {
            })
        }
    }
    $scope.DataFolder = function () {
        $scope.VM.listPackages.BookingAmount = $scope.VM.listPackages.PackagePrice;
    }
    $scope.SaveVenueGallery = function () {

        if ($scope.VM.VenueId != undefined) {
            var imgBlob = [];
            var da = []
            for (var i = 0; i < $scope.files.length; i++) {
                imgBlob = $scope.files[i].base64;
                da.push({
                    url: imgBlob,
                    name: $scope.fill[i].name
                })
            }
            $scope.VM.listImage = da;
            var saveTeam = RequestData.SaveVenueGallery($scope.VM);
            saveTeam.then(function (pl) {

                if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                    alert('Your Venue images are added succesfully');
                    $scope.GetAllStayVenue();
                    $scope.GetAllPackages();
                }
                else {
                    alert(pl.data);
                }
            }, function () {
            })
        }
    }
    $scope.SaveVenuePackages = function () {

        if ($scope.VM.VenueId != undefined) {

            var saveTeam = RequestData.SaveVenuePackages($scope.VM);
            saveTeam.then(function (pl) {

                if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                    alert('Your venue package is added succesfully');
                    $('#addPackages').modal('hide');
                    $scope.GetAllPackages();
                }
                else {
                    alert(pl.data);
                }
            }, function () {
            })
        }
    }
    $scope.SaveVenuePrivacy = function () {

        if ($scope.VM.VenueId != undefined) {
            $scope.VM.listPrivacyPolicy.TermSevice = 0;
            $scope.VM.listPrivacyPolicy.BookingTerms = 0;
            $scope.VM.listPrivacyPolicy.CancellationServices = 0;
            $scope.VM.listPrivacyPolicy.TravellingTerms = 0;
            if ($scope.VM.listPrivacyPolicy.TSDetails != undefined) {
                $scope.VM.listPrivacyPolicy.TermSevice = 1;

            }
            if ($scope.VM.listPrivacyPolicy.BTDetails != undefined) {
                $scope.VM.listPrivacyPolicy.BookingTerms = 1;
            }
            if ($scope.VM.listPrivacyPolicy.CSDetails != undefined) {
                $scope.VM.listPrivacyPolicy.CancellationServices = 1;
            }
            if ($scope.VM.listPrivacyPolicy.TTDetails != undefined) {
                $scope.VM.listPrivacyPolicy.TravellingTerms = 1;
            }
            var saveTeam = RequestData.SaveVenuePolicy($scope.VM);
            saveTeam.then(function (pl) {

                if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                    alert('Your venue privacy poilcy is been added succesfully');
                    $('#btnAdd').css('display', 'block')
                    $('#ShowData').css('display', 'block')
                    $('#addData').css('display', 'none')
                    $('#btnView').css('display', 'none')
                }
                else {
                    alert(pl.data);
                }
            }, function () {
            })
        }
    }
    $scope.SaveHighlights = function () {

        if ($scope.VM.VenueId != undefined) {
            $scope.VM.listHighlights.HId = JSON.stringify($scope.highlightsSelect);
            var saveTeam = RequestData.SaveHighlights($scope.VM);
            saveTeam.then(function (pl) {

                if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                    alert('Your venue privacy poilcy is been added succesfully');
                }
                else {
                    alert(pl.data);
                }
            }, function () {
            })
        }
    }

    $scope.GetAllVenueManage = function () {

        var CreatedBy = vendorId;
        var viewTeam = RequestData.GetVenueManage(CreatedBy, 'getAllVenueData');
        viewTeam.then(function (pl) {

            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {

                $scope.VenueManage = pl.data;
                sessionStorage.VenueManage = JSON.stringify(pl.data);
            }
        }, function () {
        })
    }

    //Pendings
    $scope.GetAllPackages = function () {

        if ($scope.VM.VenueId != undefined) {
            $scope.VM.VenueId = $scope.VM.VenueId;
            $scope.VM.CreatedBy = vendorId;
            var viewPackages = RequestData.GetAllPackages($scope.VM);
            viewPackages.then(function (pl) {

                if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {

                    $scope.packages = pl.data;
                }
            }, function () {
            })
        }
    }
    $scope.editVenue = function (venueList) {

        var CreatedBy = vendorId;
        var VenueId = venueList.VenueId;
        var viewTeam = RequestData.GetEditVenueManage(VenueId, CreatedBy, 'getEditVenueData');
        viewTeam.then(function (pl) {

            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {

                $('#btnAdd').css('display', 'none');
                $('#ShowData').css('display', 'none');
                $('#addData').css('display', 'block');
                $('#btnView').css('display', 'block');
                $('#update').removeClass('hidden');
                $('#save').addClass('hidden');
                $scope.venueData = pl.data;
                sessionStorage.VenueManage = JSON.stringify(pl.data);

                $scope.VM.VenueId = $scope.venueData[0].VenueId;
                $scope.VM.VenueName = $scope.venueData[0].VenueName;
                $scope.autocomplete = $scope.venueData[0].VenueAddress;
                $scope.VM.VLng = $scope.venueData[0].VLng;
                $scope.VM.VLong = $scope.venueData[0].VLong;
                $scope.VM.VenueType = $scope.venueData[0].VenueType;
                $scope.Customers.GroupId = $scope.venueData[0].EventType;
                $scope.VM.ContactNo = $scope.venueData[0].ContactNo;
                $scope.VM.ContactTimeFrom = $scope.venueData[0].ContactTimeFrom;
                $scope.VM.ContactTimeTo = $scope.venueData[0].ContactTimeTo;
                $scope.VM.VenueDetails = $scope.venueData[0].VenueDetails;
                $scope.VM.SpecialityTags = $scope.venueData[0].SpecialityTags;
                for (var i = 0; i < $scope.venueData.length; i++) {
                    //imgBlob = $scope.venueData[i].base64;
                    $scope.SubTask.push({
                        VTypeId: $scope.venueData[i].VTypeId,
                        VSTId: $scope.venueData[i].VSTId,
                        VTNames: $scope.venueData[i].VTNames,
                        VQuntity: $scope.venueData[i].VQuntity,
                        VArea: $scope.venueData[i].VArea,
                        VCapacity: $scope.venueData[i].VCapacity,
                    })
                    //$scope.VenueType.push
                }
                Map.init();
                Map.search($scope.venueData[0].VenueAddress)
                    .then(function (res) { // success

                        Map.addMarker(res);
                        //$scope.$apply();
                    },
                    function (status) { // error
                        $scope.apiError = true;
                        $scope.apiStatus = status;
                    });
                $scope.$apply();
            }
        }, function () {
        })
        //$scope.VenueId = venueList.VenueId;
        //$scope.VName = venueList.VenueName;
        //$scope.VM.autocomplete = venueList.VenueAddress;
        //$scope.lat = venueList.VLng;
        //$scope.lng = venueList.VLong;
        //$scope.VenueType = venueList.VenueType;
        //$scope.Customers.GroupId = venueList.EventType;
        //// $scope.$apply();
        //$scope.VDetails = venueList.VenueDetails;
        ////  sessionStorage.VenueManage = JSON.stringify(pl.data);
        //Map.init();
        //Map.search(venueList.VenueAddress)
        //       .then(function (res) { // success
        //           
        //           Map.addMarker(res);
        //           $scope.$apply();
        //       },
        //       function (status) { // error
        //           $scope.apiError = true;
        //           $scope.apiStatus = status;
        //       });
        //$scope.$apply();

    }
    $scope.UpdateVenueManage = function () {

        var vAddress = '';
        if ($scope.VM.autocomplete == '') {
            var vAddress = $scope.VM.autocomplete.getPlace().formatted_address;
        }
        else {
            var vAddress = $scope.VM.autocomplete;
        }
        var tempData = {
            venueId: $scope.VenueId,
            VenueName: $scope.VName,
            VenueAddress: vAddress,
            VLng: $scope.lat,
            VLong: $scope.lng,
            ETID: $scope.Customers.GroupId,
            VenueType: $scope.VenueType,
            VenueDetails: $scope.VDetails,
            CreatedBy: vendorId
        }
        var VenuManage = tempData;
        var updateVenue = RequestData.UpdateVenueManage(VenuManage);
        updateVenue.then(function (pl) {

            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                alert('Your Venue is updated succesfully');
                $('#btnAdd').css('display', 'block');
                $('#ShowData').css('display', 'block');
                $('#addData').css('display', 'none');
                $('#btnView').css('display', 'none');
                $scope.GetAllVenueManage();
            }
            else {
                alert(pl.data);
            }
        }, function () {
        })

    }
    $scope.DeleteVenue = function (venueList) {
        var venueData = {
            venueId: venueList.VenueId
        }
        var deleteData = RequestData.DeleteVenueManage(venueData)
        deleteData.then(function (pl) {
            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                alert('Your Venue is Removed succesfully');
                $('#btnAdd').css('display', 'block');
                $('#ShowData').css('display', 'block');
                $('#addData').css('display', 'none');
                $('#btnView').css('display', 'none');
                $scope.GetAllVenueManage();
            }
            else {
                alert(pl.data);
            }
        })
    }
    $scope.Venuechedule = function (venueList) {
        $scope.VenueId = venueList.VenueId;
        $scope.VenueName = venueList.VenueName;
        $scope.GetAllVenueSchedule();
    }
    $scope.SaveVenueSchedule = function () {

        var tempData = {
            VenueId: $scope.VenueId,
            VSDay: $scope.Customers.MemberEmail,
            VSDateFrom: $scope.dataFrom,
            VSDateTo: $scope.dataTo,
            VSTimeFrom: $scope.timeFrom,
            VSTimeto: $scope.timeTo,
            CreatedBy: vendorId
        }

        var VenuSchedule = tempData;
        var saveTeam = RequestData.SaveVenueSchedule(VenuSchedule);
        saveTeam.then(function (pl) {

            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                alert('Your Venue schedule is added succesfully');
                $scope.GetAllVenueSchedule();
            }
            else {
                alert(pl.data);
            }
        }, function () {
        })
    }
    $scope.GetAllVenueSchedule = function () {

        var VData = {
            CreatedBy: vendorId,
            VenueId: $scope.VenueId
        }
        var viewTeam = RequestData.GetVenueSchedule(VData);
        viewTeam.then(function (pl) {

            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {

                $scope.VenueSchedule = pl.data;
                sessionStorage.VenueSchedule = JSON.stringify(pl.data);
            }
        }, function () {
        })
    }
    $scope.EditVenueSchedule = function (schedule) {

        $('#updateScheduleData').removeClass('hidden');
        $('#addScheduleData').addClass('hidden');
        $scope.VSId = schedule.VSId;
        $scope.VenueId = schedule.VenueId;
        $scope.Customers.MemberEmail = schedule.VSDay;
        $scope.dataFrom = schedule.VSDateFrom;
        $scope.dataTo = schedule.VSDateTo;
        $scope.timeFrom = schedule.VSTimeFrom;
        $scope.timeTo = schedule.VSTimeto;
        //$scope.$apply();
    }
    $scope.UpdateVenueScheudle = function () {

        var tempData = {
            VSId: $scope.VSId,
            venueId: $scope.VenueId,
            VSDay: $scope.Customers.MemberEmail,
            VSDateFrom: $scope.dataFrom,
            VSDateTo: $scope.dataTo,
            VSTimeFrom: $scope.timeFrom,
            VSTimeto: $scope.timeTo,
            CreatedBy: vendorId
        }
        var VenuManage = tempData;
        var updateVenue = RequestData.UpdateVenueSchedule(VenuManage);
        updateVenue.then(function (pl) {

            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                alert('Your Venue is updated succesfully');
                $('#btnAdd').css('display', 'block');
                $('#ShowData').css('display', 'block');
                $('#addData').css('display', 'none');
                $('#btnView').css('display', 'none');
                $('#updateScheduleData').addClass('hidden');
                $('#addScheduleData').removeClass('hidden');
                $scope.GetAllVenueSchedule();
            }
            else {
                alert(pl.data);
            }
        }, function () {
        })

    }
    $scope.DeleteVenueSchedule = function (venueSList) {
        var venueData = {
            VSId: venueSList.VSId
        }
        var deleteData = RequestData.DeleteVeueSechedule(venueData)
        deleteData.then(function (pl) {
            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                alert('Your Venue is Removed succesfully');
                $('#btnAdd').css('display', 'block');
                $('#ShowData').css('display', 'block');
                $('#addData').css('display', 'none');
                $('#btnView').css('display', 'none');
                $scope.GetAllVenueSchedule();
            }
            else {
                alert(pl.data);
            }
        })
    }
    $scope.cancel = function () {
        $scope.VM.VName = '';
        $scope.VM.autocomplete = '';
        $scope.VM.Customers.GroupId = '';
        $scope.VM.VenueType = '';
        $scope.VM.VDetails = '';
        $('#btnAdd').css('display', 'block')
        $('#ShowData').css('display', 'block')
        $('#addData').css('display', 'none')
        $('#btnView').css('display', 'none')
    }
    $scope.GetAllVenueType = function () {

        var Ctype = 'VH';
        var resultType = RequestData.GetAllVenueType(Ctype);
        resultType.then(function (pl) {
            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                $scope.VenueType = pl.data;
            }
            else {
                alert(pl.data);
            }
        })
    }
    $scope.GetStayVenueType = function () {

        var Ctype = 'VR';
        var resultType = RequestData.GetAllVenueType(Ctype);
        resultType.then(function (pl) {
            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                $scope.StayVenueType = pl.data;
            }
            else {
                alert(pl.data);
            }
        })
    }
    $scope.GetAllHighlights = function () {
        var resultType = RequestData.GetAllHighlights();
        resultType.then(function (pl) {
            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                $scope.highlights = pl.data;
            }
            else {
                alert(pl.data);
            }
        })
    }
    $scope.GetAllStayVenue = function () {

        if ($scope.VM.VenueId != undefined) {
            var VenueId = $scope.VM.VenueId;
            var resultType = RequestData.GetAllStayVenue(VenueId, 'SelectVenueStay');
            resultType.then(function (pl) {
                if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                    $scope.StayVenues = pl.data;
                }
                else {
                    alert(pl.data);
                }
            })
        }
    }
    $scope.ShowTheView = function (dl) {

        localStorage.VenueId = dl.VenueId;
        $location.path('/ViewServices');
        //$location.path('#!/ViewServices?VenueId=' + dl.VenueId);
    }

})

app.controller('ViewServicesCtrl', function ($scope, RequestData, $window, $routeParams, Map) {
    $scope.VM = {};
    var VenueId = localStorage.VenueId;
    var CreatedBy = localStorage.VendorInfo;
    $scope.GetServicesData = function () {
        $scope.VM.venueId = VenueId;
        $scope.VM.CreatedBy = CreatedBy;
        var viewData = RequestData.GetEditVenueManage(VenueId, CreatedBy, 'getAllVenueInfo');
        viewData.then(function (pl) {

            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {

                $scope.VenueDetails = pl.data;
                var pData = RequestData.GetEditVenueManage(VenueId, CreatedBy, 'getAllprivacyInfo');
                pData.then(function (pl) {

                    if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                        debugger
                        $scope.PrivacyDetails = pl.data;
                    }
                }, function () {
                })
                var eventData = RequestData.GetEditVenueManage(VenueId, CreatedBy, 'getAllEveentInfo');
                eventData.then(function (pl) {

                    if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {

                        $scope.EventDetails = pl.data;
                    }
                }, function () {
                })
                $scope.GetPackagesData();
                $scope.GetHighlightData();
                Map.init();
                Map.search($scope.VenueDetails[0].VenueAddress)
                    .then(function (res) { // success

                        Map.addMarker(res);
                        //$scope.place.name = res.name;
                        $scope.lat = res.geometry.location.lat();
                        $scope.lng = res.geometry.location.lng();
                        $scope.$apply();
                    },
                    function (status) { // error
                        $scope.apiError = true;
                        $scope.apiStatus = status;
                    });
            }
        }, function () {
        })
    }
    $scope.GetServicesImages = function () {
        $scope.VM.venueId = VenueId;
        $scope.VM.CreatedBy = CreatedBy;
        var viewData = RequestData.GetServicesImages($scope.VM);
        viewData.then(function (pl) {

            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {

                $scope.serviceImages = pl.data;

            }
        }, function () {
        })
    }
    $scope.GetPackagesData = function () {
        $scope.VM.venueId = VenueId;
        $scope.VM.CreatedBy = CreatedBy;
        var viewData = RequestData.GetEditVenueManage(VenueId, CreatedBy, 'getAllPackages');
        viewData.then(function (pl) {

            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {

                $scope.PackageDetails = pl.data;

            }
        }, function () {
        })
    }
    $scope.GetHighlightData = function () {
        $scope.VM.venueId = VenueId;
        $scope.VM.CreatedBy = CreatedBy;
        var viewData = RequestData.GetEditVenueManage(VenueId, CreatedBy, 'getAllHighlight');
        viewData.then(function (pl) {

            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {

                $scope.HighlightDetails = pl.data;

            }
        }, function () {
        })
    }
})

////Controlers For Comman Header and Footers ////
app.controller('FooterCtrl', function ($scope) {
    $scope.footer = { name: "Footer.html", url: "MainPage/CommanLinks/Footer.html" };
})
app.controller('HeaderCtrl', function ($scope) {
    $scope.header = { name: "HeaderBar.html", url: "MainPage/CommanLinks/HeaderBar.html" };
})
app.controller('LoginFooterCtrl', function ($scope) {
    $scope.footer = { name: "loginFooter.html", url: "MainPage/CommanLinks/loginFooter.html" };
})
app.controller('LoginHeaderCtrl', function ($scope) {
    $scope.header = { name: "HeaderBar.html", url: "MainPage/CommanLinks/LoginHeader.html" };
})
app.controller('AdminFooterCtrl', function ($scope) {
    $scope.footer = { name: "AdminFooter.html", url: "MainPage/CommanLinks/AdminFooter.html" };
})
app.controller('AdminHeaderCtrl', function ($scope) {
    $scope.header = { name: "AdminHeader.html", url: "MainPage/CommanLinks/AdminHeader.html" };
})
app.controller('SliderMenuCtrl', function ($scope) {
    $scope.sliderHeader = { name: "AdminSliderMenu.html", url: "MainPage/CommanLinks/AdminSliderMenu.html" };
    $scope.VendoreImage = localStorage.VendorImage;
})
app.controller('HeaderPageCtrl', function ($scope) {
    $scope.HeaderPage = { name: "HeaderPage.html", url: "DemoPages/Master/HeaderPage.html" };
})
app.controller('FixHeaderCtrl', function ($scope) {
    $scope.FixHeader = { name: "FixHeader.html", url: "DemoPages/Master/FixHeader.html" };
})
app.controller('FooterPageCtrl', function ($scope) {
    $scope.FooterPage = { name: "FooterPage.html", url: "DemoPages/Master/FooterPage.html" };
})


////Controlers For Guest Management ////
app.controller('GuestManagementCtrl', function ($scope, $window, RequestData) {


    //File Select event 
    $scope.selectFileforUpload = function (file) {
        debugger;
        $scope.SelectedFileForUpload = file[0];
    }

    //multiple file Select event 
    $scope.selectMultipleFileforUpload = function (file) {
        $scope.SelectedMultipleFileForUpload = file;
    }
    //Image File Validation
    $scope.ChechImageFileValid = function (file) {
        var isValid = false;
        if (file != null) {
            if ((file.type == 'image/png' || file.type == 'image/jpeg' || file.type == 'image/gif')) {
                $scope.FileInvalidMessage = "";
                isValid = true;
            }
            else {
                $scope.FileInvalidMessage = "Selected file is Invalid. (only file type png, jpeg and gif allowed)";
            }
        }
        else {
            $scope.FileInvalidMessage = "Image required!";
        }
        $scope.IsFileValid = isValid;
    };

    //Excel File Validation
    $scope.ChechExcelFileValid = function (file) {

        var isValid = false;
        if ($scope.SelectedFileForUpload != null) {
            if ((file.type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || file.type == 'application/vnd.ms-excel')) {
                $scope.FileInvalidMessage = "";
                isValid = true;
            }
            else {
                $scope.FileInvalidMessage = "Selected file is Invalid. (only file type xls,xlsx are allowed)";
            }
        }
        else {
            $scope.FileInvalidMessage = "Image required!";
        }
        $scope.IsFileValid = isValid;
    };

    //Save form data
    $scope.saveData = function () {
        debugger;
        if (!angular.isUndefined($scope.uploadme)) {
            $scope.Guest.IdProof = $scope.uploadme;
            if ($scope.Guest.IdProof.length > 0 || $scope.Guest.IdProof != undefined) {
                $scope.Guest.IdProof = $scope.Guest.IdProof.replace(/data:image\/jpeg;base64,/g, '');
                $scope.Guest.IdProof = $scope.Guest.IdProof.replace(/data:image\/png;base64,/g, '');

                $scope.ChechImageFileValid($scope.SelectedFileForUpload);
                $scope.Guest.ImageName = $scope.SelectedFileForUpload.name;
            }
        }

        var aa = $scope.Guest;

        var ResultData = RequestData.SaveNewGuest(aa);
        ResultData.then(function (pl) {
            if (pl.status == EventovitiCommon.GetResponseStatus('Not Found')) {
                alert(pl.data);
            }
            else {
                $scope.CityData = pl.data;
            }
        })
    }

    //save guest by Excel upload
    $scope.SaveGuestByCSV = function () {

        $scope.ChechExcelFileValid($scope.SelectedFileForUpload);
        var formData = new FormData();
        formData.append("file", $scope.SelectedFileForUpload);
        var ResultData = RequestData.SaveExcelFile(formData);
        ResultData.then(function (pl) {
            if (pl.status == EventovitiCommon.GetResponseStatus('Not Found')) {
                alert(pl.data);
            }
            else {

            }
        })
    }

    //save multiple images
    $scope.SaveMultipleImages = function () {
        debugger;

        var formData = new FormData();
        for (var i = 0; i < $scope.SelectedMultipleFileForUpload.length; i++) {
            formData.append("file", $scope.SelectedMultipleFileForUpload[i]);
        }

        var ResultData = RequestData.SaveMultipleImages(formData);
        ResultData.then(function (pl) {
            if (pl.status == false) {
                alert(pl.data);
            }
            else {
                alert('All images save succesfully');
                debugger;
                $scope.files = {};
            }
        })

    }
    //save multiple images
    $scope.SaveLogo = function () {
        debugger;
        var formData = new FormData();
        $scope.ChechImageFileValid($scope.SelectedFileForUpload);
        if ($scope.IsFileValid) {
            formData.append("file", $scope.SelectedFileForUpload);
            var ResultData = RequestData.SaveLogo(formData);
            ResultData.then(function (pl) {
                if (pl.status == false) {
                    alert(pl.data);
                }
                else {
                    alert('Logo save succesfully');
                }
            })
        }

    }

    //get all guest record with  status
    $scope.GetListStatus = function () {
        debugger;
        var viewTeam = RequestData.GetGuestListStatus();
        viewTeam.then(function (pl) {

            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                $scope.TeamMembers = pl.data;
                sessionStorage.TeamMembers = JSON.stringify(pl.data);
            }
        }, function () {
        })
    }

    //get all guest detail
    $scope.getGuestListDetail = function () {
        debugger;
        var viewTeam = RequestData.GetGuestListDetail();
        viewTeam.then(function (pl) {

            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                $scope.Guests = pl.data;
                sessionStorage.TeamMembers = JSON.stringify(pl.data);
            }
        }, function () {
        })
    }

    //get single guest record 
    $scope.GetGuestDetailByID = function (GuestID) {
        debugger;
        var viewTeam = RequestData.srvc_GetGuestDetailByID();
        viewTeam.then(function (pl) {

            if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                $scope.TeamMembers = pl.data;
                sessionStorage.TeamMembers = JSON.stringify(pl.data);
            }
        }, function () {
        })
    }

    //Add bulk upload textboxes
    $scope.SubTask = [{ id: '0', GuestName: '', City: '', GuestGroup: '', ContactNo: '', WhatsAppNo: '' }];
    $scope.currentRow = $scope.SubTask.length - 1;
    $scope.Remove = function (index) {
        //Find the record using Index from Array.
        var name = $scope.SubTask[index].Guest;
        if ($window.confirm("Do you want to delete: " + name)) {
            //Remove the item from Array using Index.
            $scope.SubTask.splice(index, 1);
            if ($scope.SubTask.length == 0) {
                $scope.Add();
            }
            $scope.currentRow = $scope.SubTask.length - 1;
        }
    }
    $scope.Add = function () {
        debugger;
        $scope.row = $scope.SubTask.length - 1;
        if ($scope.SubTask[$scope.SubTask.length - 1].GuestName && $scope.SubTask[$scope.SubTask.length - 1].City && $scope.SubTask[$scope.SubTask.length - 1].GuestGroup && $scope.SubTask[$scope.SubTask.length - 1].ContactNo && $scope.SubTask[$scope.SubTask.length - 1].WhatsAppNo) {
            $scope.currentRow = $scope.SubTask.length;
            $scope.SubTask.push({ id: '0', GuestName: '', City: '', GuestGroup: '', ContactNo: '', WhatsAppNo: '' });
            $scope.removeBorder($scope.row, 'none');
        }
        else {
            $scope.addBorder($scope.row, '1px solid red');
        }
    };
    $scope.removeBorder = function (row, type) {
        $('#txtGuestName' + row).parent('div').removeClass('has-error');
        $('#txtCity' + row).parent('div').removeClass('has-error');
        $('#txtGuestGroup' + row).parent('div').removeClass('has-error');
        $('#txtContactNo' + row).parent('div').removeClass('has-error');
        $('#txtWhatsAppNo' + row).parent('div').removeClass('has-error');
    }
    $scope.addBorder = function (row, type) {
        $('#txtGuestName' + row).parent('div').addClass('has-error');
        $('#txtCity' + row).parent('div').addClass('has-error');
        $('#txtGuestGroup' + row).parent('div').addClass('has-error');
        $('#txtContactNo' + row).parent('div').addClass('has-error');
        $('#txtWhatsAppNo' + row).parent('div').addClass('has-error');
    }

    //save guest bulk data
    $scope.SaveGuestBulkUpload = function () {
        debugger;
        for (var i = 0; i < $scope.SubTask.length; i++) {


            if ($scope.SubTask[i].GuestName != undefined) {

                //$scope.VM.VenueAddress = $scope.autocomplete.getPlace().formatted_address;
                //$scope.VM.ETID = JSON.stringify($scope.Customers.GroupId);

                var saveTeam = RequestData.SaveBulkData($scope.SubTask);
                saveTeam.then(function (pl) {

                    if (pl.status == EventovitiCommon.GetResponseStatus('Ok')) {
                        alert('All Guest is added succesfully');
                        $scope.SubTask = [{ id: '0', GuestName: '0', City: '', GuestGroup: '', ContactNo: '', WhatsAppNo: '' }];
                        $scope.getGuestListDetail();
                    }
                    else {
                        alert(pl.data);
                    }
                });
            }
            else {
                $scope.row = $scope.SubTask.length - 1;
                $scope.addBorder($scope.row, '1px solid red');
            }
        }
    }
})

