﻿/// <reference path="Controller.js" />
app.service('RequestData', function ($http) {
    //By Aditya
    this.loginEventoviti = function (vPassword, VEmailId, op) {

        var VendorLogin = { VPassword: vPassword, VEmailId: VEmailId, op: op }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/VendorRegistration",
            data: VendorLogin,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //By Aditya

    this.registration = function (RegistrationData) {
        
        //var VRegistrationData = { VCompanyName: VCompanyName, Address: Address, VPassword: vPassword, VEmailId: VEmailId, FirstName: FirstName, LastName: LastName, PhoneNo: PhoneNo, VendorInfo: VendorInfo, VImage: VImage, VMange: VMange, UserType: UserType, op: op, ImageName: ImageName }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/VendorRegistrations",
            data: RegistrationData,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    this.SaveVendorType = function (RegistrationData) {

        //var VRegistrationData = { VCompanyName: VCompanyName, Address: Address, VPassword: vPassword, VEmailId: VEmailId, FirstName: FirstName, LastName: LastName, PhoneNo: PhoneNo, VendorInfo: VendorInfo, VImage: VImage, VMange: VMange, UserType: UserType, op: op, ImageName: ImageName }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/SaveVendorType",
            data: RegistrationData,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //By Aditya
    this.selectCategory = function (E_Cat_ID, E_Cat_Type, E_Cat_Image, op) {

        var SelectEventCategory = { E_Cat_ID: E_Cat_ID, E_Cat_Type: E_Cat_Type, E_Cat_Image: E_Cat_Image, op: op }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/SelectEventCategory",
            data: SelectEventCategory,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //By Aditya
    this.SaveEventTeam = function (TemaId, MemberEmail, MemberName, MemberRole, op) {

        var SaveEventTeam = { TemaId: TemaId, MemberEmail: MemberEmail, MemberName: MemberName, MemberRole: MemberRole, op: op }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/SaveEventTeam",
            data: SaveEventTeam,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //By Aditya
    this.CityName = function (op, CityID, CityName) {

        var SaveEventTeam = { op: op, CityID: CityID, CityName: CityName }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/GetCityName",
            data: SaveEventTeam,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //By Aditya
    this.vendorCategory = function (vendorId, op) {

        var SaveEventTeam = { VRId: vendorId, op: op }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/GetEventType",
            data: SaveEventTeam,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //By Aditya
    this.eventType = function (ETID, vendorId, op) {

        var SaveEventTeam = { ETID: ETID, VRId: vendorId, op: op }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/GetEventType",
            data: SaveEventTeam,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //By Aditya
    this.featureType = function (op) {

        var selectFeature = { op: op }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/GetFeatures",
            data: selectFeature,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //by vikas
    this.AddTeamMember = function (teamData) {
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/SaveTeamMember",
            data: teamData,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //by vikas
    this.UpdateTeamMembers = function (teamData) {
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/UpdateTeamMember",
            data: teamData,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //by vikas
    this.DeleteTeamMembers = function (id) {
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/DeleteTeamMember",
            data: { TeamId: id },
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //by vikas
    this.GetRoles = function () {
        return $http({
            method: 'GET',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/GetAllRoles",
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }

    //by vikas
    this.GetTeamMember = function () {
        return $http({
            method: 'GET',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/GetAllTeamMembers",
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //by vikas
    this.GetGroups = function () {
        return $http({
            method: 'GET',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/GetAllGroups",
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }

    //By Aditya
    this.AddNewEvent = function (eventData) {
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/SaveNewEvent",
            data: eventData,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //By Aditya
    this.GetAllEvent = function (CreateBy) {
        var allEvent = { CreateBy: CreateBy }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/GetAllEvent",
            data: allEvent,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //By Aditya
    this.EditAllEvent = function (EventID) {
        var editEvent = { EventID: EventID }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + 'Eventoviti/EditAllEvent',
            data: editEvent,
            async: false,
            headers: { 'Content-Type': 'appliction/json; charset=utf-8' }
        })
    }
    //By Aditya
    this.GetAllOngoingEvent = function (CreateBy) {
        var allEvent = { CreateBy: CreateBy }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/GetAllOngoingEvent",
            data: allEvent,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //By Aditya
    this.GetAllUpcomingEvent = function (CreateBy) {
        var allEvent = { CreateBy: CreateBy }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/GetAllUpcomingEvent",
            data: allEvent,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //By Aditya
    this.SendInviteToOwner = function (EName, EHostEmail, EOwnerName) {
        var inviteParm = { EName: EName, EHostEmail: EHostEmail, EOwnerName: EOwnerName }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/InviteOwnerByMail",
            data: inviteParm,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //By Aditya
    this.SaveVenue = function (VenuManage) {
        debugger
        // VenuManage = JSON.stringify(VenuManage)
        //var inviteParm = { VenueName: VenueName, VenueAddress: VenueAddress, VLng: VLng, VLong: VLong, EventType: EventType, VenueDetails: VenueDetails, CreatedBy: CreatedBy }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/InsertVenue",
            data: VenuManage,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //By Aditya
    this.GetVenueManage = function (CreatedBy, op) {
        var venueData = {
            CreatedBy: CreatedBy, op: op
        }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/GetAllVenuesDatas",
            data: venueData,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    // By Aditya
    this.GetEditVenueManage = function (VenueId, CreatedBy, op) {
        var venueData = {
            VenueId: VenueId,
            CreatedBy: CreatedBy,
            op: op
        }
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/GetAllVenuesDatas",
            data: venueData,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //By Aditya
    this.GetVenueManageByID = function (venueData) {
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/GetVenueByID",
            data: venueData,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //By Aditya
    this.UpdateVenueManage = function (venueData) {
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/UpdateVenueManage",
            data: venueData,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //By Aditya
    this.DeleteVenueManage = function (venueData) {
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/DeleteVeueManage",
            data: venueData,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //By Aditya
    this.SaveVenueSchedule = function (VenuSchedule) {
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/SaveVenueSchedule",
            data: VenuSchedule,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //By Aditya
    this.GetVenueSchedule = function (VData) {
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/GetAllVenueSchedule",
            data: VData,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //By Aditya
    this.UpdateVenueSchedule = function (venueData) {
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/UpdateVenueSchedule",
            data: venueData,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //By Aditya
    this.DeleteVeueSechedule = function (venueData) {
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/DeleteVeueSechedule",
            data: venueData,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //By Aditya
    this.GetAllVenueType = function (Ctype) {
        dataVenue = { Ctype: Ctype };
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/SelectVenuetype",
            data: dataVenue,
            async: false,
            headers: { 'Content-Type': 'application/json; charest=utf-8' }
        })
    }
    //by vikas
    this.SaveTask = function (teamData) {
        debugger
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/AddTask",
            data: teamData,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //By Vikas
    this.SaveGroup = function (groupName, groupId) {
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/SaveGroup",
            data: { GroupName: groupName, GroupId: groupId },
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    //By Vikas
    this.DeleteGroup = function (groupId) {
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/DeleteGroup",
            data: { GroupId: groupId },
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    this.GetVenuePackages = function (packagesData) {
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/SaveGroup",
            data: packagesData,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    this.SaveVenueGallery = function (galleryData) {
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/InsertVenueGallery",
            data: galleryData,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    this.SaveVenuePackages = function (packageData) {
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/InsertVenuePackages",
            data: packageData,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    this.SaveVenuePolicy = function (packageData) {
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/InsertVenuePolicy",
            data: packageData,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    this.SaveHighlights = function (packageData) {
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/InsertHighlights",
            data: packageData,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    this.GetSubVendorList = function (VendorSubData) {
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/GetSubVendorList",
            data: VendorSubData,
            async: false,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
    }
    this.GetAllHighlights = function () {
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/GetHighlights",
            async: false,
            headers: { 'Content-Type': 'application/json; charest=utf-8' }
        })
    }
    this.GetAllStayVenue = function (VenueId,op) {
        dataVenue = { VenueId: VenueId, op: op };
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/SelectVenueOnlyType",
            data: dataVenue,
            async: false,
            headers: { 'Content-Type': 'application/json; charest=utf-8' }
        })
    }
    this.GetAllPackages = function (packagesData) {
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/GetPackages",
            data: packagesData,
            async: false,
            headers: { 'Content-Type': 'application/json; charest=utf-8' }
        })
    }
    this.GetServicesDetails = function (detailData) {
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/GetServicesDetails",
            data: detailData,
            async: false,
            headers: { 'Content-Type': 'application/json; charest=utf-8' }
        })
    }
    this.GetServicesImages = function (imagesData) {
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "Eventoviti/GetServicesImages",
            data: imagesData,
            async: false,
            headers: { 'Content-Type': 'application/json; charest=utf-8' }
        })
    }
    //By vineet
    this.SaveNewGuest = function (formData) {
        debugger;
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "TaskManagement/SaveNewGuest",
            data: formData,
            async: false,
            headers: { 'Content-Type': 'application/json; charest=utf-8' }
        })
    }
    //By vineet
    this.SaveExcelFile = function (formData) {
        
        return $http({
            method: 'POST',
            url: "/Data/SaveAndUpload",
            data: formData,
            async: false,
            headers: { 'Content-Type': undefined }
        })
    }
    //By vineet
    this.GetGuestListStatus = function () {

        return $http({
            method: 'GET',
            url: EventovitiCommon.GetLocalHost() + "TaskManagement/GetAllGuestListStatus",
            async: false,
            headers: { 'Content-Type': 'application/json; charest=utf-8' }
        })
    }
    //By vineet
    this.GetGuestListDetail = function () {

        return $http({
            method: 'GET',
            url: EventovitiCommon.GetLocalHost() + "TaskManagement/GetAllGuestListDetail",
            async: false,
            headers: { 'Content-Type': 'application/json; charest=utf-8' }
        })
    }
    //By vineet
    this.srvc_GetGuestDetailByID = function (formData) {

        return $http({
            method: 'GET',
            url: EventovitiCommon.GetLocalHost() + "TaskManagement/GetAllGuestListStatus",
            data: formData,
            async: false,
            headers: { 'Content-Type': 'application/json; charest=utf-8' }
        })
    }
    //By vineet
    this.SaveBulkData = function (formData) {
        debugger;
        return $http({
            method: 'POST',
            url: EventovitiCommon.GetLocalHost() + "TaskManagement/SaveBulkData",
            data: formData,
            async: false,
            headers: { 'Content-Type': 'application/json; charest=utf-8' }
        })
    }
    
    //By vineet
    this.SaveMultipleImages = function (formData) {

        return $http({
            method: 'POST',
            url: "/Data/SaveMultipleImages",
            data: formData,
            async: false,
            headers: { 'Content-Type': undefined }
        })
    }
    //By vineet
    this.SaveLogo = function (formData) {

        return $http({
            method: 'POST',
            url: "/Data/SaveLogo",
            data: formData,
            async: false,
            headers: { 'Content-Type': undefined }
        })
    }
    
    
});
app.service('Map', function ($q) {

    this.init = function () {
        debugger
        var options = {
            center: new google.maps.LatLng(40.7127837, -74.00594130000002),
            zoom: 13,
            disableDefaultUI: true
        }
        this.map = new google.maps.Map(
            document.getElementById("map"), options
        );
        this.places = new google.maps.places.PlacesService(this.map);
    }

    this.search = function (str) {
        debugger
        var d = $q.defer();
        this.places.textSearch({ query: str }, function (results, status) {
            if (status == 'OK') {
                d.resolve(results[0]);
            }
            else d.reject(status);
        });
        return d.promise;
    }

    this.addMarker = function (res) {
        debugger
        if (this.marker) this.marker.setMap(null);
        this.marker = new google.maps.Marker({
            map: this.map,
            position: res.geometry.location,
            animation: google.maps.Animation.DROP
        });
        this.map.setCenter(res.geometry.location);
    }

});