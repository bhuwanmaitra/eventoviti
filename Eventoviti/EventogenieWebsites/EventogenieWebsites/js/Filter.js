﻿/// <reference path="Controller.js" />

app.config(['$routeProvider', function ($routeProvider) {

    $routeProvider
        .when('/Dashboard', {
            templateUrl: "MainPage/Home.html",
            controller: "",
        })
        .when('/Login', {
            templateUrl: "MainPage/login-page.html",
            controller: "LoginCtrl",
        })
        .when('/ChossePage', {
            templateUrl: "MainPage/ChosseEventRegistration.html",
            controller: "ChossePageCtrl",
        })
        .when('/SignupPage/:id', {
            templateUrl: "MainPage/signup-page.html",
            controller: "VendorRegistrationCtrl",
        })
        .when('/SignupPage', {
            templateUrl: "MainPage/signup-page.html",
            controller: "VendorRegistrationCtrl",
        })
        .when('/VendorDashboard', {
            templateUrl: "DemoPages/HomePage.html",
            controller: "VendorDashboardCtrl",

        })
        .when('/vendorMaster', {
            templateUrl: "Index.html",
            controller: "vendorMasterCtrl",
        })
        .when('/userDashboard', {
            templateUrl: "Index.html",
            controller: "userDashboardCtrl",
        })
        .when('/NewEvent', {
            templateUrl: "Vendors/CreateNewEvent.html",
            controller: "CreateNewEvent",
        })
        .when('/TaskList', {
            templateUrl: "Vendors/TaskList.html",
            controller: "",
        })
        .when('/TeamMembers', {
            templateUrl: "Vendors/TeamMembers.html",
            controller: "CreateTeamCtrl",
        })
        .when('/TaskAllocation', {
            templateUrl: "Vendors/TaskAllocation.html",
            controller: "TaskAlloctionCtrl",
        })
        .when('/OngoingEvent', {
            templateUrl: "Vendors/OngoingEvent.html",
            controller: "OngoingEventCtrl",
        })
        .when('/UpcomingEvent', {
            templateUrl: "Vendors/UpcomingEvent.html",
            controller: "UpcomingEventCtrl",
        })
        .when('/ViewEvent', {
            templateUrl: "Vendors/ViewEvent.html",
            controller: "ViewEventCtrl",
        })
        .when('/SuccessPage', {
            templateUrl: "MainPage/SuccessPage.html",
            controller: "",
        })
        .when('/VenuManagement', {
            templateUrl: "Vendors/VenuManagement.html",
            controller: "VenuCtrl",
        })
        .when('/AboutUs', {
            templateUrl: "MainPage/AboutUs.html",
            controller: "",
        })
        .when('/VenueScheduleList', {
            templateUrl: "Vendors/VenueScheduleList.html",
            controller: "VenueScheduleCtrl",
        })
        .when('/ViewServices', {
            templateUrl: "Vendors/ViewServices.html",
            controller: "ViewServicesCtrl",
        })
        .when('/UserSingnUp?:id', {
            templateUrl: "MainPage/UserSingUp.html",
            controller: "ViewServicesCtrl",
        })
        .when('/AddNewGuest', {
            templateUrl: "GuestManagement/AddNewGuest.html",
            controller: "GuestManagementCtrl",
        })
        .when('/GuestListStatus', {
            templateUrl: "GuestManagement/GuestListStatus.html",
            controller: "GuestManagementCtrl",
        })
        .when('/AddNewGusetByHost', {
            templateUrl: "GuestManagement/AddNewGusetByHost.html",
            controller: "GuestManagementCtrl",
        })
        .when('/GuestBulkUpload', {
            templateUrl: "GuestManagement/GuestBulkUpload.html",
            controller: "GuestManagementCtrl",
        })
        .when('/AddLogoAndMedia', {
            templateUrl: "GuestManagement/AddLogoNMedia.html",
            controller: "GuestManagementCtrl",
        })


        .otherwise({
            redirectTo: '/Dashboard'
        });

}]);