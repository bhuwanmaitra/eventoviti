﻿var EventovitiCommon = function () {
    var EventovitiExt = {
        GetLocalHost: function () {
            return '../api/';
            //return 'https://connect.emeryvilleoccmed.com/SBR/API/Doctorsingin.asmx?op=';

        },
        LocalHost: function () {
            //return "http://localhost:55924/services/srvcTranscription.asmx?op=";
            return "https://connect.emeryvilleoccmed.com/services/srvcTranscription.asmx?op=";
        },
        GetResponseStatus: function (Status) {
            if (Status == 'Not Found') {
                return '203';
            }
            if (Status == 'Ok') {
                return '200';
            }
        },
    };
    return EventovitiExt;
}();