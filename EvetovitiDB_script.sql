USE [master]
GO
/****** Object:  Database [EvetovitiDatabase]    Script Date: 2/6/2018 05:18:58 PM ******/
CREATE DATABASE [EvetovitiDatabase] ON  PRIMARY 
( NAME = N'EvetovitiDatabase', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.EKESTO\MSSQL\DATA\EvetovitiDatabase.mdf' , SIZE = 2304KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'EvetovitiDatabase_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.EKESTO\MSSQL\DATA\EvetovitiDatabase_log.LDF' , SIZE = 832KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [EvetovitiDatabase] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [EvetovitiDatabase].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [EvetovitiDatabase] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [EvetovitiDatabase] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [EvetovitiDatabase] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [EvetovitiDatabase] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [EvetovitiDatabase] SET ARITHABORT OFF 
GO
ALTER DATABASE [EvetovitiDatabase] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [EvetovitiDatabase] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [EvetovitiDatabase] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [EvetovitiDatabase] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [EvetovitiDatabase] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [EvetovitiDatabase] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [EvetovitiDatabase] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [EvetovitiDatabase] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [EvetovitiDatabase] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [EvetovitiDatabase] SET  ENABLE_BROKER 
GO
ALTER DATABASE [EvetovitiDatabase] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [EvetovitiDatabase] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [EvetovitiDatabase] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [EvetovitiDatabase] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [EvetovitiDatabase] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [EvetovitiDatabase] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [EvetovitiDatabase] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [EvetovitiDatabase] SET RECOVERY FULL 
GO
ALTER DATABASE [EvetovitiDatabase] SET  MULTI_USER 
GO
ALTER DATABASE [EvetovitiDatabase] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [EvetovitiDatabase] SET DB_CHAINING OFF 
GO
USE [EvetovitiDatabase]
GO
/****** Object:  UserDefinedFunction [dbo].[SplitString]    Script Date: 2/6/2018 05:18:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[SplitString]
(    
      @Input NVARCHAR(MAX),
      @Character CHAR(1)
)
RETURNS @Output TABLE (
      Item NVARCHAR(1000)
)
AS
BEGIN
      DECLARE @StartIndex INT, @EndIndex INT
 
      SET @StartIndex = 1
      IF SUBSTRING(@Input, LEN(@Input) - 1, LEN(@Input)) <> @Character
      BEGIN
            SET @Input = @Input + @Character
      END
 
      WHILE CHARINDEX(@Character, @Input) > 0
      BEGIN
            SET @EndIndex = CHARINDEX(@Character, @Input)
           
            INSERT INTO @Output(Item)
            SELECT SUBSTRING(@Input, @StartIndex, @EndIndex - 1)
           
            SET @Input = SUBSTRING(@Input, @EndIndex + 1, LEN(@Input))
      END
 
      RETURN
END


GO
/****** Object:  Table [dbo].[ac_City]    Script Date: 2/6/2018 05:18:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_City](
	[CityID] [int] IDENTITY(1,1) NOT NULL,
	[CityName] [nvarchar](50) NULL,
 CONSTRAINT [PK_ac_City] PRIMARY KEY CLUSTERED 
(
	[CityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ac_EventCategoryType]    Script Date: 2/6/2018 05:18:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_EventCategoryType](
	[E_Cat_ID] [int] IDENTITY(1,1) NOT NULL,
	[E_Cat_Type] [nvarchar](150) NULL,
	[E_Cat_Image] [nvarchar](max) NULL,
 CONSTRAINT [PK_EventCategoryType] PRIMARY KEY CLUSTERED 
(
	[E_Cat_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ac_EventType]    Script Date: 2/6/2018 05:18:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_EventType](
	[ETID] [int] IDENTITY(1,1) NOT NULL,
	[VCID] [int] NULL,
	[EventType] [nvarchar](60) NULL,
	[EventTypeImage] [nvarchar](max) NULL,
 CONSTRAINT [PK_EventType] PRIMARY KEY CLUSTERED 
(
	[ETID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ac_Feature]    Script Date: 2/6/2018 05:18:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_Feature](
	[FId] [int] IDENTITY(1,1) NOT NULL,
	[EventId] [int] NULL,
	[FeatureName] [nvarchar](50) NULL,
 CONSTRAINT [PK_ac_Feature] PRIMARY KEY CLUSTERED 
(
	[FId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ac_group]    Script Date: 2/6/2018 05:18:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_group](
	[GroupId] [int] IDENTITY(1,1) NOT NULL,
	[GroupName] [nvarchar](50) NULL,
	[GroupBy] [int] NULL,
 CONSTRAINT [PK_ac_group] PRIMARY KEY CLUSTERED 
(
	[GroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ac_Highlights]    Script Date: 2/6/2018 05:18:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_Highlights](
	[HId] [int] IDENTITY(1,1) NOT NULL,
	[VSId] [int] NULL,
	[HName] [nvarchar](max) NULL,
 CONSTRAINT [PK_ac_Highlights] PRIMARY KEY CLUSTERED 
(
	[HId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ac_MenueMaster]    Script Date: 2/6/2018 05:18:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_MenueMaster](
	[MId] [int] IDENTITY(1,1) NOT NULL,
	[MLevel1] [nvarchar](50) NULL,
	[MLevel2] [nvarchar](50) NULL,
	[MLevel3] [nvarchar](50) NULL,
	[MName1] [nvarchar](50) NULL,
	[MName2] [nvarchar](50) NULL,
	[MName3] [nvarchar](50) NULL,
	[isActive] [bit] NULL,
 CONSTRAINT [PK_ac_MenueMaster] PRIMARY KEY CLUSTERED 
(
	[MId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ac_SubTask]    Script Date: 2/6/2018 05:18:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_SubTask](
	[SubTaskId] [int] IDENTITY(1,1) NOT NULL,
	[SubTaskName] [nvarchar](250) NULL,
	[DateAllocate] [datetime] NULL,
	[TimeAllocate] [time](7) NULL,
	[TaskId] [int] NULL,
 CONSTRAINT [PK_ac_SubTask] PRIMARY KEY CLUSTERED 
(
	[SubTaskId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ac_Task]    Script Date: 2/6/2018 05:18:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_Task](
	[TaskId] [int] IDENTITY(1,1) NOT NULL,
	[TaskName] [varchar](200) NULL,
	[TaskDate] [datetime] NULL,
	[ReminderDate] [datetime] NULL,
	[ReminderTime] [time](7) NULL,
	[IsReminderNotification] [bit] NULL,
	[IsReminderMail] [bit] NULL,
	[SubTaskId] [int] NULL,
	[TeamId] [int] NULL,
	[Priority] [int] NULL,
	[EventId] [int] NULL,
	[TaskAssignedBy] [int] NULL,
	[TaskDateFrom] [datetime] NULL,
	[TaskDateTo] [datetime] NULL,
	[TaskTimeFrom] [time](7) NULL,
	[TaskTimeTo] [time](7) NULL,
	[TaskDescription] [nvarchar](500) NULL,
 CONSTRAINT [PK_ac_Task] PRIMARY KEY CLUSTERED 
(
	[TaskId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_VebdorSubType]    Script Date: 2/6/2018 05:18:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ac_VebdorSubType](
	[VSId] [int] IDENTITY(1,1) NOT NULL,
	[VSName] [nvarchar](50) NULL,
	[E_Cat_ID] [int] NULL,
	[VSImages] [nvarchar](max) NULL,
	[Ctype] [char](2) NULL,
 CONSTRAINT [PK_VebdorSubType] PRIMARY KEY CLUSTERED 
(
	[VSId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ac_VenueType]    Script Date: 2/6/2018 05:18:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ac_VenueType](
	[VTId] [int] IDENTITY(1,1) NOT NULL,
	[VTName] [nvarchar](50) NULL,
 CONSTRAINT [PK_ac_VenueType] PRIMARY KEY CLUSTERED 
(
	[VTId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EventImagesVideoFiles]    Script Date: 2/6/2018 05:18:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventImagesVideoFiles](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[EventId] [int] NULL,
	[FilePath] [nvarchar](max) NULL,
	[Type] [nvarchar](50) NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
 CONSTRAINT [PK_EventImagesVideoFiles] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Guest]    Script Date: 2/6/2018 05:18:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Guest](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[EventID] [bigint] NOT NULL,
	[GuestName] [varchar](50) NULL,
	[IdProof] [nvarchar](max) NULL,
	[GuestNo] [int] NULL,
	[City] [varchar](50) NULL,
	[Address] [varchar](500) NULL,
	[Pincode] [int] NULL,
	[GuestGroup] [varchar](50) NULL,
	[ContactNo] [decimal](12, 0) NULL,
	[WhatsAppNo] [decimal](12, 0) NULL,
	[EmailId] [varchar](50) NULL,
	[AnySpecialRequiremnt] [varchar](max) NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[GuestStatus] [varchar](50) NULL,
	[RoomDetail] [varchar](50) NULL,
 CONSTRAINT [PK_Guest] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[u_Event]    Script Date: 2/6/2018 05:18:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[u_Event](
	[EventID] [int] IDENTITY(1,1) NOT NULL,
	[EProfilePic] [nvarchar](max) NULL,
	[EName] [nvarchar](50) NULL,
	[EOwnerName] [nvarchar](50) NULL,
	[EDateFrom] [datetime] NULL,
	[EDateTo] [datetime] NULL,
	[EAddress] [nvarchar](max) NULL,
	[ELan] [decimal](18, 7) NULL,
	[ELang] [decimal](18, 7) NULL,
	[EGuestCount] [nvarchar](50) NULL,
	[EType] [nvarchar](50) NULL,
	[GroupId] [nvarchar](50) NULL,
	[TeamId] [nvarchar](50) NULL,
	[isEHost] [bit] NULL,
	[EHostEmail] [nvarchar](50) NULL,
	[EFeatureId] [nvarchar](50) NULL,
	[CreateBy] [int] NULL,
	[ImageName] [nvarchar](max) NULL,
 CONSTRAINT [PK_u_CreateEvent] PRIMARY KEY CLUSTERED 
(
	[EventID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[u_Highlights]    Script Date: 2/6/2018 05:18:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[u_Highlights](
	[VHId] [int] IDENTITY(1,1) NOT NULL,
	[HId] [nvarchar](150) NULL,
	[WYear] [int] NULL,
	[EHandled] [int] NULL,
	[USP] [nvarchar](max) NULL,
	[CreatedBy] [int] NULL,
	[VenueId] [int] NULL,
 CONSTRAINT [PK_u_Highlights] PRIMARY KEY CLUSTERED 
(
	[VHId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[u_Packages]    Script Date: 2/6/2018 05:18:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[u_Packages](
	[PackageId] [int] IDENTITY(1,1) NOT NULL,
	[VenueId] [int] NULL,
	[PackageName] [nvarchar](150) NULL,
	[PackagePrice] [numeric](18, 2) NULL,
	[BookingAmount] [numeric](18, 2) NULL,
	[Capacity] [int] NULL,
	[PackageInfo] [nvarchar](max) NULL,
	[CreatedBy] [int] NULL,
 CONSTRAINT [PK_u_Packages] PRIMARY KEY CLUSTERED 
(
	[PackageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[u_Roles]    Script Date: 2/6/2018 05:18:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[u_Roles](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [varchar](50) NULL,
 CONSTRAINT [PK_u_Roles] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[u_StaySubVenue]    Script Date: 2/6/2018 05:18:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[u_StaySubVenue](
	[StayVTId] [int] IDENTITY(1,1) NOT NULL,
	[VSTId] [int] NULL,
	[SVTNames] [nvarchar](50) NULL,
	[SQuantity] [int] NULL,
	[StayCapacity] [int] NULL,
	[CreatedBy] [int] NULL,
	[VenueId] [int] NULL,
 CONSTRAINT [PK_u_StaySubVenue] PRIMARY KEY CLUSTERED 
(
	[StayVTId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[u_Team]    Script Date: 2/6/2018 05:18:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[u_Team](
	[TeamId] [int] IDENTITY(1,1) NOT NULL,
	[MemberEmail] [nvarchar](100) NULL,
	[MemberName] [nvarchar](250) NULL,
	[RoleId] [int] NULL,
	[GroupId] [int] NULL,
	[IsAdmin] [bit] NULL,
 CONSTRAINT [PK_u_Team] PRIMARY KEY CLUSTERED 
(
	[TeamId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[u_VenueAccientType]    Script Date: 2/6/2018 05:18:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[u_VenueAccientType](
	[VTypeId] [int] IDENTITY(1,1) NOT NULL,
	[VSTId] [int] NULL,
	[VTNames] [nvarchar](50) NULL,
	[SCapacity] [int] NULL,
	[FCapacity] [int] NULL,
	[CreatedBy] [int] NULL,
	[VenueId] [int] NULL,
 CONSTRAINT [PK_u_VenueAccientType] PRIMARY KEY CLUSTERED 
(
	[VTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[u_VenueGallery]    Script Date: 2/6/2018 05:18:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[u_VenueGallery](
	[VImageId] [int] IDENTITY(1,1) NOT NULL,
	[VenueId] [int] NULL,
	[VIName] [nvarchar](max) NULL,
	[CreatedBy] [int] NULL,
 CONSTRAINT [PK_u_VenueGallery] PRIMARY KEY CLUSTERED 
(
	[VImageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[u_VenueManagement]    Script Date: 2/6/2018 05:18:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[u_VenueManagement](
	[VenueId] [int] IDENTITY(1,1) NOT NULL,
	[VenueName] [nvarchar](50) NULL,
	[VenueAddress] [nvarchar](max) NULL,
	[VLng] [decimal](18, 7) NULL,
	[VLong] [decimal](18, 7) NULL,
	[ETID] [nvarchar](350) NULL,
	[SpecialityTags] [nvarchar](max) NULL,
	[ContactNo] [nvarchar](50) NULL,
	[ContactTimeFrom] [time](7) NULL,
	[ContactTimeTo] [time](7) NULL,
	[VenueDetails] [nvarchar](max) NULL,
	[CreatedBy] [int] NULL,
	[EmailId] [nvarchar](50) NULL,
	[OutLocation] [bit] NULL,
 CONSTRAINT [PK_u_VenueManagement] PRIMARY KEY CLUSTERED 
(
	[VenueId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[u_VenuePrivacyPolicy]    Script Date: 2/6/2018 05:18:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[u_VenuePrivacyPolicy](
	[VPCId] [int] IDENTITY(1,1) NOT NULL,
	[VenueId] [int] NULL,
	[TermSevice] [bit] NULL CONSTRAINT [DF_u_VenuePrivacyPolicy_TermSevice]  DEFAULT ((0)),
	[TSDetails] [nvarchar](max) NULL,
	[BookingTerms] [bit] NULL CONSTRAINT [DF_u_VenuePrivacyPolicy_BookingTerms]  DEFAULT ((0)),
	[BTDetails] [nvarchar](max) NULL,
	[CancellationServices] [bit] NULL CONSTRAINT [DF_u_VenuePrivacyPolicy_CancellationServices]  DEFAULT ((0)),
	[CSDetails] [nvarchar](max) NULL,
	[TravellingTerms] [bit] NULL CONSTRAINT [DF_u_VenuePrivacyPolicy_TravellingTerms]  DEFAULT ((0)),
	[TTDetails] [nvarchar](max) NULL,
	[CreatedBy] [int] NULL,
 CONSTRAINT [PK_u_VenuePrivacyPolicy] PRIMARY KEY CLUSTERED 
(
	[VPCId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[u_VenueSchedule]    Script Date: 2/6/2018 05:18:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[u_VenueSchedule](
	[VSId] [int] IDENTITY(1,1) NOT NULL,
	[VenueId] [int] NULL,
	[VSDay] [nvarchar](50) NULL,
	[VSDateFrom] [datetime] NULL,
	[VSDateTo] [datetime] NULL,
	[VSTimeFrom] [nvarchar](15) NULL,
	[VSTimeto] [nvarchar](15) NULL,
	[CreatedBy] [int] NULL,
 CONSTRAINT [PK_u_VenueSchedule] PRIMARY KEY CLUSTERED 
(
	[VSId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Vendor_Registration]    Script Date: 2/6/2018 05:18:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vendor_Registration](
	[VRId] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](250) NULL,
	[VCompanyName] [nvarchar](50) NULL,
	[Address] [nvarchar](max) NULL,
	[PhoneNo] [nvarchar](50) NULL,
	[VEmailId] [nvarchar](50) NULL,
	[VPassword] [nvarchar](50) NULL,
	[VImage] [nvarchar](max) NULL,
	[UserType] [nvarchar](10) NULL,
 CONSTRAINT [PK_Vendor_Registration] PRIMARY KEY CLUSTERED 
(
	[VRId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VRegistrationType]    Script Date: 2/6/2018 05:18:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VRegistrationType](
	[VTRId] [int] IDENTITY(1,1) NOT NULL,
	[VRId] [int] NULL,
	[E_Cat_ID] [int] NULL,
	[VSId] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NULL,
 CONSTRAINT [PK_VRegistrationType] PRIMARY KEY CLUSTERED 
(
	[VTRId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[ac_City] ON 

GO
INSERT [dbo].[ac_City] ([CityID], [CityName]) VALUES (1, N'Ahmedabad')
GO
SET IDENTITY_INSERT [dbo].[ac_City] OFF
GO
SET IDENTITY_INSERT [dbo].[ac_EventCategoryType] ON 

GO
INSERT [dbo].[ac_EventCategoryType] ([E_Cat_ID], [E_Cat_Type], [E_Cat_Image]) VALUES (13, N'Venue', N'local-map-icon-mapicon.png')
GO
INSERT [dbo].[ac_EventCategoryType] ([E_Cat_ID], [E_Cat_Type], [E_Cat_Image]) VALUES (14, N'Event Manager', N'pi_2014_icon_3_rounded.png')
GO
INSERT [dbo].[ac_EventCategoryType] ([E_Cat_ID], [E_Cat_Type], [E_Cat_Image]) VALUES (15, N'Vendors', N'1470253448_vector_65_02.png')
GO
SET IDENTITY_INSERT [dbo].[ac_EventCategoryType] OFF
GO
SET IDENTITY_INSERT [dbo].[ac_EventType] ON 

GO
INSERT [dbo].[ac_EventType] ([ETID], [VCID], [EventType], [EventTypeImage]) VALUES (1, 13, N'Party Venu', N'icons8-Party-48.png')
GO
INSERT [dbo].[ac_EventType] ([ETID], [VCID], [EventType], [EventTypeImage]) VALUES (2, 13, N'Marriage venue', N'icons8-Wedding Day-48.png')
GO
INSERT [dbo].[ac_EventType] ([ETID], [VCID], [EventType], [EventTypeImage]) VALUES (1002, 2, N'Flower', N'icons8-Wedding Day-48.png')
GO
SET IDENTITY_INSERT [dbo].[ac_EventType] OFF
GO
SET IDENTITY_INSERT [dbo].[ac_Feature] ON 

GO
INSERT [dbo].[ac_Feature] ([FId], [EventId], [FeatureName]) VALUES (1, 1, N'venue management')
GO
INSERT [dbo].[ac_Feature] ([FId], [EventId], [FeatureName]) VALUES (2, 1, N'team management')
GO
INSERT [dbo].[ac_Feature] ([FId], [EventId], [FeatureName]) VALUES (3, 1, N'schedule management')
GO
INSERT [dbo].[ac_Feature] ([FId], [EventId], [FeatureName]) VALUES (4, 1, N'vendor management')
GO
INSERT [dbo].[ac_Feature] ([FId], [EventId], [FeatureName]) VALUES (5, 1, N'guest management')
GO
SET IDENTITY_INSERT [dbo].[ac_Feature] OFF
GO
SET IDENTITY_INSERT [dbo].[ac_group] ON 

GO
INSERT [dbo].[ac_group] ([GroupId], [GroupName], [GroupBy]) VALUES (1, N'GroupA', 2)
GO
INSERT [dbo].[ac_group] ([GroupId], [GroupName], [GroupBy]) VALUES (3, N'GroupC', 3)
GO
INSERT [dbo].[ac_group] ([GroupId], [GroupName], [GroupBy]) VALUES (4, N'Photography', 2)
GO
INSERT [dbo].[ac_group] ([GroupId], [GroupName], [GroupBy]) VALUES (7, N'ipoioio', 0)
GO
SET IDENTITY_INSERT [dbo].[ac_group] OFF
GO
SET IDENTITY_INSERT [dbo].[ac_Highlights] ON 

GO
INSERT [dbo].[ac_Highlights] ([HId], [VSId], [HName]) VALUES (1, 1, N'Hospitality Desk')
GO
INSERT [dbo].[ac_Highlights] ([HId], [VSId], [HName]) VALUES (2, 1, N'Decor/Theme Design')
GO
INSERT [dbo].[ac_Highlights] ([HId], [VSId], [HName]) VALUES (3, 1, N'Decor Execution')
GO
INSERT [dbo].[ac_Highlights] ([HId], [VSId], [HName]) VALUES (4, 1, N'Logistics Management')
GO
SET IDENTITY_INSERT [dbo].[ac_Highlights] OFF
GO
SET IDENTITY_INSERT [dbo].[ac_VebdorSubType] ON 

GO
INSERT [dbo].[ac_VebdorSubType] ([VSId], [VSName], [E_Cat_ID], [VSImages], [Ctype]) VALUES (1, N'Gardens And Lawns
', 13, N'local-map-icon-mapicon.png', N'VH')
GO
INSERT [dbo].[ac_VebdorSubType] ([VSId], [VSName], [E_Cat_ID], [VSImages], [Ctype]) VALUES (2, N'choreographers
', 15, N'1470253448_vector_65_02.png', N'VE')
GO
INSERT [dbo].[ac_VebdorSubType] ([VSId], [VSName], [E_Cat_ID], [VSImages], [Ctype]) VALUES (3, N'caterers
', 15, N'1470253448_vector_65_02.png', N'VE')
GO
INSERT [dbo].[ac_VebdorSubType] ([VSId], [VSName], [E_Cat_ID], [VSImages], [Ctype]) VALUES (4, N'Florists & decorators & Cake Designers
', 15, N'1470253448_vector_65_02.png', N'VE')
GO
INSERT [dbo].[ac_VebdorSubType] ([VSId], [VSName], [E_Cat_ID], [VSImages], [Ctype]) VALUES (5, N'Transport services and drivers
', 15, N'1470253448_vector_65_02.png', N'VE')
GO
INSERT [dbo].[ac_VebdorSubType] ([VSId], [VSName], [E_Cat_ID], [VSImages], [Ctype]) VALUES (6, N'musicians and artists
', 15, N'1470253448_vector_65_02.png', N'VE')
GO
INSERT [dbo].[ac_VebdorSubType] ([VSId], [VSName], [E_Cat_ID], [VSImages], [Ctype]) VALUES (7, N'workers and helpers
', 15, N'1470253448_vector_65_02.png', N'VE')
GO
INSERT [dbo].[ac_VebdorSubType] ([VSId], [VSName], [E_Cat_ID], [VSImages], [Ctype]) VALUES (8, N'anchors
', 15, N'1470253448_vector_65_02.png', N'VE')
GO
INSERT [dbo].[ac_VebdorSubType] ([VSId], [VSName], [E_Cat_ID], [VSImages], [Ctype]) VALUES (9, N'Sound system and DJ
', 15, N'1470253448_vector_65_02.png', N'VE')
GO
INSERT [dbo].[ac_VebdorSubType] ([VSId], [VSName], [E_Cat_ID], [VSImages], [Ctype]) VALUES (10, N'Makeup artists  & mehandi designers
', 15, N'1470253448_vector_65_02.png', N'VE')
GO
INSERT [dbo].[ac_VebdorSubType] ([VSId], [VSName], [E_Cat_ID], [VSImages], [Ctype]) VALUES (11, N'Banquets
', 13, N'local-map-icon-mapicon.png', N'VH')
GO
INSERT [dbo].[ac_VebdorSubType] ([VSId], [VSName], [E_Cat_ID], [VSImages], [Ctype]) VALUES (12, N'Auditorium
', 13, N'local-map-icon-mapicon.png', N'VH')
GO
INSERT [dbo].[ac_VebdorSubType] ([VSId], [VSName], [E_Cat_ID], [VSImages], [Ctype]) VALUES (13, N'Board And Meeting Rooms
', 13, N'local-map-icon-mapicon.png', N'VH')
GO
INSERT [dbo].[ac_VebdorSubType] ([VSId], [VSName], [E_Cat_ID], [VSImages], [Ctype]) VALUES (14, N'Open Terrace Venues
', 13, N'local-map-icon-mapicon.png', N'VH')
GO
INSERT [dbo].[ac_VebdorSubType] ([VSId], [VSName], [E_Cat_ID], [VSImages], [Ctype]) VALUES (15, N'Hotel
', 13, N'local-map-icon-mapicon.png', N'VR')
GO
INSERT [dbo].[ac_VebdorSubType] ([VSId], [VSName], [E_Cat_ID], [VSImages], [Ctype]) VALUES (16, N'Villas And Farm Houses
', 13, N'local-map-icon-mapicon.png', N'VR')
GO
INSERT [dbo].[ac_VebdorSubType] ([VSId], [VSName], [E_Cat_ID], [VSImages], [Ctype]) VALUES (17, N'Resorts
', 13, N'local-map-icon-mapicon.png', N'VR')
GO
INSERT [dbo].[ac_VebdorSubType] ([VSId], [VSName], [E_Cat_ID], [VSImages], [Ctype]) VALUES (18, N'Beach Venues
', 13, N'local-map-icon-mapicon.png', N'VR')
GO
INSERT [dbo].[ac_VebdorSubType] ([VSId], [VSName], [E_Cat_ID], [VSImages], [Ctype]) VALUES (19, N'Destination Wedding
', 13, N'local-map-icon-mapicon.png', N'VR')
GO
SET IDENTITY_INSERT [dbo].[ac_VebdorSubType] OFF
GO
SET IDENTITY_INSERT [dbo].[ac_VenueType] ON 

GO
INSERT [dbo].[ac_VenueType] ([VTId], [VTName]) VALUES (1, N'Banquet Hall')
GO
INSERT [dbo].[ac_VenueType] ([VTId], [VTName]) VALUES (2, N'garden banquet hall')
GO
INSERT [dbo].[ac_VenueType] ([VTId], [VTName]) VALUES (3, N'Garden')
GO
SET IDENTITY_INSERT [dbo].[ac_VenueType] OFF
GO
SET IDENTITY_INSERT [dbo].[Guest] ON 

GO
INSERT [dbo].[Guest] ([id], [EventID], [GuestName], [IdProof], [GuestNo], [City], [Address], [Pincode], [GuestGroup], [ContactNo], [WhatsAppNo], [EmailId], [AnySpecialRequiremnt], [CreatedBy], [CreatedOn], [GuestStatus], [RoomDetail]) VALUES (1, 0, N'Sonam singh', NULL, 2, N'jsr', NULL, NULL, N'Function', CAST(9523677564 AS Decimal(12, 0)), CAST(8603669520 AS Decimal(12, 0)), NULL, NULL, NULL, CAST(N'2018-01-10 13:50:52.613' AS DateTime), N'Guest Added', NULL)
GO
INSERT [dbo].[Guest] ([id], [EventID], [GuestName], [IdProof], [GuestNo], [City], [Address], [Pincode], [GuestGroup], [ContactNo], [WhatsAppNo], [EmailId], [AnySpecialRequiremnt], [CreatedBy], [CreatedOn], [GuestStatus], [RoomDetail]) VALUES (2, 0, N'Rahul Yadav', NULL, 3, N'jsr', NULL, NULL, N'Function', CAST(9523677564 AS Decimal(12, 0)), CAST(8603669520 AS Decimal(12, 0)), NULL, NULL, NULL, CAST(N'2018-01-10 13:50:52.613' AS DateTime), N'Guest Added', NULL)
GO
INSERT [dbo].[Guest] ([id], [EventID], [GuestName], [IdProof], [GuestNo], [City], [Address], [Pincode], [GuestGroup], [ContactNo], [WhatsAppNo], [EmailId], [AnySpecialRequiremnt], [CreatedBy], [CreatedOn], [GuestStatus], [RoomDetail]) VALUES (3, 0, N'Abijeet singh', NULL, 4, N'jsr', NULL, NULL, N'Function', CAST(9523677564 AS Decimal(12, 0)), CAST(8603669520 AS Decimal(12, 0)), NULL, NULL, NULL, CAST(N'2018-01-10 13:50:52.613' AS DateTime), N'Guest Added', NULL)
GO
INSERT [dbo].[Guest] ([id], [EventID], [GuestName], [IdProof], [GuestNo], [City], [Address], [Pincode], [GuestGroup], [ContactNo], [WhatsAppNo], [EmailId], [AnySpecialRequiremnt], [CreatedBy], [CreatedOn], [GuestStatus], [RoomDetail]) VALUES (4, 0, N'Sonam singh', NULL, 2, N'jsr', NULL, NULL, N'Function', CAST(9523677564 AS Decimal(12, 0)), CAST(8603669520 AS Decimal(12, 0)), NULL, NULL, NULL, CAST(N'2018-01-10 13:54:53.520' AS DateTime), N'Guest Added', NULL)
GO
INSERT [dbo].[Guest] ([id], [EventID], [GuestName], [IdProof], [GuestNo], [City], [Address], [Pincode], [GuestGroup], [ContactNo], [WhatsAppNo], [EmailId], [AnySpecialRequiremnt], [CreatedBy], [CreatedOn], [GuestStatus], [RoomDetail]) VALUES (5, 0, N'Rahul Yadav', NULL, 3, N'jsr', NULL, NULL, N'Function', CAST(9523677564 AS Decimal(12, 0)), CAST(8603669520 AS Decimal(12, 0)), NULL, NULL, NULL, CAST(N'2018-01-10 13:54:53.520' AS DateTime), N'Guest Added', NULL)
GO
INSERT [dbo].[Guest] ([id], [EventID], [GuestName], [IdProof], [GuestNo], [City], [Address], [Pincode], [GuestGroup], [ContactNo], [WhatsAppNo], [EmailId], [AnySpecialRequiremnt], [CreatedBy], [CreatedOn], [GuestStatus], [RoomDetail]) VALUES (6, 0, N'Abijeet singh', NULL, 4, N'jsr', NULL, NULL, N'Function', CAST(9523677564 AS Decimal(12, 0)), CAST(8603669520 AS Decimal(12, 0)), NULL, NULL, NULL, CAST(N'2018-01-10 13:54:53.520' AS DateTime), N'Guest Added', NULL)
GO
SET IDENTITY_INSERT [dbo].[Guest] OFF
GO
SET IDENTITY_INSERT [dbo].[u_Event] ON 

GO
INSERT [dbo].[u_Event] ([EventID], [EProfilePic], [EName], [EOwnerName], [EDateFrom], [EDateTo], [EAddress], [ELan], [ELang], [EGuestCount], [EType], [GroupId], [TeamId], [isEHost], [EHostEmail], [EFeatureId], [CreateBy], [ImageName]) VALUES (1, N'avatar.jpg', N'sad', N'Aditya', CAST(N'2017-11-16 00:00:00.000' AS DateTime), CAST(N'2017-11-20 00:00:00.000' AS DateTime), N'Sarthi Avenue, Satellite, Ahmedabad, Gujarat, India', CAST(-21.0214000 AS Decimal(18, 7)), CAST(214.2451900 AS Decimal(18, 7)), N'233', N'1,2', N'1', N'2', 0, NULL, N'2,5,4', 2, N'0Screenshot_20170914-172311.png')
GO
INSERT [dbo].[u_Event] ([EventID], [EProfilePic], [EName], [EOwnerName], [EDateFrom], [EDateTo], [EAddress], [ELan], [ELang], [EGuestCount], [EType], [GroupId], [TeamId], [isEHost], [EHostEmail], [EFeatureId], [CreateBy], [ImageName]) VALUES (4, N'card-profile6-square.jpg', N'Business Party', N'Della', CAST(N'2017-11-18 00:00:00.000' AS DateTime), CAST(N'2017-11-23 00:00:00.000' AS DateTime), N'San Francisco, CA, United States', CAST(-21.0214000 AS Decimal(18, 7)), CAST(214.2451900 AS Decimal(18, 7)), N'201', N'1,1002', N'2', N'2', 0, NULL, N'2,5,4', 2, N'0SMD.png')
GO
INSERT [dbo].[u_Event] ([EventID], [EProfilePic], [EName], [EOwnerName], [EDateFrom], [EDateTo], [EAddress], [ELan], [ELang], [EGuestCount], [EType], [GroupId], [TeamId], [isEHost], [EHostEmail], [EFeatureId], [CreateBy], [ImageName]) VALUES (5, N'08142021943343449170-account_id=1.jpg', N'asd', N'dsads', CAST(N'2017-12-08 00:00:00.000' AS DateTime), CAST(N'2017-12-06 00:00:00.000' AS DateTime), N'Ahmedabad, Gujarat, India', CAST(23.0200000 AS Decimal(18, 7)), CAST(72.5700000 AS Decimal(18, 7)), N'202', N'2,1002', N'[1,3]', N'[1,2]', 0, NULL, N'5,4,2', 2, N'08142021943343449170-account_id=1.jpg')
GO
SET IDENTITY_INSERT [dbo].[u_Event] OFF
GO
SET IDENTITY_INSERT [dbo].[u_Highlights] ON 

GO
INSERT [dbo].[u_Highlights] ([VHId], [HId], [WYear], [EHandled], [USP], [CreatedBy], [VenueId]) VALUES (1, N'1,2', 343, 3434, N'sdfdf fdsfdsf<div>dsfsd</div><div>fsd</div><div>&nbsp;ds</div><div>fsd</div><div>f sd</div><div>ff</div>', 2, 23)
GO
INSERT [dbo].[u_Highlights] ([VHId], [HId], [WYear], [EHandled], [USP], [CreatedBy], [VenueId]) VALUES (2, N'3,4', 23, 223, N'dsfsdfds fsd<div>yhis&nbsp;</div><div>thto&nbsp;</div><div>sdfikjksdf</div><div>jjksdfk</div><div>dsfkjkjsdf</div><div>ldskf;</div><div>sdfklksdf</div><div>sdfksdjfsdf</div><div>sdfkjkjsdf</div><div><br></div>', 2, 24)
GO
INSERT [dbo].[u_Highlights] ([VHId], [HId], [WYear], [EHandled], [USP], [CreatedBy], [VenueId]) VALUES (3, N'1,2', 3, 3, N'asdsa', 2, 25)
GO
INSERT [dbo].[u_Highlights] ([VHId], [HId], [WYear], [EHandled], [USP], [CreatedBy], [VenueId]) VALUES (4, N'3,2', 142, 14, N'<i>sdfdsf sdf</i><b style=""><i>sdfsdf sd</i>&nbsp;sdf sdf sdf sd sdfsdf sd fds&nbsp;<i>&nbsp;sdf sd sdf ds sd</i></b><div><b style=""><i>fd sf</i></b></div><div><b style=""><i>sf sd</i></b></div><div><b style=""><i>f ds</i></b></div><div><b style=""><i>&nbsp;fs f</i></b></div><div><b style=""><i>f&nbsp;</i></b></div><div><b style=""><i>s fs</i></b></div><div><b style=""><i>&nbsp;ds&nbsp;</i></b></div><div><b style=""><i>s&nbsp;</i></b></div><div><b style=""><i>&nbsp;sdfdsfdsf&nbsp;</i></b></div>', 2, 26)
GO
INSERT [dbo].[u_Highlights] ([VHId], [HId], [WYear], [EHandled], [USP], [CreatedBy], [VenueId]) VALUES (5, N'1,2', 774, 74714, N'SAD<div><i>SAD</i></div><div><i><b>ASD</b></i></div><div><i><b>SADRTRET</b></i></div><div><i><b>ERTRET BHFCHGFG</b></i></div><div><i><b>HFH GFHGFH</b></i></div><div><i><b>GGFHGFHFHGF</b></i></div><div><i><b>SFDFDS</b></i></div>', 2, 27)
GO
INSERT [dbo].[u_Highlights] ([VHId], [HId], [WYear], [EHandled], [USP], [CreatedBy], [VenueId]) VALUES (6, N'[1,3,4]', 14, 144, N'Party<div>catering and buffet dinner&nbsp;</div>', 2, 28)
GO
SET IDENTITY_INSERT [dbo].[u_Highlights] OFF
GO
SET IDENTITY_INSERT [dbo].[u_Packages] ON 

GO
INSERT [dbo].[u_Packages] ([PackageId], [VenueId], [PackageName], [PackagePrice], [BookingAmount], [Capacity], [PackageInfo], [CreatedBy]) VALUES (1, 0, NULL, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), 0, NULL, 0)
GO
INSERT [dbo].[u_Packages] ([PackageId], [VenueId], [PackageName], [PackagePrice], [BookingAmount], [Capacity], [PackageInfo], [CreatedBy]) VALUES (2, 0, NULL, CAST(0.00 AS Numeric(18, 2)), CAST(0.00 AS Numeric(18, 2)), 0, NULL, 0)
GO
INSERT [dbo].[u_Packages] ([PackageId], [VenueId], [PackageName], [PackagePrice], [BookingAmount], [Capacity], [PackageInfo], [CreatedBy]) VALUES (3, 0, N'this', CAST(747.00 AS Numeric(18, 2)), CAST(747.00 AS Numeric(18, 2)), 4, N'sada asdsads
sadasd sadsa
d sadsad 
sadsa
dsa
dsa
dsa
dsa
 dsa
dsa
dsa
d sa
dsadsadsad sadsadsad sa sad sa
asdasdsa', 0)
GO
INSERT [dbo].[u_Packages] ([PackageId], [VenueId], [PackageName], [PackagePrice], [BookingAmount], [Capacity], [PackageInfo], [CreatedBy]) VALUES (4, 12, N'Room1', CAST(1222.00 AS Numeric(18, 2)), CAST(1222.00 AS Numeric(18, 2)), 2, N'This is the best way of living your hollidays', 2)
GO
INSERT [dbo].[u_Packages] ([PackageId], [VenueId], [PackageName], [PackagePrice], [BookingAmount], [Capacity], [PackageInfo], [CreatedBy]) VALUES (5, 14, N'Rooms', CAST(1200.00 AS Numeric(18, 2)), CAST(1200.00 AS Numeric(18, 2)), 2, N'This pafes', 2)
GO
INSERT [dbo].[u_Packages] ([PackageId], [VenueId], [PackageName], [PackagePrice], [BookingAmount], [Capacity], [PackageInfo], [CreatedBy]) VALUES (6, 15, N'asd', CAST(942.00 AS Numeric(18, 2)), CAST(942.00 AS Numeric(18, 2)), 2, N'asdasd sadasdsada dsas', 2)
GO
INSERT [dbo].[u_Packages] ([PackageId], [VenueId], [PackageName], [PackagePrice], [BookingAmount], [Capacity], [PackageInfo], [CreatedBy]) VALUES (7, 16, N'asd', CAST(780.00 AS Numeric(18, 2)), CAST(780.00 AS Numeric(18, 2)), 4, N'asdsdaxc adsdasd sadsad ad sad sad asd
asda
s dsad sad sa
das dsad 
as dsad 
asd asd 
asd asd 
asd asd 
as', 2)
GO
INSERT [dbo].[u_Packages] ([PackageId], [VenueId], [PackageName], [PackagePrice], [BookingAmount], [Capacity], [PackageInfo], [CreatedBy]) VALUES (8, 17, N'Foods', CAST(9000.00 AS Numeric(18, 2)), CAST(9000.00 AS Numeric(18, 2)), 10, N'this is the packages data to be inserted', 2)
GO
INSERT [dbo].[u_Packages] ([PackageId], [VenueId], [PackageName], [PackagePrice], [BookingAmount], [Capacity], [PackageInfo], [CreatedBy]) VALUES (9, 23, N'dasd', CAST(32423.00 AS Numeric(18, 2)), CAST(32423.00 AS Numeric(18, 2)), 234, N'24234sdf dsf
dsfds
 fs
 fds
f 
dsf', 2)
GO
INSERT [dbo].[u_Packages] ([PackageId], [VenueId], [PackageName], [PackagePrice], [BookingAmount], [Capacity], [PackageInfo], [CreatedBy]) VALUES (10, 27, N'ASAS', CAST(34534.00 AS Numeric(18, 2)), CAST(34534.00 AS Numeric(18, 2)), 34, N'ASDDFDSF SDFDS
F SDFSD
FSDF sdfssd f
 dsfdsfdsfsdf
sdds fsdf
ds s d
dsfsdf
sdf ds
fsdf
dsfds', 2)
GO
INSERT [dbo].[u_Packages] ([PackageId], [VenueId], [PackageName], [PackagePrice], [BookingAmount], [Capacity], [PackageInfo], [CreatedBy]) VALUES (11, 27, N'dataTpp', CAST(333.25 AS Numeric(18, 2)), CAST(333.25 AS Numeric(18, 2)), 214, N'ASDDFDSF SDFDS
F SDFSD
FSDF sdfssd f
 dsfdsfdsfsdf
sdds fsdf
ds s d
dsfsdf
sdf ds
fsdf
dsfds', 2)
GO
INSERT [dbo].[u_Packages] ([PackageId], [VenueId], [PackageName], [PackagePrice], [BookingAmount], [Capacity], [PackageInfo], [CreatedBy]) VALUES (13, 28, N'Food', CAST(145.00 AS Numeric(18, 2)), CAST(145.00 AS Numeric(18, 2)), 1, N'Glass Container Set 
BPA free and leak proof. 
A little heavier, but great, if you don’t want to microwave your food in plastic containers. 
I have a set, and I’m super satisfied – works excellent with soups', 2)
GO
SET IDENTITY_INSERT [dbo].[u_Packages] OFF
GO
SET IDENTITY_INSERT [dbo].[u_Roles] ON 

GO
INSERT [dbo].[u_Roles] ([RoleId], [RoleName]) VALUES (1, N'Manager')
GO
INSERT [dbo].[u_Roles] ([RoleId], [RoleName]) VALUES (2, N'Assistant Manager')
GO
INSERT [dbo].[u_Roles] ([RoleId], [RoleName]) VALUES (3, N'Photographer')
GO
SET IDENTITY_INSERT [dbo].[u_Roles] OFF
GO
SET IDENTITY_INSERT [dbo].[u_StaySubVenue] ON 

GO
INSERT [dbo].[u_StaySubVenue] ([StayVTId], [VSTId], [SVTNames], [SQuantity], [StayCapacity], [CreatedBy], [VenueId]) VALUES (1, 15, N'Room1', 5, 2, 2, 17)
GO
INSERT [dbo].[u_StaySubVenue] ([StayVTId], [VSTId], [SVTNames], [SQuantity], [StayCapacity], [CreatedBy], [VenueId]) VALUES (2, 15, N'Room2', 10, 3, 2, 17)
GO
INSERT [dbo].[u_StaySubVenue] ([StayVTId], [VSTId], [SVTNames], [SQuantity], [StayCapacity], [CreatedBy], [VenueId]) VALUES (3, 16, N'Room3', 4, 1, 2, 17)
GO
INSERT [dbo].[u_StaySubVenue] ([StayVTId], [VSTId], [SVTNames], [SQuantity], [StayCapacity], [CreatedBy], [VenueId]) VALUES (4, 16, N'dasd', 1, 2, 2, 18)
GO
INSERT [dbo].[u_StaySubVenue] ([StayVTId], [VSTId], [SVTNames], [SQuantity], [StayCapacity], [CreatedBy], [VenueId]) VALUES (5, 15, N'Sdas', 12, 1, 2, 18)
GO
INSERT [dbo].[u_StaySubVenue] ([StayVTId], [VSTId], [SVTNames], [SQuantity], [StayCapacity], [CreatedBy], [VenueId]) VALUES (6, 0, N'asdsadsd', 32, 2, 2, 19)
GO
INSERT [dbo].[u_StaySubVenue] ([StayVTId], [VSTId], [SVTNames], [SQuantity], [StayCapacity], [CreatedBy], [VenueId]) VALUES (7, 0, N'sadsadd', 223, 2, 2, 20)
GO
INSERT [dbo].[u_StaySubVenue] ([StayVTId], [VSTId], [SVTNames], [SQuantity], [StayCapacity], [CreatedBy], [VenueId]) VALUES (8, 0, N'dsaa', 232, 232, 2, 21)
GO
INSERT [dbo].[u_StaySubVenue] ([StayVTId], [VSTId], [SVTNames], [SQuantity], [StayCapacity], [CreatedBy], [VenueId]) VALUES (9, 0, N'3423as', 234, 23432, 2, 22)
GO
INSERT [dbo].[u_StaySubVenue] ([StayVTId], [VSTId], [SVTNames], [SQuantity], [StayCapacity], [CreatedBy], [VenueId]) VALUES (10, 0, N'23sdad', 232, 23, 2, 23)
GO
INSERT [dbo].[u_StaySubVenue] ([StayVTId], [VSTId], [SVTNames], [SQuantity], [StayCapacity], [CreatedBy], [VenueId]) VALUES (11, 15, N'sadsadad sad', 23423, 234, 2, 24)
GO
INSERT [dbo].[u_StaySubVenue] ([StayVTId], [VSTId], [SVTNames], [SQuantity], [StayCapacity], [CreatedBy], [VenueId]) VALUES (12, 0, N'qwe', 33, 2, 2, 25)
GO
INSERT [dbo].[u_StaySubVenue] ([StayVTId], [VSTId], [SVTNames], [SQuantity], [StayCapacity], [CreatedBy], [VenueId]) VALUES (13, 0, N'wer', 234, 234, 2, 26)
GO
INSERT [dbo].[u_StaySubVenue] ([StayVTId], [VSTId], [SVTNames], [SQuantity], [StayCapacity], [CreatedBy], [VenueId]) VALUES (14, 0, N'sad44', 44, 234, 2, 27)
GO
INSERT [dbo].[u_StaySubVenue] ([StayVTId], [VSTId], [SVTNames], [SQuantity], [StayCapacity], [CreatedBy], [VenueId]) VALUES (15, 0, N'Premium', 12, 14, 2, 28)
GO
INSERT [dbo].[u_StaySubVenue] ([StayVTId], [VSTId], [SVTNames], [SQuantity], [StayCapacity], [CreatedBy], [VenueId]) VALUES (16, 0, N'', 25, 25, 0, 29)
GO
INSERT [dbo].[u_StaySubVenue] ([StayVTId], [VSTId], [SVTNames], [SQuantity], [StayCapacity], [CreatedBy], [VenueId]) VALUES (17, 0, N'aa', 54, 21, 0, 30)
GO
SET IDENTITY_INSERT [dbo].[u_StaySubVenue] OFF
GO
SET IDENTITY_INSERT [dbo].[u_Team] ON 

GO
INSERT [dbo].[u_Team] ([TeamId], [MemberEmail], [MemberName], [RoleId], [GroupId], [IsAdmin]) VALUES (2, N'shrivastava.aditya002@gmail.com', N'vikas', 2, 3, NULL)
GO
INSERT [dbo].[u_Team] ([TeamId], [MemberEmail], [MemberName], [RoleId], [GroupId], [IsAdmin]) VALUES (3, N'shrivastava.aditya002@gmail.com', N'Rohan', 1, 2, NULL)
GO
INSERT [dbo].[u_Team] ([TeamId], [MemberEmail], [MemberName], [RoleId], [GroupId], [IsAdmin]) VALUES (4, N'shrivastava.aditya002@gmail.com', N'Tina', 1, 4, NULL)
GO
INSERT [dbo].[u_Team] ([TeamId], [MemberEmail], [MemberName], [RoleId], [GroupId], [IsAdmin]) VALUES (5, N'shrivastava.aditya002@gmail.com', N'Shela', 1, 2, NULL)
GO
INSERT [dbo].[u_Team] ([TeamId], [MemberEmail], [MemberName], [RoleId], [GroupId], [IsAdmin]) VALUES (9, N'vikas.vanvi@gmail.com', N'vikas', 2, 2, 1)
GO
INSERT [dbo].[u_Team] ([TeamId], [MemberEmail], [MemberName], [RoleId], [GroupId], [IsAdmin]) VALUES (10, N'nitrick.srivastava002@gmail.com', N'Nitrick', 3, 4, 0)
GO
INSERT [dbo].[u_Team] ([TeamId], [MemberEmail], [MemberName], [RoleId], [GroupId], [IsAdmin]) VALUES (11, N'aa', N'aa', 2, 1, 0)
GO
INSERT [dbo].[u_Team] ([TeamId], [MemberEmail], [MemberName], [RoleId], [GroupId], [IsAdmin]) VALUES (13, N'as', N'sss', 1, 1, 1)
GO
INSERT [dbo].[u_Team] ([TeamId], [MemberEmail], [MemberName], [RoleId], [GroupId], [IsAdmin]) VALUES (14, N'as', N's', 1, 4, 0)
GO
SET IDENTITY_INSERT [dbo].[u_Team] OFF
GO
SET IDENTITY_INSERT [dbo].[u_VenueAccientType] ON 

GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (1, 2, NULL, 1, 1, 2, 8)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (2, 3, NULL, 1, 11, 2, 8)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (3, 3, NULL, 1, 1341, 2, 9)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (4, 2, NULL, 1, 1433, 2, 9)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (5, 4, NULL, 1, 1, 2, 10)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (6, 3, NULL, 2, 223, 2, 11)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (7, 2, NULL, 1, 1200, 2, 12)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (8, 2, NULL, 1, 2300, 2, 12)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (9, 5, NULL, 1, 1220, 2, 13)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (10, 3, NULL, 1, 2300, 2, 14)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (11, 5, N'This', 1, 2010, 2, 14)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (12, 2, N'This', 1, 2100, 2, 15)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (13, 3, N'qwe', 3, 1500, 2, 16)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (14, 1, N'Premium', 1, 2100, 2, 17)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (15, 1, N'Golden', 2, 2400, 2, 17)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (16, 0, N'Friday special', 112, 145, 2, 18)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (17, 0, N'Distinct', 252, 252, 2, 18)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (18, 12, N'sadsad', 222, 34343, 2, 19)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (19, 11, N'asds', 12, 12, 2, 20)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (20, 1, N'3244sad', 232, 2323, 2, 21)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (21, 1, N'qwe', 232, 342, 2, 22)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (22, 12, N'sdf', 3223, 2323, 2, 23)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (23, 13, N'dsfsf', 124, 124, 2, 24)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (24, 12, N'erw', 333, 3322, 2, 25)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (25, 11, N'sdf', 114, 144, 2, 26)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (26, 12, N'sad', 3434, 454, 2, 27)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (27, 11, N'Primium', 2500, 1500, 2, 28)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (28, 11, N'a', 250, 21, 0, 29)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (29, 12, N'a', 320, 20, 0, 29)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (30, 12, N'14', 25, 14, 0, 30)
GO
INSERT [dbo].[u_VenueAccientType] ([VTypeId], [VSTId], [VTNames], [SCapacity], [FCapacity], [CreatedBy], [VenueId]) VALUES (31, 11, N'21', 2, 32, 0, 30)
GO
SET IDENTITY_INSERT [dbo].[u_VenueAccientType] OFF
GO
SET IDENTITY_INSERT [dbo].[u_VenueGallery] ON 

GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (1, 0, N'apple-icon.png', 0)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (2, 0, N'bg-pricing.jpg', 0)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (3, 0, N'card-1.jpg', 0)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (4, 0, N'card-2.jpg', 0)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (5, 0, N'card-3.jpg', 0)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (6, 12, N'12sidebar-1.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (7, 12, N'12sidebar-2.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (8, 12, N'12sidebar-3.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (9, 12, N'12sidebar-4.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (10, 13, N'product1.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (11, 13, N'product2.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (12, 13, N'product3.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (13, 13, N'register.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (14, 14, N'lock.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (15, 14, N'login.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (16, 15, N'DSC_0032.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (17, 15, N'DSC_0033.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (18, 15, N'DSC_0034.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (19, 16, N'b_1.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (20, 16, N'b_2.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (21, 16, N'b_3.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (22, 16, N'b_4.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (23, 17, N'cards-test.png', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (24, 17, N'content-test.png', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (25, 17, N'ex-contact.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (26, 17, N'ex-login.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (27, 23, N'1.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (28, 23, N'2.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (29, 23, N'3.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (30, 23, N'4.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (31, 23, N'5.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (32, 23, N'6.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (33, 23, N'7.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (34, 24, N'1.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (35, 24, N'2.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (36, 24, N'3.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (37, 24, N'4.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (38, 25, N'1.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (39, 25, N'2.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (40, 25, N'3.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (41, 25, N'1.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (42, 25, N'2.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (43, 25, N'3.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (44, 26, N'1.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (45, 26, N'2.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (46, 26, N'3.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (47, 27, N'cover.jpeg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (48, 27, N'sidebar-1.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (49, 27, N'sidebar-2.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (50, 27, N'sidebar-4.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (51, 28, N'1.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (52, 28, N'2.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (53, 28, N'3.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (54, 28, N'4.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (55, 28, N'5.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (56, 28, N'6.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (57, 28, N'7.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (58, 28, N'8.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (59, 28, N'9.jpg', 2)
GO
INSERT [dbo].[u_VenueGallery] ([VImageId], [VenueId], [VIName], [CreatedBy]) VALUES (60, 28, N'10.jpg', 2)
GO
SET IDENTITY_INSERT [dbo].[u_VenueGallery] OFF
GO
SET IDENTITY_INSERT [dbo].[u_VenueManagement] ON 

GO
INSERT [dbo].[u_VenueManagement] ([VenueId], [VenueName], [VenueAddress], [VLng], [VLong], [ETID], [SpecialityTags], [ContactNo], [ContactTimeFrom], [ContactTimeTo], [VenueDetails], [CreatedBy], [EmailId], [OutLocation]) VALUES (2, N'Platinum Banquet Hall', N'100 ft Ring Road, Prahlad Nagar, Ahmedabad, Gujarat 380015, India', CAST(23.0100000 AS Decimal(18, 7)), CAST(72.5000000 AS Decimal(18, 7)), N'1,2', N'Hotel', NULL, NULL, NULL, N'This is a wedding hall and have good experience in handling the marriages', 2, NULL, NULL)
GO
INSERT [dbo].[u_VenueManagement] ([VenueId], [VenueName], [VenueAddress], [VLng], [VLong], [ETID], [SpecialityTags], [ContactNo], [ContactTimeFrom], [ContactTimeTo], [VenueDetails], [CreatedBy], [EmailId], [OutLocation]) VALUES (5, N'Cambay Weddings', N'Thaltej, Ahmedabad, Gujarat 380059, India', CAST(23.0600000 AS Decimal(18, 7)), CAST(72.4900000 AS Decimal(18, 7)), N'1,2', N'Hotel', NULL, NULL, NULL, N'this is for weddings', 2, NULL, NULL)
GO
INSERT [dbo].[u_VenueManagement] ([VenueId], [VenueName], [VenueAddress], [VLng], [VLong], [ETID], [SpecialityTags], [ContactNo], [ContactTimeFrom], [ContactTimeTo], [VenueDetails], [CreatedBy], [EmailId], [OutLocation]) VALUES (12, N'K K Farm', N'203, Sindhu Bhavan Marg, PRL Colony, Bodakdev, Ahmedabad, Gujarat 380054, India', CAST(23.0400000 AS Decimal(18, 7)), CAST(72.5000000 AS Decimal(18, 7)), N'1,2', N'Demo Tags', N'9409618903', CAST(N'07:19:49' AS Time), CAST(N'19:19:55' AS Time), N'This is done by Aditya', 2, NULL, NULL)
GO
INSERT [dbo].[u_VenueManagement] ([VenueId], [VenueName], [VenueAddress], [VLng], [VLong], [ETID], [SpecialityTags], [ContactNo], [ContactTimeFrom], [ContactTimeTo], [VenueDetails], [CreatedBy], [EmailId], [OutLocation]) VALUES (14, N'Lemon Tree Hotel, Ahmedabad', N'Off Nehru Bridge, Sabarmati River Front, Towards Khanpur, Khanpur, Ahmedabad, Gujarat 380001, India', CAST(23.0200000 AS Decimal(18, 7)), CAST(72.5700000 AS Decimal(18, 7)), N'1,2', N'Demo Tags', N'9409618903', CAST(N'15:18:14' AS Time), CAST(N'03:18:15' AS Time), N'Business hotel in Ahmedabad, located near the Airport. Book direct to get 
exclusive deals, Free Wi-Fi, Breakfast and Pay@Hotel. Lowest rates guaranteed!', 2, NULL, NULL)
GO
INSERT [dbo].[u_VenueManagement] ([VenueId], [VenueName], [VenueAddress], [VLng], [VLong], [ETID], [SpecialityTags], [ContactNo], [ContactTimeFrom], [ContactTimeTo], [VenueDetails], [CreatedBy], [EmailId], [OutLocation]) VALUES (17, N'Ginger Hotel', N'Opp. Rajpath Club, Next to Gordhan Thal, S.G. Road, Bodakdev, Ahmedabad –, 380054, Gujarat, Bodakdev, Ahmedabad, Gujarat 380054, India', CAST(23.0300000 AS Decimal(18, 7)), CAST(72.5100000 AS Decimal(18, 7)), N'"1","2"', N'Party', N'940918902', CAST(N'00:35:50' AS Time), CAST(N'12:35:52' AS Time), N'Enjoy your stay at the modern city of Ahmedabad without burning a hole in your 
pocket. Ginger Ahmedabad invites you to an indulgence where reasonability and 
hospitality redefines itself. Book a room today at great prices and enjoy a 
comfortable stay at one of the finest branded budget hotels in Ahmedabad near 
Railway', 2, NULL, NULL)
GO
INSERT [dbo].[u_VenueManagement] ([VenueId], [VenueName], [VenueAddress], [VLng], [VLong], [ETID], [SpecialityTags], [ContactNo], [ContactTimeFrom], [ContactTimeTo], [VenueDetails], [CreatedBy], [EmailId], [OutLocation]) VALUES (25, N'sad', N'Saddle Brook, NJ 07663, USA', CAST(40.9000000 AS Decimal(18, 7)), CAST(-74.1000000 AS Decimal(18, 7)), N'""', N'Demo Tags, Tags of days', N'3242234', CAST(N'15:35:56' AS Time), CAST(N'15:35:57' AS Time), N'sdf', 2, N'we@dfds.com', NULL)
GO
INSERT [dbo].[u_VenueManagement] ([VenueId], [VenueName], [VenueAddress], [VLng], [VLong], [ETID], [SpecialityTags], [ContactNo], [ContactTimeFrom], [ContactTimeTo], [VenueDetails], [CreatedBy], [EmailId], [OutLocation]) VALUES (26, N'sadsa', N'Halsema Highway, Bauko, Mountain Province, Philippines', CAST(16.8800000 AS Decimal(18, 7)), CAST(120.8700000 AS Decimal(18, 7)), N'""', N'Demo Tags, Tags of days', N'324', CAST(N'16:59:00' AS Time), CAST(N'16:59:49' AS Time), N'sdf', 2, N'as@s.com', NULL)
GO
INSERT [dbo].[u_VenueManagement] ([VenueId], [VenueName], [VenueAddress], [VLng], [VLong], [ETID], [SpecialityTags], [ContactNo], [ContactTimeFrom], [ContactTimeTo], [VenueDetails], [CreatedBy], [EmailId], [OutLocation]) VALUES (27, N'sad', N'Tollgate Rd, London E6 5JP, UK', CAST(51.5100000 AS Decimal(18, 7)), CAST(0.0500000 AS Decimal(18, 7)), N'"1","2"', N'Demo Tags, Tags of days', N'2423234', CAST(N'17:03:25' AS Time), CAST(N'17:03:26' AS Time), N'dasfdsflk', 2, N'asdhg@hgd.com', NULL)
GO
INSERT [dbo].[u_VenueManagement] ([VenueId], [VenueName], [VenueAddress], [VLng], [VLong], [ETID], [SpecialityTags], [ContactNo], [ContactTimeFrom], [ContactTimeTo], [VenueDetails], [CreatedBy], [EmailId], [OutLocation]) VALUES (28, N'Cambay Sapphire', N'132 ft Ring Road,, Adjacent to Jivraj Park Over-Bridge, Vejalpur Cross Road, Vallabhacharya Society, Vejalpur, Ahmedabad, Gujarat 380051, India', CAST(23.0000000 AS Decimal(18, 7)), CAST(72.5300000 AS Decimal(18, 7)), N'""', N'Partys,Marriages,DJ Nights', N'7926828001', CAST(N'07:55:33' AS Time), CAST(N'21:54:52' AS Time), N'A 2-minute walk from a bus stop, this low-key hotel is 12 km from the Saabaramatee aashram and Ghandi museum 7 km from Maaneek cauk and Nehru Bridge. 

Featuring leather furnishings and wood floors, the casual rooms offer free Wi-Fi, flat-screen TVs, safes, minibars, and tea and coffeemaking facilities. Room service is available.

Breakfast and parking are free, and there’s a casual international restaurant, an outdoor pool and a gym', 2, N'Shrivastava.aditya002@gmail.com', NULL)
GO
INSERT [dbo].[u_VenueManagement] ([VenueId], [VenueName], [VenueAddress], [VLng], [VLong], [ETID], [SpecialityTags], [ContactNo], [ContactTimeFrom], [ContactTimeTo], [VenueDetails], [CreatedBy], [EmailId], [OutLocation]) VALUES (29, N'Meny', N'Amarillo, TX, USA', CAST(35.2200000 AS Decimal(18, 7)), CAST(-101.8300000 AS Decimal(18, 7)), N'""', N'Tags of days,asas', N'8603669516', CAST(N'19:47:06' AS Time), CAST(N'19:47:08' AS Time), N'as', 0, N'test@h.com', NULL)
GO
INSERT [dbo].[u_VenueManagement] ([VenueId], [VenueName], [VenueAddress], [VLng], [VLong], [ETID], [SpecialityTags], [ContactNo], [ContactTimeFrom], [ContactTimeTo], [VenueDetails], [CreatedBy], [EmailId], [OutLocation]) VALUES (30, N'Function', N'Jamshedpur, Jharkhand, India', CAST(22.8000000 AS Decimal(18, 7)), CAST(86.2000000 AS Decimal(18, 7)), N'""', N'Demo Tags, Tags of days', N'8603669520', CAST(N'11:58:45' AS Time), CAST(N'11:58:46' AS Time), N'asdas', 0, N'bhwuan@gmai.com', NULL)
GO
INSERT [dbo].[u_VenueManagement] ([VenueId], [VenueName], [VenueAddress], [VLng], [VLong], [ETID], [SpecialityTags], [ContactNo], [ContactTimeFrom], [ContactTimeTo], [VenueDetails], [CreatedBy], [EmailId], [OutLocation]) VALUES (31, N'Masnufacturing', N'Jamshedpur, Jharkhand, India', CAST(22.8000000 AS Decimal(18, 7)), CAST(86.2000000 AS Decimal(18, 7)), N'""', N'Demo Tags, Tags of days', N'8603669520', CAST(N'12:21:00' AS Time), CAST(N'12:21:00' AS Time), NULL, 0, N'b@g.com', NULL)
GO
SET IDENTITY_INSERT [dbo].[u_VenueManagement] OFF
GO
SET IDENTITY_INSERT [dbo].[u_VenuePrivacyPolicy] ON 

GO
INSERT [dbo].[u_VenuePrivacyPolicy] ([VPCId], [VenueId], [TermSevice], [TSDetails], [BookingTerms], [BTDetails], [CancellationServices], [CSDetails], [TravellingTerms], [TTDetails], [CreatedBy]) VALUES (1, 12, 1, N'<center style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Arial, Helvetica, sans-serif; color: rgb(51, 51, 51); background-color: rgb(254, 254, 254);"><strong style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;">Privacy Notice</strong></center><p style="box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);">This privacy notice discloses the privacy practices for&nbsp;<span style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; text-decoration-line: underline;">(website address)</span>. This privacy notice applies solely to information collected by this website. It will notify you of the following:</p><ol type="1" style="box-sizing: content-box; margin-right: 0px; margin-bottom: 0px; margin-left: 40px; padding: 0px 0px 0px 25px; border: 0px; outline: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Arial, Helvetica, sans-serif; list-style-position: initial; list-style-image: initial; color: rgb(51, 51, 51); background-color: rgb(254, 254, 254);"><li style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: decimal;">What personally identifiable information is collected from you through the website, how it is used and with whom it may be shared.</li><li style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: decimal;">What choices are available to you regarding the use of your data.</li><li style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: decimal;">The security procedures in place to protect the misuse of your information.</li><li style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: decimal;">How you can correct any inaccuracies in the information.</li></ol><p style="box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);"><strong style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;">Information Collection, Use, and Sharing</strong>&nbsp;<br style="box-sizing: content-box;">We are the sole owners of the information collected on this site. We only have access to/collect information that you voluntarily give us via email or other direct contact from you. We will not sell or rent this information to anyone.</p><p style="box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);">We will use your information to respond to you, regarding the reason you contacted us. We will not share your information with any third party outside of our organization, other than as necessary to fulfill your request, e.g. to ship an order.</p><p style="box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);">Unless you ask us not to, we may contact you via email in the future to tell you about specials, new products or services, or changes to this privacy policy.</p><p style="box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);"><strong style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;">Your Access to and Control Over Information</strong>&nbsp;<br style="box-sizing: content-box;">You may opt out of any future contacts from us at any time. You can do the following at any time by contacting us via the email address or phone number given on our website:</p><ul style="box-sizing: content-box; margin-top: 0px; margin-bottom: 0px; margin-left: 40px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Arial, Helvetica, sans-serif; list-style: none; color: rgb(51, 51, 51); background-color: rgb(254, 254, 254);"><li style="box-sizing: content-box; margin: 0px 0px 0px 30px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: disc;">See what data we have about you, if any.</li><li style="box-sizing: content-box; margin: 0px 0px 0px 30px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: disc;">Change/correct any data we have about you.</li><li style="box-sizing: content-box; margin: 0px 0px 0px 30px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: disc;">Have us delete any data we have about you.</li><li style="box-sizing: content-box; margin: 0px 0px 0px 30px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: disc;">Express any concern you have about our use of your data.</li></ul><p style="box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);"><strong style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;">Security</strong>&nbsp;<br style="box-sizing: content-box;">We take precautions to protect your information. When you submit sensitive information via the website, your information is protected both online and offline.</p><p style="box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);">Wherever we collect sensitive information (such as credit card data), that information is encrypted and transmitted to us in a secure way. You can verify this by looking for a lock icon in the address bar and looking for "https" at the beginning of the address of the Web page.</p><p style="box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);">While we use encryption to protect sensitive information transmitted online, we also protect your information offline. Only employees who need the information to perform a specific job (for example, billing or customer service) are granted access to personally identifiable information. The computers/servers in which we store personally identifiable information are kept in a secure environment.</p><p style="box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);"><strong style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;">If you feel that we are not abiding by this privacy policy, you should contact us immediately via telephone at&nbsp;<span style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; text-decoration-line: underline;">XXX YYY-ZZZZ</span>&nbsp;or&nbsp;<span style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; text-decoration-line: underline;">via email</span>.</strong></p>', 0, NULL, 0, NULL, 0, NULL, 2)
GO
INSERT [dbo].[u_VenuePrivacyPolicy] ([VPCId], [VenueId], [TermSevice], [TSDetails], [BookingTerms], [BTDetails], [CancellationServices], [CSDetails], [TravellingTerms], [TTDetails], [CreatedBy]) VALUES (2, 14, 1, N'<center style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Arial, Helvetica, sans-serif; color: rgb(51, 51, 51); background-color: rgb(254, 254, 254);"><strong style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;">Privacy Notice</strong></center><p style="box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);">This privacy notice discloses the privacy practices for&nbsp;<span style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; text-decoration-line: underline;">(website address)</span>. This privacy notice applies solely to information collected by this website. It will notify you of the following:</p><ol type="1" style="box-sizing: content-box; margin-right: 0px; margin-bottom: 0px; margin-left: 40px; padding: 0px 0px 0px 25px; border: 0px; outline: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Arial, Helvetica, sans-serif; list-style-position: initial; list-style-image: initial; color: rgb(51, 51, 51); background-color: rgb(254, 254, 254);"><li style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: decimal;">What personally identifiable information is collected from you through the website, how it is used and with whom it may be shared.</li><li style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: decimal;">What choices are available to you regarding the use of your data.</li><li style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: decimal;">The security procedures in place to protect the misuse of your information.</li><li style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: decimal;">How you can correct any inaccuracies in the information.</li></ol><p style="box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);"><strong style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;">Information Collection, Use, and Sharing</strong>&nbsp;<br style="box-sizing: content-box;">We are the sole owners of the information collected on this site. We only have access to/collect information that you voluntarily give us via email or other direct contact from you. We will not sell or rent this information to anyone.</p><p style="box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);">We will use your information to respond to you, regarding the reason you contacted us. We will not share your information with any third party outside of our organization, other than as necessary to fulfill your request, e.g. to ship an order.</p><p style="box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);">Unless you ask us not to, we may contact you via email in the future to tell you about specials, new products or services, or changes to this privacy policy.</p><p style="box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);"><strong style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;">Your Access to and Control Over Information</strong>&nbsp;<br style="box-sizing: content-box;">You may opt out of any future contacts from us at any time. You can do the following at any time by contacting us via the email address or phone number given on our website:</p><ul style="box-sizing: content-box; margin-top: 0px; margin-bottom: 0px; margin-left: 40px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Arial, Helvetica, sans-serif; list-style: none; color: rgb(51, 51, 51); background-color: rgb(254, 254, 254);"><li style="box-sizing: content-box; margin: 0px 0px 0px 30px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: disc;">See what data we have about you, if any.</li><li style="box-sizing: content-box; margin: 0px 0px 0px 30px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: disc;">Change/correct any data we have about you.</li><li style="box-sizing: content-box; margin: 0px 0px 0px 30px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: disc;">Have us delete any data we have about you.</li><li style="box-sizing: content-box; margin: 0px 0px 0px 30px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: disc;">Express any concern you have about our use of your data.</li></ul><p style="box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);"><strong style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;">Security</strong>&nbsp;<br style="box-sizing: content-box;">We take precautions to protect your information. When you submit sensitive information via the website, your information is protected both online and offline.</p><p style="box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);">Wherever we collect sensitive information (such as credit card data), that information is encrypted and transmitted to us in a secure way. You can verify this by looking for a lock icon in the address bar and looking for "https" at the beginning of the address of the Web page.</p><p style="box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);">While we use encryption to protect sensitive information transmitted online, we also protect your information offline. Only employees who need the information to perform a specific job (for example, billing or customer service) are granted access to personally identifiable information. The computers/servers in which we store personally identifiable information are kept in a secure environment.</p><p style="box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);"><strong style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;">If you feel that we are not abiding by this privacy policy, you should contact us immediately via telephone at&nbsp;<span style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; text-decoration-line: underline;">XXX YYY-ZZZZ</span>&nbsp;or&nbsp;<span style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; text-decoration-line: underline;">via email</span>.</strong></p>', 0, NULL, 0, NULL, 0, NULL, 2)
GO
INSERT [dbo].[u_VenuePrivacyPolicy] ([VPCId], [VenueId], [TermSevice], [TSDetails], [BookingTerms], [BTDetails], [CancellationServices], [CSDetails], [TravellingTerms], [TTDetails], [CreatedBy]) VALUES (3, 16, 1, N'asd saddas<div>asdsadnknndvsvd</div><div>dsvmvnfdsaklfn&nbsp;</div><div>lmsaldalmg</div><div>ssjfksdfjkj</div><div>dsflsdf'';</div><div><br></div><div><br></div><p>asdllasdlldsadsad</p><h3>asd;asdasdlkcvmdnkdnfsdfd</h3><div>sadasdasdsad</div>', 0, NULL, 0, NULL, 0, NULL, 2)
GO
INSERT [dbo].[u_VenuePrivacyPolicy] ([VPCId], [VenueId], [TermSevice], [TSDetails], [BookingTerms], [BTDetails], [CancellationServices], [CSDetails], [TravellingTerms], [TTDetails], [CreatedBy]) VALUES (4, 17, 1, N'<center style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Arial, Helvetica, sans-serif; color: rgb(51, 51, 51); background-color: rgb(254, 254, 254);"><strong style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;">Privacy Notice</strong></center><p style="box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);">This privacy notice discloses the privacy practices for&nbsp;<span style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; text-decoration-line: underline;">(website address)</span>. This privacy notice applies solely to information collected by this website. It will notify you of the following:</p><ol type="1" style="box-sizing: content-box; margin-right: 0px; margin-bottom: 0px; margin-left: 40px; padding: 0px 0px 0px 25px; border: 0px; outline: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Arial, Helvetica, sans-serif; list-style-position: initial; list-style-image: initial; color: rgb(51, 51, 51); background-color: rgb(254, 254, 254);"><li style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: decimal;">What personally identifiable information is collected from you through the website, how it is used and with whom it may be shared.</li><li style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: decimal;">What choices are available to you regarding the use of your data.</li><li style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: decimal;">The security procedures in place to protect the misuse of your information.</li><li style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: decimal;">How you can correct any inaccuracies in the information.</li></ol><p style="box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);"><strong style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;">Information Collection, Use, and Sharing</strong>&nbsp;<br style="box-sizing: content-box;">We are the sole owners of the information collected on this site. We only have access to/collect information that you voluntarily give us via email or other direct contact from you. We will not sell or rent this information to anyone.</p><p style="box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);">We will use your information to respond to you, regarding the reason you contacted us. We will not share your information with any third party outside of our organization, other than as necessary to fulfill your request, e.g. to ship an order.</p><p style="box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);">Unless you ask us not to, we may contact you via email in the future to tell you about specials, new products or services, or changes to this privacy policy.</p><p style="box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);"><strong style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;">Your Access to and Control Over Information</strong>&nbsp;<br style="box-sizing: content-box;">You may opt out of any future contacts from us at any time. You can do the following at any time by contacting us via the email address or phone number given on our website:</p><ul style="box-sizing: content-box; margin-top: 0px; margin-bottom: 0px; margin-left: 40px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Arial, Helvetica, sans-serif; list-style: none; color: rgb(51, 51, 51); background-color: rgb(254, 254, 254);"><li style="box-sizing: content-box; margin: 0px 0px 0px 30px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: disc;">See what data we have about you, if any.</li><li style="box-sizing: content-box; margin: 0px 0px 0px 30px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: disc;">Change/correct any data we have about you.</li><li style="box-sizing: content-box; margin: 0px 0px 0px 30px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: disc;">Have us delete any data we have about you.</li><li style="box-sizing: content-box; margin: 0px 0px 0px 30px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: disc;">Express any concern you have about our use of your data.</li></ul><p style="box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);"><strong style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;">Security</strong>&nbsp;<br style="box-sizing: content-box;">We take precautions to protect your information. When you submit sensitive information via the website, your information is protected both online and offline.</p><p style="box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);">Wherever we collect sensitive information (such as credit card data), that information is encrypted and transmitted to us in a secure way. You can verify this by looking for a lock icon in the address bar and looking for "https" at the beginning of the address of the Web page.</p><p style="box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);">While we use encryption to protect sensitive information transmitted online, we also protect your information offline. Only employees who need the information to perform a specific job (for example, billing or customer service) are granted access to personally identifiable information. The computers/servers in which we store personally identifiable information are kept in a secure environment.</p><p style="box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);"><strong style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;">If you feel that we are not abiding by this privacy policy, you should contact us immediately via telephone at&nbsp;<span style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; text-decoration-line: underline;">XXX YYY-ZZZZ</span>&nbsp;or&nbsp;<span style="box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; text-decoration-line: underline;">via email</span>.</strong></p>', 0, NULL, 0, NULL, 0, NULL, 2)
GO
INSERT [dbo].[u_VenuePrivacyPolicy] ([VPCId], [VenueId], [TermSevice], [TSDetails], [BookingTerms], [BTDetails], [CancellationServices], [CSDetails], [TravellingTerms], [TTDetails], [CreatedBy]) VALUES (5, 23, 1, N'nmsdnfsdjsdfklsdnfdsf<div>sd</div><div>fsdfdsfkldjskfjdskf;ljdf</div><div>sdf</div><div>dsf''dsfdskfjdskfjdsf</div><div>dsfsdfdsklf;dsjfsdlfkdsfdsf</div><div>sdfkkfsdjfdjsfjsfd</div><div>sdfsdkfjsdfjkdsf</div><div>sdfkjsdfsdjfd</div><div>sdfdsffds</div>', 0, NULL, 0, NULL, 0, NULL, 2)
GO
INSERT [dbo].[u_VenuePrivacyPolicy] ([VPCId], [VenueId], [TermSevice], [TSDetails], [BookingTerms], [BTDetails], [CancellationServices], [CSDetails], [TravellingTerms], [TTDetails], [CreatedBy]) VALUES (6, 25, 1, N'sadsad<div>sadasad</div><div>asd</div>', 0, NULL, 0, NULL, 0, NULL, 2)
GO
INSERT [dbo].[u_VenuePrivacyPolicy] ([VPCId], [VenueId], [TermSevice], [TSDetails], [BookingTerms], [BTDetails], [CancellationServices], [CSDetails], [TravellingTerms], [TTDetails], [CreatedBy]) VALUES (7, 27, 1, N'asdsad<div>asdas</div><div>asdasd</div><div>asdsad</div>', 1, N'dasdasd<div>sadsad</div><div>asdsad</div><div>sads</div><div>sadsad</div><div>sad</div><div>sadsa</div><div>dsad</div><div><br></div>', 1, N'asdasdsad<div>sad</div><div>sad</div><div>sad</div><div>sad</div><div>sad</div><div>sadsa</div><div>sad</div><div>sad</div><div>sad</div><div>sad</div><div>asd</div>', 0, NULL, 2)
GO
INSERT [dbo].[u_VenuePrivacyPolicy] ([VPCId], [VenueId], [TermSevice], [TSDetails], [BookingTerms], [BTDetails], [CancellationServices], [CSDetails], [TravellingTerms], [TTDetails], [CreatedBy]) VALUES (8, 28, 1, N'<p><span style="color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 16px; background-color: transparent;"><b>A Terms and Conditions could legally protect your ecommerce store. It is in this type of legal agreement where you set the rules that your customers would follow during a purchase and limit your liability in the event that your products fail.</b></span></p>', 1, N'<p><b>All fares and prices quoted are subject to availability. Prices are subject to change until full payment is received and voucher(s) are issued. Prices that are quoted include all taxes and fees unless we advises otherwise. New information.</b></p>', 1, N'<ol><li><b>&nbsp;Cancellation more than a week in advance : 15% administration fee</b></li><li><b>Cancellation between a week and 48 hours prior to departure date: 50% of the total amount plus administration fees that might be imposed</b></li><li><b>Cancellation less than 48 hours prior to departure date and time or no-show : 100% of the total amount</b></li><li><b>Tickets for Events or Attractions are non-refundable</b></li><li><b>Airport, Limousine or Coach – transfer services are non-refundable</b></li></ol>', 1, N'<p style="outline: 0px; margin-bottom: 0px; padding: 0px 0px 10px; color: rgb(0, 0, 0); text-align: justify; font-family: calibri !important; line-height: 26px !important; font-size: 17px !important; font-variant-numeric: normal !important; font-variant-east-asian: normal !important; font-stretch: normal !important;"><b>For all the services contracted, certain advance payment should be made to hold the booking, on confirmed basis &amp; the balance amount can be paid either before your departure from your country or upon arrival in INDIA but definitely before the commencement of the services. Management personnels hold the right to decide upon the amount to be paid as advance payment, based on the nature of the service &amp; the time left for the commencement of the service.</b></p><p style="outline: 0px; margin-bottom: 0px; padding: 0px 0px 10px; color: rgb(0, 0, 0); text-align: justify; font-family: calibri !important; line-height: 26px !important; font-size: 17px !important; font-variant-numeric: normal !important; font-variant-east-asian: normal !important; font-stretch: normal !important;"><b>Apart from above in some cases like Special Train Journeys, hotels or resorts bookings during the peak season (X-Mas, New Year), full payment is required to be sent in advance.</b></p>', 2)
GO
SET IDENTITY_INSERT [dbo].[u_VenuePrivacyPolicy] OFF
GO
SET IDENTITY_INSERT [dbo].[u_VenueSchedule] ON 

GO
INSERT [dbo].[u_VenueSchedule] ([VSId], [VenueId], [VSDay], [VSDateFrom], [VSDateTo], [VSTimeFrom], [VSTimeto], [CreatedBy]) VALUES (5, 5, N'Monday', CAST(N'2017-12-06 00:00:00.000' AS DateTime), CAST(N'2018-03-03 00:00:00.000' AS DateTime), N'10:00:00', N'18:00:00', 2)
GO
INSERT [dbo].[u_VenueSchedule] ([VSId], [VenueId], [VSDay], [VSDateFrom], [VSDateTo], [VSTimeFrom], [VSTimeto], [CreatedBy]) VALUES (6, 5, N'Tuesday', CAST(N'2017-12-06 00:00:00.000' AS DateTime), CAST(N'2018-03-03 00:00:00.000' AS DateTime), N'10:00:00', N'18:00:00', 2)
GO
INSERT [dbo].[u_VenueSchedule] ([VSId], [VenueId], [VSDay], [VSDateFrom], [VSDateTo], [VSTimeFrom], [VSTimeto], [CreatedBy]) VALUES (7, 5, N'Wednesday', CAST(N'2017-12-06 00:00:00.000' AS DateTime), CAST(N'2018-03-03 00:00:00.000' AS DateTime), N'10:00:00', N'18:00:00', 2)
GO
SET IDENTITY_INSERT [dbo].[u_VenueSchedule] OFF
GO
SET IDENTITY_INSERT [dbo].[Vendor_Registration] ON 

GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (1, N'sad', N'asd', N'sdsad', N'asd', N'sad@adssad', N'Aditya', N'0Screenshot_20170717-205747.png', NULL)
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (2, N'asd', N'asd', N'asd', N'sad', N'a@gmail.com', N'Aditya@002', N'0Screenshot_20170717-205759.png', NULL)
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (3, N'asdas sadsa', N'aditya', N'asd', N'123456789', N'asd@sads', N'12345', N'DSC_0004.jpg', N'BE')
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (4, N'sad', N'sad', N'sad', N'23232323', N'a@gmail.com', N'Aditya@002', N'card-profile4-square.jpg', NULL)
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (5, N'asd', N'32423', N'sad', N'2432423', N'a@gmail.com', N'Aditya@002', N'card-profile2-square.jpg', NULL)
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (6, N'asd', N'sad', N'asd', N'2342423', N'asd@sads', N'12345', N'card-profile2-square.jpg', NULL)
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (7, N'sad', N'324324', N'23', N'4234', N'a@gmail.com', N'Aditya@002', N'card-profile1-square.jpg', NULL)
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (8, N'sad', N'324324', N'23sad', N'4234', N'a@gmail.com', N'Aditya@002', N'card-profile1-square.jpg', NULL)
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (9, N'sad', N'asdas', N'sad', N'23423423', N'a@gmail.com', N'Aditya@002', N'card-profile2-square.jpg', NULL)
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (10, N'sad', N'sadasd', N'asd', N'23432', N'a@gmail.com', N'12345', N'avatar.jpg', NULL)
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (11, N'asd', N'sdfsf', N'sad', N'43534', N'a@gmail.com', N'Aditya@002', N'camp.jpg', NULL)
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (12, N'sad', N'324234', N'sad', N'2342342', N'a@gmail.com', N'Aditya@002', N'kendall.jpg', NULL)
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (13, N'sad', N'324234', N'asd', N'32423432', N'a@gmail.com', N'Aditya@002', N'marc.jpg', NULL)
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (14, N'sad', N'sad', N'asdd', N'2342342', N'a@gmail.com', N'Aditya@002', N'christian.jpg', NULL)
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (15, N'asd', N'asd', N'sadsd', N'3423423', N'a@gmail.com', N'Aditya@002', N'card-profile2-square.jpg', NULL)
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (16, N'wee', N'weewe', N'wewe', N'434343', N'a@gmail.com', N'Aditya@002', N'card-profile2-square.jpg', NULL)
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (17, N'sad', N'asd', N'sadasd', N'343434', N'a@gmail.com', N'Aditya@002', N'card-profile2-square.jpg', NULL)
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (18, N'asd', N'sad', N'asd', N'234324234', N'asd@sads', N'12345', N'kendall.jpg', NULL)
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (19, N'asdasd', N'sad', N'asdd', N'3434334', N'a@gmail.com', N'Aditya@002', N'camp.jpg', NULL)
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (20, N'asd', N'asd', N'fdsf3', N'34343', N'a@gmail.com', N'Aditya@002', N'card-profile2-square.jpg', NULL)
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (21, N'asd', N'asdas', N'asdsad', N'324223432', N'a@gmail.com', N'Aditya@002', N'card-profile1-square.jpg', N'BE')
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (22, N'asdas', N'asd', N'asd', N'343434', N'a@gmail.com', N'Aditya@002', N'card-profile1-square.jpg', N'BE')
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (23, N'asdsad', N'asdsad', N'asdasd', N'3434343', N'a@gmail.com', N'Aditya@002', N'card-profile5-square.jpg', N'BE')
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (24, N'w', N'werwe', N'werwer', N'34534534', N'a@gmail.com', N'Aditya@002', N'card-profile2-square.jpg', N'id=MA')
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (25, N'343', N'werwe', N'werwer', N'34534534', N'a@gmail.com', N'Aditya@002', N'card-profile2-square.jpg', N'id=MA')
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (26, N'erter', N'3453453', N'ertretr', N'345345', NULL, N'werwerwer', N'card-profile4-square.jpg', N'id=MA')
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (27, N'erter', N'3453453', N'ertretr', N'345345', N'a@gmail.com', N'Aditya@002', N'card-profile4-square.jpg', N'id=MA')
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (28, N'aSD', N'asds', N'asd', N'324234324', N'a@gmail.com', N'Aditya@002', N'2.jpg', N'id=BE')
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (29, NULL, NULL, NULL, NULL, NULL, NULL, N'2017-08-10 at 16-14-03.png', N'BE')
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (30, N'uiuiui', NULL, NULL, NULL, NULL, NULL, N'2017-08-10 at 16-14-03.png', N'BE')
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (31, N'uiuiui', NULL, N'uiuii', NULL, NULL, NULL, N'2017-08-10 at 16-14-03.png', N'BE')
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (32, N'uiuiui', NULL, N'uiuii', NULL, NULL, NULL, N'2017-08-10 at 16-14-03.png', N'BE')
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (33, N'[P[', N'[[[[[[', NULL, N'8', N'bhuwanmaitra@gmail.com', N's', N'2017-08-10 at 16-14-03.png', N'id=MA')
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (34, N'[P[', N'[[[[[[', N'ff', N'8', N'bhuwanmaitra@gmail.com', N's', N'2017-08-10 at 16-14-03.png', N'id=MA')
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (35, N'[P[', N'[[[[[[', N'ff', N'8', N'bhuwanmaitra@gmail.com', N's', N'678134-sign-check-128.png', N'id=MA')
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (36, N'Test1', N'Test1', N'Orisha', N'12', N'bhuwanmaitra@gmail.com', N'1', N'YCdl0J9.jpg', N'id=MA')
GO
INSERT [dbo].[Vendor_Registration] ([VRId], [FullName], [VCompanyName], [Address], [PhoneNo], [VEmailId], [VPassword], [VImage], [UserType]) VALUES (37, N'Test1', N'Test1', N'Orisha', N'2', N'bhuwanmaitra@gmail.com', N'1', N'Lighthouse.jpg', N'id=MA')
GO
SET IDENTITY_INSERT [dbo].[Vendor_Registration] OFF
GO
SET IDENTITY_INSERT [dbo].[VRegistrationType] ON 

GO
INSERT [dbo].[VRegistrationType] ([VTRId], [VRId], [E_Cat_ID], [VSId], [CreatedBy]) VALUES (1, 2, 13, N'1,11', NULL)
GO
INSERT [dbo].[VRegistrationType] ([VTRId], [VRId], [E_Cat_ID], [VSId], [CreatedBy]) VALUES (2, 21, 15, N'3,4,7,5', N'21')
GO
INSERT [dbo].[VRegistrationType] ([VTRId], [VRId], [E_Cat_ID], [VSId], [CreatedBy]) VALUES (3, 22, 13, N'11,1', N'22')
GO
INSERT [dbo].[VRegistrationType] ([VTRId], [VRId], [E_Cat_ID], [VSId], [CreatedBy]) VALUES (4, 23, 15, N'6,5,2,8', N'23')
GO
INSERT [dbo].[VRegistrationType] ([VTRId], [VRId], [E_Cat_ID], [VSId], [CreatedBy]) VALUES (5, NULL, 14, N'[]', NULL)
GO
INSERT [dbo].[VRegistrationType] ([VTRId], [VRId], [E_Cat_ID], [VSId], [CreatedBy]) VALUES (6, NULL, 14, N'[]', NULL)
GO
SET IDENTITY_INSERT [dbo].[VRegistrationType] OFF
GO
ALTER TABLE [dbo].[ac_MenueMaster] ADD  CONSTRAINT [DF_ac_MenueMaster_isActive]  DEFAULT ((0)) FOR [isActive]
GO
/****** Object:  StoredProcedure [dbo].[Sp_CategoryType]    Script Date: 2/6/2018 05:19:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Sp_CategoryType] (
	@E_Cat_ID INT = 0
	,@E_Cat_Type NVARCHAR(150) = NULL
	,@E_Cat_Image NVARCHAR(MAX) = NULL
	,@op NVARCHAR(50) = NULL
	)
AS
BEGIN
	IF @op = 'InsertCategoryType'
	BEGIN
		INSERT INTO ac_EventCategoryType (E_Cat_Type,E_Cat_Image)
		VALUES (@E_Cat_Type,@E_Cat_Image)
	END

	IF @op = 'SelectCategoryType'
	BEGIN
		SELECT *
		FROM ac_EventCategoryType
	END

	IF @op = 'EditCategoryType'
	BEGIN
		SELECT *
		FROM ac_EventCategoryType
		WHERE E_Cat_ID = @E_Cat_ID
	END

	IF @op = 'UpdateCategoryType'
	BEGIN
		UPDATE ac_EventCategoryType
		SET E_Cat_Type = @E_Cat_Type
		,E_Cat_Image = @E_Cat_Image
		WHERE E_Cat_ID = @E_Cat_ID
	END

	IF @op = 'DeleteCategoryType'
	BEGIN
		DELETE
		FROM ac_EventCategoryType
		WHERE E_Cat_ID = @E_Cat_ID
	END
END


GO
/****** Object:  StoredProcedure [dbo].[sp_city]    Script Date: 2/6/2018 05:19:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_city] (
	@CityID INT = 0
	,@CityName NVARCHAR(50) = NULL
	,@op NVARCHAR(50) = NULL
	)
AS
BEGIN
	IF @op = 'insertCity'
	BEGIN
		INSERT INTO ac_City (CityName)
		VALUES (@CityName)
	END

	IF @op = 'selectCityName'
	BEGIN
		SELECT *
		FROM ac_City
	END
END


GO
/****** Object:  StoredProcedure [dbo].[sp_CreateNewEvent]    Script Date: 2/6/2018 05:19:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_CreateNewEvent] (
	@EventID INT = 0
	,@EName NVARCHAR(50) = NULL
	,@EDate NVARCHAR(50) = NULL
	,@ECity NVARCHAR(50) = NULL
	,@EManager BIT = 0
	,@EType NVARCHAR(50) = NULL
	,@EHName NVARCHAR(50) = NULL
	,@EHEamil NVARCHAR(50) = NULL
	,@EHPhono NVARCHAR(50) = NULL
	,@ETNameId INT = 0
	,@ETRole NVARCHAR(50) = NULL
	,@EVName NVARCHAR(50) = NULL
	,@EVRole NVARCHAR(50) = NULL
	,@EFeatures NVARCHAR(50) = NULL
	,@op NVARCHAR(50) = NULL
	,@VId INT = 0
	)
AS
BEGIN
	IF @op = 'InsertEventNew'
	BEGIN
		INSERT INTO u_CreateEvent (
			EName
			,EDate
			,ECity
			,EManager
			,EType
			,EHName
			,EHEamil
			,EHPhono
			,ETNameId
			,ETRole
			,EVName
			,EVRole
			,EFeatures
			)
		VALUES (
			@EName
			,@EDate
			,@ECity
			,@EManager
			,@EType
			,@EHName
			,@EHEamil
			,@EHPhono
			,@ETNameId
			,@ETRole
			,@EVName
			,@EVRole
			,@EFeatures
			)
	END

	IF @op = 'SelectTeamGroup'
	BEGIN
		SELECT *
		FROM u_Team ut
		INNER JOIN ac_group ag ON ag.GroupId = ut.MemberGroup
		WHERE ag.GroupBy = @VId
	END
END


GO
/****** Object:  StoredProcedure [dbo].[sp_CreateTeamMembers]    Script Date: 2/6/2018 05:19:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[sp_CreateTeamMembers]
(
@TemaId int = 0
,@MemberEmail varchar(100) = null
,@MemberName varchar(50)= null
,@MemberRole varchar(50) = null
,@op nvarchar(50) = null 
)
as
begin
if @op = 'insertTeamMembers'
begin 
insert into u_CreateTeam
(
MemberEmail
,MemberName
,MemberRole
)
values (
@MemberEmail
,@MemberName
,@MemberRole
)
end
end

GO
/****** Object:  StoredProcedure [dbo].[sp_EventType]    Script Date: 2/6/2018 05:19:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_EventType] (
	@ETID INT = 0
	,@VCID INT = 0
	,@VRId INT = 0
	,@EventType NVARCHAR(60) = NULL
	,@EventTypeImage NVARCHAR(MAX) = NULL
	,@op NVARCHAR(50) = NULL
	)
AS
BEGIN
	IF @op = 'InsertEventType'
	BEGIN
		INSERT INTO ac_EventType (
			VCID
			,EventType
			,EventTypeImage
			)
		VALUES (
			@VCID
			,@EventType
			,@EventTypeImage
			)
	END

	IF @op = 'SelectVendorCategory'
	BEGIN
		SELECT *
		FROM ac_EventCategoryType
	END

	IF @op = 'SelectEventType'
	BEGIN
		SELECT ev.*
			,ec.E_Cat_Type
		FROM ac_EventType ev
		--INNER JOIN Vendor_Registration vr ON ev.VCID LIKE ('%' + REPLACE(vr.VMange, ',', ''))
		INNER JOIN ac_EventCategoryType ec ON ev.VCID = ec.E_Cat_ID
		--WHERE vr.VRId = @VRId
			
	END

	IF @op = 'selectvendorCatrgory'
	BEGIN
		SELECT DISTINCT ec.E_Cat_Type
			,ec.E_Cat_ID
		FROM ac_EventType ev
		--INNER JOIN Vendor_Registration vr ON ev.VCID LIKE ('%' + REPLACE(vr.VMange, ',', ''))
		INNER JOIN ac_EventCategoryType ec ON ev.VCID = ec.E_Cat_ID
		--WHERE vr.VRId = @VRId
	END
	IF @op = 'SelectEventTypeId'
	BEGIN
		SELECT ev.*
			,ec.E_Cat_Type
		FROM ac_EventType ev
		--INNER JOIN Vendor_Registration vr ON ev.VCID LIKE ('%' + REPLACE(vr.VMange, ',', ''))
		INNER JOIN ac_EventCategoryType ec ON ev.VCID = ec.E_Cat_ID
		WHERE 
		--vr.VRId = @VRId and 
		VCID = @ETID
			
	END
END


GO
/****** Object:  StoredProcedure [dbo].[sp_Feature]    Script Date: 2/6/2018 05:19:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Feature] (
	@FId INT = 0
	,@EventId INT = 0
	,@FeatureName NVARCHAR(50) = NULL
	,@op NVARCHAR(50) = NULL
	)
AS
BEGIN
	IF @op = 'InsertFeature'
	BEGIN
		INSERT INTO ac_Feature (
			EventId
			,FeatureName
			)
		VALUES (
			@EventId
			,@FeatureName
			)
	END

	IF @op = 'SelectEventype'
	BEGIN
		SELECT *
		FROM ac_EventType
	END
	IF @op = 'SelectFeatures'
	BEGIN
		SELECT *
		FROM ac_Feature
	END
	
END


GO
/****** Object:  StoredProcedure [dbo].[sp_roleMaster]    Script Date: 2/6/2018 05:19:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_roleMaster] (
	@RoleID INT = 0
	,@RoleName NVARCHAR(50) = NULL
	,@op NVARCHAR(50) = NULL
	)
AS
BEGIN
	IF @op = 'insertRole'
	BEGIN
		INSERT INTO ac_role (RoleName)
		VALUES (@RoleName)
	END
END


GO
/****** Object:  StoredProcedure [dbo].[sp_VendorRegistration]    Script Date: 2/6/2018 05:19:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_VendorRegistration] (
	@VRId int = 0
	,@VCompanyName NVARCHAR(50) = NULL
	,@Address NVARCHAR(MAX) = NULL
	,@VPassword NVARCHAR(50) = NULL
	,@VEmailId NVARCHAR(50) = NULL	
	,@PhoneNo NVARCHAR(50) = NULL
	,@VImage NVARCHAR(MAX) = NULL
	,@UserType nvarchar(10) = null
	,@op NVARCHAR(50) = NULL
	)
AS
BEGIN
	IF @op = 'InsertVendor'
	BEGIN
		INSERT INTO Vendor_Registration (
			VCompanyName
			,Address
			,VPassword
			,VEmailId
			,PhoneNo
			,VImage
			,UserType
			)
		VALUES (
			@VCompanyName
			,@Address
			,@VPassword
			,@VEmailId
			,@PhoneNo
			,@VImage
			,@UserType
			)
	END

	IF @op = 'vendorLogin'
	BEGIN
		SELECT *
		FROM Vendor_Registration
		WHERE VEmailId = @VEmailId
			AND VPassword = @VPassword
	END
END


GO
/****** Object:  StoredProcedure [dbo].[sp_VendorSubType]    Script Date: 2/6/2018 05:19:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_VendorSubType] (
	@VSId INT = 0
	,@VSName NVARCHAR(50) = NULL
	,@E_Cat_ID INT = 0
	,@VSImages NVARCHAR(MAX) = NULL
	,@venueId INT = 0
	,@op NVARCHAR(50) = NULL
	)
AS
BEGIN
	IF @op = 'insertVenodrSub'
	BEGIN
		INSERT INTO ac_VebdorSubType (
			VSName
			,E_Cat_ID
			,VSImages
			)
		VALUES (
			@VSName
			,@E_Cat_ID
			,@VSImages
			)
	END

	IF @op = 'SelectAllVendorData'
	BEGIN
		SELECT *
		FROM ac_EventCategoryType
	END

	IF @op = 'SelectVendorSub'
	BEGIN
		SELECT *
		FROM ac_VebdorSubType
	END

	IF @op = 'SelectVenueStay'
	BEGIN
		SELECT *
		FROM u_StaySubVenue VS
		INNER JOIN ac_VebdorSubType VT ON vt.VSId = vs.VSTId
		where VS.VenueId = @venueId
		UNION
		
		SELECT *
		FROM u_VenueAccientType VA
		INNER JOIN ac_VebdorSubType VT ON vt.VSId = va.VSTId
		WHERE VA.VenueId = @venueId
	END
END


GO
/****** Object:  StoredProcedure [dbo].[sp_VenueManage]    Script Date: 2/6/2018 05:19:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_VenueManage] (
	@VenueId INT = 0
	,@VenueName NVARCHAR(50) = NULL
	,@VenueAddress NVARCHAR(MAX) = NULL
	,@VLng DECIMAL(18, 7) = NULL
	,@VLong DECIMAL(18, 7) = NULL
	,@ETID NVARCHAR(350) = NULL
	,@SpecialityTags NVARCHAR(MAX) = NULL
	,@ContactNo NVARCHAR(50) = NULL
	,@ContactTimeFrom TIME(7) = NULL
	,@ContactTimeTo TIME(7) = NULL
	,@VenueDetails NVARCHAR(MAX) = NULL
	,@CreatedBy INT = NULL
	,@op NVARCHAR(50) = NULL
	)
AS
BEGIN
	IF @op = 'getEditVenueData'
	BEGIN
		SELECT DISTINCT vm.*
			,VT.EventType
			,va.*
			,av.VTId
			,av.VTName AS VTYName
		FROM u_VenueManagement vm
		INNER JOIN u_VenueAccientType va ON vm.VenueId = va.VenueId
		INNER JOIN ac_VenueType av ON va.VSTId = av.VTId
		INNER JOIN ac_EventType VT ON REPLACE(vm.ETID, ',', ' ') LIKE '%' + cast(Vt.ETID AS VARCHAR) + '%'
		WHERE vm.VenueId = @VenueId
			AND vm.CreatedBy = @CreatedBy
	END

	IF @op = 'getAllVenueData'
	BEGIN
		SELECT DISTINCT vm.*
		FROM u_VenueManagement vm
		WHERE vm.CreatedBy = @CreatedBy
	END

	IF @op = 'getAllVenueInfo'
	BEGIN
		SELECT *
		FROM u_VenueManagement vm
		INNER JOIN Vendor_Registration vr ON vr.VRId = vm.CreatedBy
		WHERE vm.CreatedBy = @CreatedBy
			AND vm.VenueId = @VenueId
	END

	IF @op = 'getAllEveentInfo'
	BEGIN
		SELECT et.ETID EventId
			,et.EventType
		FROM u_VenueManagement vm
		INNER JOIN ac_EventType et ON REPLACE(vm.ETID, ',', ' ') LIKE '%' + cast(et.ETID AS VARCHAR) + '%'
		WHERE vm.CreatedBy = @CreatedBy
			AND vm.VenueId = @VenueId
	END

	IF @op = 'getAllprivacyInfo'
	BEGIN
		SELECT vp.*
		FROM u_VenueManagement vm
		INNER JOIN u_VenuePrivacyPolicy vp ON vm.VenueId = vp.VenueId
		WHERE vm.CreatedBy = @CreatedBy
			AND vm.VenueId = @VenueId
	END

	IF @op = 'getAllPackages'
	BEGIN
		SELECT vp.*
		FROM u_VenueManagement vm
		INNER JOIN u_Packages vp ON vm.VenueId = vp.VenueId
		WHERE vm.CreatedBy = @CreatedBy
			AND vm.VenueId = @VenueId
	END

	IF @op = 'getAllHighlight'
	BEGIN
		SELECT DISTINCT *
		FROM (
			SELECT et.*
				,vr.VenueId
			FROM u_VenueManagement vm
			INNER JOIN u_Highlights vr ON vr.VenueId = vm.VenueId
			INNER JOIN ac_Highlights et ON REPLACE(vr.HId, ',', ' ') LIKE '%' + cast(et.HId AS VARCHAR) + '%'
			
			UNION
			
			SELECT et.*
				,vr.VenueId
			FROM u_Highlights vr
			INNER JOIN ac_Highlights et ON REPLACE(vr.HId, ',', ' ') LIKE '%' + cast(et.HId AS VARCHAR) + '%'
			) AS vk
	END
END


GO
/****** Object:  StoredProcedure [dbo].[sp_VenueTypes]    Script Date: 2/6/2018 05:19:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_VenueTypes] (
	@VTId INT = 0
	,@VTName NVARCHAR(50) = NULL
	,@op NVARCHAR(50) = NULL
	)
AS
BEGIN
	IF @op = 'insertVenueType'
	BEGIN
		INSERT INTO ac_VenueType (VTName)
		VALUES (@VTName)
	END

	IF @op = 'GetAllVenueType'
	BEGIN
		SELECT *
		FROM ac_VenueType
	END
END


GO
USE [master]
GO
ALTER DATABASE [EvetovitiDatabase] SET  READ_WRITE 
GO
